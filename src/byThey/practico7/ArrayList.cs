﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;



namespace ArrayListExampleProject
{
    public class ArrayList
    {
        public static int maxSize = 10;
        private Object[] items;
        private int last = -1;

        public ArrayList()
        {
 
            items = new Object[maxSize];
            last = -1;
        }

        
        
        public int getLast()
        {
            return last;

        }

        public int getMaxSize()
        {
            return maxSize;
        }
       
        public void add(Object item, int position) 
        {

            if (position < 0 || position > last + 1)
            {
                throw new IndexOutOfRangeException();
            }
            else
            {
                int aux = last;
                while (aux>=position) { 
                    items[aux+1] = items[aux];
                    aux--;
                }
                items[position] = item;
                last++;
            }
        }
        public void remove(int position)
        {

           if (position < 0 || position > (last + 1))
            {
               
                throw new IndexOutOfRangeException();
            }
            else
            {

                int aux = last;
                while (aux < position)
                {
                    items[aux] = items[aux + 1];
                    aux++;
                }
                last--;
            }
        }

    }
}
