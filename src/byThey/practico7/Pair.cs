﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwapProyect
{
    public class IntPair {

        private int x;
        private int y;

        public IntPair() { }

        public IntPair(int x, int y) { this.x = x; this.y = y; }

        public int GetX() { return x; }

        public int GetY() { return y; }

        public void SetX(int x) { this.x = x; }

        public void SetY(int y) { this.y = y; }


        public void swap()
        {
            x = x + y;
            y = x - y;
            x = x - y;
        }

    }
    public class FloatPair{

             private float x;
             private float y;

             public FloatPair() { }

             public FloatPair(float x, float y) { this.x = x; this.y = y; }

             public float GetX() { return x; }

             public float GetY() { return y; }

             public void SetX(float x) { this.x = x; }

             public void SetY(float y) { this.y = y; }

             public void swap()
             {
                 x = x + y;
                 y = x - y;
                 x = x - y;
             }



      }
 }
