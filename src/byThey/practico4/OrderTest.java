package byThey.practico4;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.*;

import org.junit.Before;
import org.junit.Test;

public class OrderTest {

	IShopDataAccess dataAccess;
	Order o;
	@Before
	public void setUp() throws Exception {
		dataAccess = createMock(IShopDataAccess.class);
		o = new Order(6, dataAccess);
	}

	@Test
	public void test1() {
		expect(dataAccess.getProductStock(1234)).andReturn(5).times(2);
		dataAccess.setNewStockProduct(1234, 4);
		replay(dataAccess);

		o.add(new OrderLine(o, 1234, 1));
		// Verifica que se ejercita el comportamiento esperado
		verify(dataAccess);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test2() {
		 try{
			 new OrderLine(null, 3, 4);
		 }catch(RuntimeException re){
		      String message = "owner";
		      assertEquals(message, re.getMessage());
		      throw re;
		 }
		 fail();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test3() {
		try{
			new OrderLine(new Order(2, dataAccess), 3, 4);
		}catch(RuntimeException re){
			String message = "product not in stock";
			assertEquals(message, re.getMessage());
			throw re;
		}
		fail();
	}
	
	@Test
	public void test4() {
		expect(dataAccess.getProductStock(1234)).andReturn(2).times(2);
		expect(dataAccess.getProductPrice(1234)).andReturn((double) 2);
		dataAccess.setNewStockProduct(1234, 1);
		expectLastCall();
		replay(dataAccess);
		
		OrderLine ol = new OrderLine(o, 1234, 1);
		assertTrue(ol.getTotal() >= 0);
		verify(dataAccess);
	}
	
	@Test  
	public void test5() {
		dataAccess.save(6, o);
		expectLastCall();
		replay(dataAccess);
		o.save();
		assertTrue(o.getLines().isEmpty());
	}

}
