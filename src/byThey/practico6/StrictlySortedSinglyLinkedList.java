package byThey.practico6;
 
import java.util.HashSet;

import korat.finitization.IClassDomain;
import korat.finitization.IFinitization;
import korat.finitization.IIntSet;
import korat.finitization.IObjSet;
import korat.finitization.impl.FinitizationFactory;

/**
 * Class  StrictlySortedSinglyLinkedList defines Strictly Sorted, Singly linked List 
 * @author 
 */ 
public class StrictlySortedSinglyLinkedList implements java.io.Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 6898469319256471378L;
	/**
	 *  
	 */
	public Node header;
    public int size = 0;
   
    public StrictlySortedSinglyLinkedList(){
    	header = new Node(); //dummy
    	size = 0;
   }	

    /**
     * This method checks whether a given Integer value belongs to the current list.
     * @param value The element whose presence in this list is to be tested.
     * @return true iff value is in the current list.
     */
    public boolean contains(Integer value){
    	Node current = header.next;	
    	while(current!=null && current.element.intValue()<= value.intValue()){
    			if(current.element.intValue()==value.intValue())
    				return  true;
    			current = current.next;
       	} 
    	return false;
    	
    }

  public boolean add(Integer value){
	Node current = header.next;	
	Node previous = header;
	if(value ==null)
		throw new IllegalArgumentException();
	
    while(current!=null && current.element.intValue()< value.intValue()){
		previous = current;		
		current = current.next;
    }
	Node n = new Node();
	n.element =  value;
	if (current==null){
		previous.next = n;
		size++;
		return true;
	} 
	if(current.element.intValue()==value.intValue())
    		return  false;

	if(current.element.intValue()>value.intValue()){
		previous.next = n;
		n.next = current;
		size++;
  		return  true;
    	}
	return true;
    		
    }

     public Integer get(int index){
    	Node current = header.next;
    	int i = 0;
    	while(current!=null && i< index){
    		current = current .next;
    		i++;
    	}
    	if(current!=null){
    		return current.element;
       	}
    	return null;
    } 

    
    public int getSize(){
    	return size;
    }    
 

    /**
 	 * Checks whether or not the current list has not elements.
     * @return true iff the current list is empty, false otherwise.
     */
    
    public boolean isEmpty(){
    	return header.next== null;
    }

    public String toString() {
        String res = "{";
        if (header != null) {
            Node cur = header.next;
            while (cur != null && cur != header) {
                res += cur.toString();
                cur = cur.next;
            }
        }
        return res + "}";
    }
    
    @Override
    public Object clone() {
    	
    	StrictlySortedSinglyLinkedList s =new StrictlySortedSinglyLinkedList();
    	for(int i=0;i<this.size;i++)
    		s.add(this.get(i));
    	
    	
    	return s;    
    }
    
    /**  
     * @return true si y sólo si la representación de la lista es internamente 
     * consistente y esta estrictamente ordenada
     * @pre. true
     * @post. retorna true si y sólo si la representación de la lista es internamente
	 * consistente. En este caso, la estructura es internamente consistente si y
	 * sólo si size es exactamente igual al número de nodos en la lista enlazada -1,
	 * la estructura es aciclica y estrictamente ordenada*/	
    public boolean repOK() {
    	if (header == null)
			return false;
    	Node aux = header.next;
    	int i = 0;
    	HashSet<Node> set = new HashSet<Node>();
    	Integer anterior = Integer.MIN_VALUE;
    	
    	while(aux != null){
    		if(set.contains(aux))
    			return false;
    		if(aux.element == null)
    			return false;
    		if(anterior >= aux.element) 
				return false;
    		set.add(aux);
    		anterior = aux.element;
    		i++;
    		aux = aux.next;
    	}
    	if (i != size) 
			return false;
    	return true;
    }
    
    public static IFinitization finStrictlySortedSinglyLinkedList(int minSize, int maxSize,
            int numEntries, int numElems) {
        IFinitization f = FinitizationFactory.create(StrictlySortedSinglyLinkedList.class);
       
        // Second parameter: Include null
        IObjSet entries = f.createObjSet(Node.class, true);
        entries.addClassDomain(f.createClassDomain(Node.class, numEntries));

        IIntSet sizes = f.createIntSet(minSize, maxSize);

        IObjSet elems = f.createObjSet(Integer.class);
        IClassDomain elemsClassDomain = f.createClassDomain(Integer.class);
        elemsClassDomain.includeInIsomorphismCheck(false);
        for (int i = 1; i <= numElems; i++)
            elemsClassDomain.addObject(new Integer(i));
        elems.addClassDomain(elemsClassDomain);
        elems.setNullAllowed(true);

        f.set("header", entries);
        f.set("size", sizes);
        f.set(Node.class, "element", elems);
        f.set(Node.class, "next", entries);
        return f;
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (!(obj instanceof StrictlySortedSinglyLinkedList)) 
			return false;

    	StrictlySortedSinglyLinkedList pobj = (StrictlySortedSinglyLinkedList) obj;
    	if (this.getSize() != pobj.getSize())
			return false;

    	for (int i = 0; i < this.getSize(); i++)
			if(!this.get(i).equals(pobj.get(i)))
				return false;
    	return true;
    }
}