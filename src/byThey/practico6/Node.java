package byThey.practico6;
import java.io.Serializable;


/**
 *StrictlySortedSinglyLinkedList's nodes
 *@author
 */

public  class Node implements Serializable{
  
	public  static final long serialVersionUID = 1; 
	public Integer element;

	public Node next;

	public String toString() {
		return "[" + (element != null ? element.toString() : "null") + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Node))
			return false;
		
		if(this.element == null && ((Node) obj).element == null)
			return true;
		if(this.element == null || ((Node) obj).element == null)
			return false;
		return element.equals(obj);
	}
}
