package byThey.practico1;

public class Minefield {

	private Mine[][] field;
	
	public Minefield() {
		field = new Mine[8][8];
		for (int i=0; i<8; i++) {
			for (int j=0; j<8; j++) {
				field[i][j] = new Mine();
			}
		}
	}
	
	public void open(int x, int y) {
		field[x][y].setOpened(true);
	}

	public void close(int x, int y) {
		field[x][y].setOpened(false);
	}

	public void mark(int x, int y) {
		field[x][y].setMarked(true);
	}

	public void unmark(int x, int y) {
		field[x][y].setMarked(false);
	}

	public void putMine(int x, int y) {
		field[x][y].setMined(true);
	}

	public void removeMine(int x, int y) {
		field[x][y].setMined(false);
	}
	
	public int minedNeighbours(int x, int y) {
		int c = 0;
		for (int i = x-1; i <= x+1 ; i++) 
			for (int k = y-1; k <= y+1; k++) 
				if (x >= 0 && x < field.length && y >= 0 && y < field[0].length && !(i == x && y==i))
					if (field[i][k].isMined())
						c++;
		return c;
	}
}
