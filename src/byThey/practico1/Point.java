package byThey.practico1;

/**
 * Clase Point representa un punto en el plano, caracterizado por sus
 * coordenadas x e y.
 */
public class Point {

	private float x; // coordenada x del punto
	private float y; // coordenada y del punto

	/**
	 * Constructor por defecto. Genera un punto en la interseccion entre los ejes de
	 * x e y.
	 */
	public Point() {
		x = 0;
		y = 0;
	}

	/**
	 * Constructor de la clase, que toma como parametros los valores para la
	 * inicializacion.
	 * 
	 * @param x es el valor a utilizar para setear la primera coordenada del punto
	 * @param y es el valor a utilizar para setear la segunda coordenda del punto.
	 */
	public Point(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Retorna el valor de la primera coordenada del punto
	 * 
	 * @return el valor de la primera coordenada del punto.
	 */
	public float getX() {
		return x;
	}

	/**
	 * Retorna el valor de la segunda coordenada del punto
	 * 
	 * @return el valor de la segunda coordenada del punto.
	 */
	public float getY() {
		// return x;
		return y;

	}

	/**
	 * Cambia el valor de la primera coordenada del punto
	 * 
	 * @param x es el valor con el cual setear la primera coordenada del punto
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * Cambia el valor de la segunda coordenada del punto
	 * 
	 * @param x es el valor con el cual setear la segunda coordenada del punto
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * Intercambia los valores de la primera y segunda coordenadas del punto.
	 */
	public void swap() {
		x = x + y;
		y = x - y;
		x = x - y;

	}

	public Double distanceTo(Point other) {
		return Math.sqrt(Math.pow(other.x - this.x, 2) + Math.pow(other.y - this.y, 2));

	}

	@Override
	public boolean equals(Object arg0) {
		if (!(arg0 instanceof Point))
			return false;
		return this.getX() == ((Point) arg0).getX() && this.getY() == ((Point) arg0).getY();
	}

	@Override
	public String toString() {
		return "("+x+","+y+")";
	}
	
	@Override
	public int hashCode() {
	    int hash = 7;
	    hash = 31 * hash + (int) x;
	    hash = 31 * hash + (int) y;
	    return hash;
	}
}
