package test.practico2;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import byThey.practico2.StaticRoutines;

@RunWith(Enclosed.class)
public class StaticRoutinesTest {

	@RunWith(Parameterized.class)
	public static class ParamPertenece {

		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] { { new Integer[] {}, 0, Boolean.valueOf(false) },
					{ new Integer[] { 1, 2, 3, 4 }, 1, Boolean.valueOf(true) },
					{ new Integer[] { 1, 2, 3, 4 }, 2, Boolean.valueOf(true) },
					{ new Integer[] { 1, 2, 3, 4 }, 4, Boolean.valueOf(true) },
					{ new Integer[] { 1, 2, 3, 4 }, 5, Boolean.valueOf(false) }, });
		}

		private int[] fInputA;

		private int fInputB;

		private boolean fExpected;

		public ParamPertenece(Integer[] inputA, int inputB, boolean expected) {
			int x[] = new int[inputA.length];
			for (int i = 0; i < inputA.length; i++) {
				x[i] = inputA[i];
			}
			this.fInputA = x;
			this.fInputB = inputB;
			this.fExpected = expected;
		}

		@Test
		public void pertenecera() {
			assertEquals(fExpected, StaticRoutines.pertenece(fInputA, fInputB));
		}
	}

	@RunWith(Parameterized.class)
	public static class ParamEven {

		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] { { 0, Boolean.valueOf(true) }, { 1, Boolean.valueOf(false) },
					{ 2, Boolean.valueOf(true) }, { -1, Boolean.valueOf(false) }, { -2, Boolean.valueOf(true) },
					{ -3, Boolean.valueOf(false)}});
		}

		private int fInputA;

		private boolean fExpected;

		public ParamEven(int inputA, boolean expected) {
			this.fInputA = inputA;
			this.fExpected = expected;
		}

		@Test
		public void evenHaven() {
			assertEquals(fExpected, StaticRoutines.isEven(fInputA));
		}
	}

	@RunWith(Parameterized.class)
	public static class ParamCapicua {

		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] { { "asd".toCharArray(), Boolean.valueOf(false) }, 
				{ "".toCharArray(), Boolean.valueOf(true) },
				{ "asddsa".toCharArray(), Boolean.valueOf(true) },
				{ "asd1dsa".toCharArray(), Boolean.valueOf(true)}, 
				{ "a".toCharArray(), Boolean.valueOf(true) },
				{ "adaa".toCharArray(), Boolean.valueOf(false) }});
		}

		private char[] fInputA;

		private boolean fExpected;

		public ParamCapicua(char[] inputA, boolean expected) {
			this.fInputA = inputA;
			this.fExpected = expected;
		}

		@Test
		public void evenHaven() {
			assertEquals(fExpected, StaticRoutines.capicua(fInputA));
		}
	}
	
	@RunWith(Parameterized.class)
	public static class ParamContar {

		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] { { new Integer[] {}, 0},
					{ new Integer[] {1}, 0},
					{ new Integer[] {2}, 1},
					{ new Integer[] { 2, 1, 2, 3, 2 }, 3}});
		}

		private int[] fInputA;

		private int fExpected;

		public ParamContar(Integer[] inputA, int expected) {
			int x[] = new int[inputA.length];
			for (int i = 0; i < inputA.length; i++) {
				x[i] = inputA[i];
			}
			this.fInputA = x;
			this.fExpected = expected;
		}

		@Test
		public void pertenecera() {
			assertEquals(fExpected, StaticRoutines.contarPares(fInputA));
		}
	}
	
	@RunWith(Parameterized.class)
	public static class ParamMCD {

		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] {
					{3,1,1},
					{3,12,3},
					{18,6,6},
					{6,6,6}});
		}

		private int fInputA;
		
		private int fInputB;

		private int fExpected;

		public ParamMCD(int inputA,int inputB , int expected) {
			this.fInputA = inputA;
			this.fInputB = inputB;
			this.fExpected = expected;
		}

		@Test
		public void mcd() {
			assertEquals(fExpected, StaticRoutines.mcd(fInputA, fInputB));
		}
	}
	
	
	@RunWith(Parameterized.class)
	public static class ParamBinary{

		@Parameters
		public static Collection<Object[]> data() {
			return Arrays.asList(new Object[][] {
					{ new Integer[] {1,2,3,4,5},1,Boolean.valueOf(true)},
					{ new Integer[] {1,2,3,4,5},3,Boolean.valueOf(true)},
					{ new Integer[] {1,2,3,4,5},5,Boolean.valueOf(true)},
					{ new Integer[] {},5,Boolean.valueOf(false)},
					{ new Integer[] {1,2,3,4,5},6,Boolean.valueOf(false)}});
		}

		private int[] fInputA;
		
		private int fInputB;

		private boolean fExpected;

		public ParamBinary(Integer[] inputA, int inputB , boolean expected) {
			int x[] = new int[inputA.length];
			for (int i = 0; i < inputA.length; i++) {
				x[i] = inputA[i];
			}
			this.fInputA = x;
			this.fInputB = inputB;
			this.fExpected = expected;
		}

		@Test
		public void binary() {
			assertEquals(fExpected, StaticRoutines.busquedaBinaria(fInputA, fInputB));
		}
	}
	
	public static class nonpara{
		@Test
		public void constructor() {
			new StaticRoutines();
			assertTrue(true);
		}
	}
}
