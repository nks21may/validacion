package test.practico5.FuncionCuadratica;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest0 {

    public static boolean debug = false;

    @Test
    public void test001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test001");
        try {
            byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 0L, (float) (byte) 0, 0.0f);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test002");
        java.lang.Object obj0 = new java.lang.Object();
        java.lang.Class<?> wildcardClass1 = obj0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
    }

    @Test
    public void test003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test003");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        java.lang.String str9 = funcionCuadratica3.toString();
        java.lang.String str10 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "10.0x^2+1.0x" + "'", str9.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "10.0x^2+1.0x" + "'", str10.equals("10.0x^2+1.0x"));
    }

    @Test
    public void test004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test004");
        try {
            byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 0, (-1.0f), (float) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test005");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        float float15 = funcionCuadratica10.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 0.0f + "'", float15 == 0.0f);
    }

    @Test
    public void test006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test006");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 1);
        float float14 = funcionCuadratica0.getA();
        try {
            java.lang.Object obj15 = funcionCuadratica0.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 100.0f + "'", float14 == 100.0f);
    }

    @Test
    public void test007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test007");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float5 = funcionCuadratica0.eval((float) (byte) 100);
        float float6 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11000.0f + "'", float5 == 11000.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
    }

    @Test
    public void test008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test008");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica3.getClass();
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test009");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        float float11 = funcionCuadratica0.eval((float) (byte) 1);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 11.0f + "'", float11 == 11.0f);
    }

    @Test
    public void test010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test010");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 1);
        funcionCuadratica0.setC((float) (short) 10);
        int int16 = funcionCuadratica0.numRaices();
        try {
            java.lang.Object obj17 = funcionCuadratica0.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16 == 0);
    }

    @Test
    public void test011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test011");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        float float5 = funcionCuadratica0.getB();
        java.lang.Object obj6 = funcionCuadratica0.raices();
        funcionCuadratica0.setB((float) '4');
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertNotNull(obj6);
    }

    @Test
    public void test012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test012");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        funcionCuadratica0.setA((float) (short) -1);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
    }

    @Test
    public void test013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test013");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        float float8 = funcionCuadratica0.determinante();
        funcionCuadratica0.setB(100.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 0.0f + "'", float8 == 0.0f);
    }

    @Test
    public void test014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test014");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        int int13 = funcionCuadratica0.numRaices();
        float float14 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 1.0f + "'", float14 == 1.0f);
    }

    @Test
    public void test015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test015");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass6 = funcionCuadratica0.getClass();
        float float7 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 1.0f + "'", float7 == 1.0f);
    }

    @Test
    public void test016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test016");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        int int5 = funcionCuadratica0.numRaices();
        float float7 = funcionCuadratica0.eval(10.0f);
        try {
            java.lang.Object obj8 = funcionCuadratica0.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 0 + "'", int5 == 0);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 110.0f + "'", float7 == 110.0f);
    }

    @Test
    public void test017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test017");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        java.lang.String str6 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "1.0x^2" + "'", str6.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test018");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        float float21 = funcionCuadratica8.eval((float) 1L);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 11.0f + "'", float21 == 11.0f);
    }

    @Test
    public void test019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test019");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        funcionCuadratica0.setC((float) (-1));
        funcionCuadratica0.setA((float) (short) 100);
        float float17 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 500.0f + "'", float17 == 500.0f);
    }

    @Test
    public void test020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test020");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        int int19 = funcionCuadratica5.numRaices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2 + "'", int19 == 2);
    }

    @Test
    public void test021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test021");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        funcionCuadratica0.setA(10010.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
    }

    @Test
    public void test022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test022");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.determinante();
        float float6 = funcionCuadratica0.getB();
        float float7 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + (-40.0f) + "'", float5 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + (-40.0f) + "'", float7 == (-40.0f));
    }

    @Test
    public void test023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test023");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        float float10 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
    }

    @Test
    public void test024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test024");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10L, (float) 1, (float) 10L);
        float float5 = funcionCuadratica3.eval((float) (byte) 100);
        try {
            java.lang.Object obj6 = funcionCuadratica3.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 100110.0f + "'", float5 == 100110.0f);
    }

    @Test
    public void test025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test025");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica0.getClass();
        float float12 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 100.0f + "'", float12 == 100.0f);
    }

    @Test
    public void test026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test026");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-1.0f), 500.0f, (float) 10);
    }

    @Test
    public void test027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test027");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        int int12 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
    }

    @Test
    public void test028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test028");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        java.lang.String str11 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.getC();
        java.lang.String str13 = funcionCuadratica0.toString();
        java.lang.String str14 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) 100L);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "1.0x^2+10.0x" + "'", str13.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "1.0x^2+10.0x" + "'", str14.equals("1.0x^2+10.0x"));
    }

    @Test
    public void test029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test029");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass2);
    }

    @Test
    public void test030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test030");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        float float5 = funcionCuadratica0.getB();
        funcionCuadratica0.setB(1.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
    }

    @Test
    public void test031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test031");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (byte) 0, 0.0f);
        float float4 = funcionCuadratica3.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
    }

    @Test
    public void test032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test032");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-1.0f), 10000.0f, 1.0f);
    }

    @Test
    public void test033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test033");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(179700.0f, 0.0f, (float) '4');
    }

    @Test
    public void test034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test034");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 1);
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica14.setB((float) 10);
        float float18 = funcionCuadratica14.eval((float) 10);
        float float20 = funcionCuadratica14.eval((-1.0f));
        int int21 = funcionCuadratica14.numRaices();
        float float23 = funcionCuadratica14.eval((float) 10L);
        int int24 = funcionCuadratica14.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica28 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica28.setB((float) 1);
        boolean boolean31 = funcionCuadratica14.equals(funcionCuadratica28);
        java.lang.String str32 = funcionCuadratica14.toString();
        float float33 = funcionCuadratica14.determinante();
        float float34 = funcionCuadratica14.getB();
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica14);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 200.0f + "'", float18 == 200.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + (-9.0f) + "'", float20 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 200.0f + "'", float23 == 200.0f);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2 + "'", int24 == 2);
        org.junit.Assert.assertTrue("'" + boolean31 + "' != '" + false + "'", boolean31 == false);
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 100.0f + "'", float33 == 100.0f);
        org.junit.Assert.assertTrue("'" + float34 + "' != '" + 10.0f + "'", float34 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + false + "'", boolean35 == false);
    }

    @Test
    public void test035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test035");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 0);
        java.lang.Class<?> wildcardClass14 = funcionCuadratica0.getClass();
        java.lang.String str15 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "100.0x^2+10.0x" + "'", str15.equals("100.0x^2+10.0x"));
    }

    @Test
    public void test036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test036");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        java.lang.Object obj8 = funcionCuadratica0.raices();
        java.lang.Class<?> wildcardClass9 = obj8.getClass();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(obj8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test037");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        funcionCuadratica3.setA(10010.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
    }

    @Test
    public void test038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test038");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        java.lang.String str9 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        java.lang.Class<?> wildcardClass12 = funcionCuadratica10.getClass();
        funcionCuadratica10.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float24 = funcionCuadratica15.eval((float) 10L);
        int int25 = funcionCuadratica15.numRaices();
        java.lang.String str26 = funcionCuadratica15.toString();
        float float27 = funcionCuadratica15.getB();
        boolean boolean28 = funcionCuadratica10.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica29 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica29.setB((float) 10);
        float float33 = funcionCuadratica29.eval((float) 10);
        float float35 = funcionCuadratica29.eval((-1.0f));
        int int36 = funcionCuadratica29.numRaices();
        float float38 = funcionCuadratica29.eval((float) 10L);
        funcionCuadratica29.setA((float) (byte) 100);
        funcionCuadratica29.setC((float) (byte) 1);
        funcionCuadratica29.setC((float) (short) 10);
        int int45 = funcionCuadratica29.numRaices();
        boolean boolean46 = funcionCuadratica15.equals(funcionCuadratica29);
        boolean boolean47 = funcionCuadratica3.equals(funcionCuadratica29);
        float float48 = funcionCuadratica3.getA();
        float float49 = funcionCuadratica3.getC();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "10.0x^2+1.0x" + "'", str9.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 200.0f + "'", float24 == 200.0f);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 10.0f + "'", float27 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 200.0f + "'", float33 == 200.0f);
        org.junit.Assert.assertTrue("'" + float35 + "' != '" + (-9.0f) + "'", float35 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 2 + "'", int36 == 2);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 200.0f + "'", float38 == 200.0f);
        org.junit.Assert.assertTrue("'" + int45 + "' != '" + 0 + "'", int45 == 0);
        org.junit.Assert.assertTrue("'" + boolean46 + "' != '" + false + "'", boolean46 == false);
        org.junit.Assert.assertTrue("'" + boolean47 + "' != '" + false + "'", boolean47 == false);
        org.junit.Assert.assertTrue("'" + float48 + "' != '" + 10.0f + "'", float48 == 10.0f);
        org.junit.Assert.assertTrue("'" + float49 + "' != '" + 0.0f + "'", float49 == 0.0f);
    }

    @Test
    public void test039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test039");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        java.lang.String str2 = funcionCuadratica0.toString();
        float float3 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + str2 + "' != '" + "1.0x^2" + "'", str2.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 1.0f + "'", float3 == 1.0f);
    }

    @Test
    public void test040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test040");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        float float9 = funcionCuadratica3.eval(1.0f);
        float float10 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 11.0f + "'", float9 == 11.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 1.0f + "'", float10 == 1.0f);
    }

    @Test
    public void test041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test041");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 10, 11.0f, 1.0f);
        funcionCuadratica3.setC((float) 100L);
    }

    @Test
    public void test042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test042");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        float float19 = funcionCuadratica14.eval(0.0f);
        java.lang.Class<?> wildcardClass20 = funcionCuadratica14.getClass();
        java.lang.Class<?> wildcardClass21 = funcionCuadratica14.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 0.0f + "'", float19 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test043");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10.0f, (float) (byte) 100, (float) (short) 0);
        funcionCuadratica3.setA(1.00200096E8f);
    }

    @Test
    public void test044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test044");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) (byte) 100);
        int int10 = funcionCuadratica0.numRaices();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test045");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        int int13 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str15 = funcionCuadratica14.toString();
        java.lang.Class<?> wildcardClass16 = funcionCuadratica14.getClass();
        java.lang.Class<?> wildcardClass17 = funcionCuadratica14.getClass();
        java.lang.Object obj18 = funcionCuadratica14.raices();
        int int19 = funcionCuadratica14.numRaices();
        boolean boolean20 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.Class<?> wildcardClass21 = funcionCuadratica0.getClass();
        float float22 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "1.0x^2" + "'", str15.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(obj18);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1 + "'", int19 == 1);
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + (-1.0f) + "'", float22 == (-1.0f));
    }

    @Test
    public void test046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test046");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-300.0f), 89999.0f, 10.0f);
    }

    @Test
    public void test047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test047");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, 11000.0f, 100.0f);
    }

    @Test
    public void test048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test048");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setB(400449.0f);
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
    }

    @Test
    public void test049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test049");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        funcionCuadratica0.setB(11.0f);
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 121.0f + "'", float8 == 121.0f);
    }

    @Test
    public void test050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test050");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.String str2 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + str2 + "' != '" + "1.0x^2" + "'", str2.equals("1.0x^2"));
    }

    @Test
    public void test051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test051");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 0);
        java.lang.Class<?> wildcardClass14 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass15 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test052");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        float float2 = funcionCuadratica0.getB();
        funcionCuadratica0.setB((float) (short) 0);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + float2 + "' != '" + 0.0f + "'", float2 == 0.0f);
    }

    @Test
    public void test053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test053");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        float float4 = funcionCuadratica3.getC();
        java.lang.String str5 = funcionCuadratica3.toString();
        java.lang.Class<?> wildcardClass6 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+-1.0x" + "'", str5.equals("1.0x^2+-1.0x"));
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test054");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-40.0f));
        java.lang.Object obj23 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 10.0f + "'", float20 == 10.0f);
        org.junit.Assert.assertNotNull(obj23);
    }

    @Test
    public void test055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test055");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float6 = funcionCuadratica0.eval(100.0f);
        float float7 = funcionCuadratica0.getC();
        funcionCuadratica0.setB((float) (short) 1);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 10010.0f + "'", float6 == 10010.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 10.0f + "'", float7 == 10.0f);
    }

    @Test
    public void test056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test056");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getA();
        float float5 = funcionCuadratica0.getC();
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica((-1.0f), (float) 100, (float) 2);
        boolean boolean10 = funcionCuadratica0.equals(funcionCuadratica9);
        funcionCuadratica9.setA(200.0f);
        float float14 = funcionCuadratica9.eval(99921.0f);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean10 + "' != '" + false + "'", boolean10 == false);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 1.99685125E12f + "'", float14 == 1.99685125E12f);
    }

    @Test
    public void test057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test057");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica7.setB((float) 10);
        float float11 = funcionCuadratica7.eval((float) 10);
        float float13 = funcionCuadratica7.eval((-1.0f));
        java.lang.String str14 = funcionCuadratica7.toString();
        funcionCuadratica7.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica17 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str18 = funcionCuadratica17.toString();
        funcionCuadratica17.setC((float) 10);
        boolean boolean21 = funcionCuadratica7.equals(funcionCuadratica17);
        int int22 = funcionCuadratica17.numRaices();
        boolean boolean23 = funcionCuadratica0.equals(funcionCuadratica17);
        float float24 = funcionCuadratica17.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 200.0f + "'", float11 == 200.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + (-9.0f) + "'", float13 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "1.0x^2+10.0x" + "'", str14.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2" + "'", str18.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean21 + "' != '" + false + "'", boolean21 == false);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22 == 0);
        org.junit.Assert.assertTrue("'" + boolean23 + "' != '" + false + "'", boolean23 == false);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 10.0f + "'", float24 == 10.0f);
    }

    @Test
    public void test058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test058");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        float float3 = funcionCuadratica0.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        java.lang.String str11 = funcionCuadratica4.toString();
        float float12 = funcionCuadratica4.getB();
        java.lang.Object obj13 = funcionCuadratica4.raices();
        int int14 = funcionCuadratica4.numRaices();
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica4);
        float float16 = funcionCuadratica0.getC();
        java.lang.Object obj17 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 1.0f + "'", float3 == 1.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14 == 2);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + 0.0f + "'", float16 == 0.0f);
        org.junit.Assert.assertNotNull(obj17);
    }

    @Test
    public void test059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test059");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) '4', (float) (short) 0, (float) 1);
        float float4 = funcionCuadratica3.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + (-208.0f) + "'", float4 == (-208.0f));
    }

    @Test
    public void test060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test060");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        float float20 = funcionCuadratica0.eval((-300.0f));
        java.lang.String str21 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 89999.0f + "'", float20 == 89999.0f);
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2+-1.0" + "'", str21.equals("1.0x^2+-1.0"));
    }

    @Test
    public void test061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test061");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        boolean boolean11 = funcionCuadratica3.equals(funcionCuadratica10);
        try {
            funcionCuadratica3.setA((float) 0L);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + boolean11 + "' != '" + false + "'", boolean11 == false);
    }

    @Test
    public void test062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test062");
        try {
            byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 0, 100100.0f, 200.0f);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test063");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 100, (float) 0L, (float) (byte) 100);
        float float4 = funcionCuadratica3.getA();
        funcionCuadratica3.setA(10.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 100.0f + "'", float4 == 100.0f);
    }

    @Test
    public void test064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test064");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        float float9 = funcionCuadratica3.eval(1.0f);
        int int10 = funcionCuadratica3.numRaices();
        float float12 = funcionCuadratica3.eval((float) ' ');
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 11.0f + "'", float9 == 11.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10272.0f + "'", float12 == 10272.0f);
    }

    @Test
    public void test065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test065");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setA((float) 2);
        float float9 = funcionCuadratica3.eval(100.0f);
        int int10 = funcionCuadratica3.numRaices();
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 20100.0f + "'", float9 == 20100.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
    }

    @Test
    public void test066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test066");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica11.setB((float) 1);
        funcionCuadratica11.setB((float) (byte) 1);
        int int16 = funcionCuadratica11.numRaices();
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica11);
        float float19 = funcionCuadratica0.eval(400449.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2 + "'", int16 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 1.60363397E11f + "'", float19 == 1.60363397E11f);
    }

    @Test
    public void test067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test067");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        java.lang.String str17 = funcionCuadratica10.toString();
        funcionCuadratica10.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str21 = funcionCuadratica20.toString();
        funcionCuadratica20.setC((float) 10);
        boolean boolean24 = funcionCuadratica10.equals(funcionCuadratica20);
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica25.setB((float) 10);
        float float29 = funcionCuadratica25.eval((float) 10);
        float float31 = funcionCuadratica25.eval((-1.0f));
        java.lang.String str32 = funcionCuadratica25.toString();
        float float33 = funcionCuadratica25.getB();
        boolean boolean34 = funcionCuadratica10.equals(funcionCuadratica25);
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica10);
        java.lang.Class<?> wildcardClass36 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "1.0x^2+10.0x" + "'", str17.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2" + "'", str21.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 200.0f + "'", float29 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + (-9.0f) + "'", float31 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + true + "'", boolean34 == true);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + true + "'", boolean35 == true);
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test068");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica8);
        funcionCuadratica0.setB(0.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
    }

    @Test
    public void test069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test069");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.String str4 = funcionCuadratica3.toString();
        java.lang.Class<?> wildcardClass5 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "10.0x^2+100.0x" + "'", str4.equals("10.0x^2+100.0x"));
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test070");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        funcionCuadratica0.setB(11000.0f);
        float float7 = funcionCuadratica0.getA();
        java.lang.Class<?> wildcardClass8 = funcionCuadratica0.getClass();
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str10 = funcionCuadratica9.toString();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica9.getClass();
        boolean boolean12 = funcionCuadratica0.equals(funcionCuadratica9);
        float float14 = funcionCuadratica9.eval((float) (short) 100);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 1.0f + "'", float7 == 1.0f);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2" + "'", str10.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + boolean12 + "' != '" + false + "'", boolean12 == false);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 10000.0f + "'", float14 == 10000.0f);
    }

    @Test
    public void test071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test071");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) '4');
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica11.setB((float) 10);
        float float15 = funcionCuadratica11.eval((float) 10);
        float float17 = funcionCuadratica11.eval((-1.0f));
        int int18 = funcionCuadratica11.numRaices();
        float float20 = funcionCuadratica11.eval((float) 10L);
        int int21 = funcionCuadratica11.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica25.setB((float) 1);
        boolean boolean28 = funcionCuadratica11.equals(funcionCuadratica25);
        java.lang.String str29 = funcionCuadratica11.toString();
        funcionCuadratica11.setC((-1.0f));
        boolean boolean32 = funcionCuadratica0.equals(funcionCuadratica11);
        float float34 = funcionCuadratica0.eval(9000.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 200.0f + "'", float15 == 200.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + (-9.0f) + "'", float17 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 200.0f + "'", float20 == 200.0f);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + str29 + "' != '" + "1.0x^2+10.0x" + "'", str29.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + boolean32 + "' != '" + false + "'", boolean32 == false);
        org.junit.Assert.assertTrue("'" + float34 + "' != '" + 8.1090048E7f + "'", float34 == 8.1090048E7f);
    }

    @Test
    public void test072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test072");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        int int5 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setB((-40.0f));
        funcionCuadratica0.setB(1.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 0 + "'", int5 == 0);
    }

    @Test
    public void test073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test073");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        float float7 = funcionCuadratica3.getA();
        java.lang.Class<?> wildcardClass8 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 100110.0f + "'", float7 == 100110.0f);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test074");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        float float3 = funcionCuadratica0.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        java.lang.String str11 = funcionCuadratica4.toString();
        float float12 = funcionCuadratica4.getB();
        java.lang.Object obj13 = funcionCuadratica4.raices();
        int int14 = funcionCuadratica4.numRaices();
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica4);
        java.lang.Object obj16 = funcionCuadratica4.raices();
        float float17 = funcionCuadratica4.getC();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 1.0f + "'", float3 == 1.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14 == 2);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
        org.junit.Assert.assertNotNull(obj16);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 0.0f + "'", float17 == 0.0f);
    }

    @Test
    public void test075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test075");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        int int15 = funcionCuadratica10.numRaices();
        float float16 = funcionCuadratica10.determinante();
        try {
            java.lang.Object obj17 = funcionCuadratica10.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-40.0f) + "'", float16 == (-40.0f));
    }

    @Test
    public void test076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test076");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        java.lang.String str10 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.eval(1.60363397E11f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 2.5716419E22f + "'", float12 == 2.5716419E22f);
    }

    @Test
    public void test077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test077");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 1.0f + "'", float8 == 1.0f);
    }

    @Test
    public void test078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test078");
        try {
            byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 0, (float) 10, (float) 2);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test079");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.determinante();
        float float6 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) 0);
        int int9 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setA((float) (byte) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass16 = funcionCuadratica15.getClass();
        java.lang.String str17 = funcionCuadratica15.toString();
        int int18 = funcionCuadratica15.numRaices();
        float float19 = funcionCuadratica15.determinante();
        boolean boolean20 = funcionCuadratica0.equals(funcionCuadratica15);
        float float21 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + (-40.0f) + "'", float5 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "10.0x^2+100.0x" + "'", str17.equals("10.0x^2+100.0x"));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 10000.0f + "'", float19 == 10000.0f);
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 0.0f + "'", float21 == 0.0f);
    }

    @Test
    public void test080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test080");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float7 = funcionCuadratica3.eval((float) 2);
        float float8 = funcionCuadratica3.getC();
        funcionCuadratica3.setC(90.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 400449.0f + "'", float7 == 400449.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 11.0f + "'", float8 == 11.0f);
    }

    @Test
    public void test081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test081");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float5 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 10.0f + "'", float5 == 10.0f);
    }

    @Test
    public void test082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test082");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-300.0f));
        int int22 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 10.0f + "'", float19 == 10.0f);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
    }

    @Test
    public void test083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test083");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        float float10 = funcionCuadratica0.getB();
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica11.setB((float) 10);
        float float15 = funcionCuadratica11.eval((float) 10);
        float float17 = funcionCuadratica11.eval((-1.0f));
        int int18 = funcionCuadratica11.numRaices();
        float float19 = funcionCuadratica11.getB();
        java.lang.Class<?> wildcardClass20 = funcionCuadratica11.getClass();
        float float21 = funcionCuadratica11.determinante();
        boolean boolean22 = funcionCuadratica0.equals(funcionCuadratica11);
        float float24 = funcionCuadratica0.eval(3.987415E24f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 10.0f + "'", float10 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 200.0f + "'", float15 == 200.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + (-9.0f) + "'", float17 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 10.0f + "'", float19 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 100.0f + "'", float21 == 100.0f);
        org.junit.Assert.assertTrue("'" + boolean22 + "' != '" + true + "'", boolean22 == true);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + Float.POSITIVE_INFINITY + "'", float24 == Float.POSITIVE_INFINITY);
    }

    @Test
    public void test084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test084");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        java.lang.String str9 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        java.lang.Class<?> wildcardClass12 = funcionCuadratica10.getClass();
        funcionCuadratica10.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float24 = funcionCuadratica15.eval((float) 10L);
        int int25 = funcionCuadratica15.numRaices();
        java.lang.String str26 = funcionCuadratica15.toString();
        float float27 = funcionCuadratica15.getB();
        boolean boolean28 = funcionCuadratica10.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica29 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica29.setB((float) 10);
        float float33 = funcionCuadratica29.eval((float) 10);
        float float35 = funcionCuadratica29.eval((-1.0f));
        int int36 = funcionCuadratica29.numRaices();
        float float38 = funcionCuadratica29.eval((float) 10L);
        funcionCuadratica29.setA((float) (byte) 100);
        funcionCuadratica29.setC((float) (byte) 1);
        funcionCuadratica29.setC((float) (short) 10);
        int int45 = funcionCuadratica29.numRaices();
        boolean boolean46 = funcionCuadratica15.equals(funcionCuadratica29);
        boolean boolean47 = funcionCuadratica3.equals(funcionCuadratica29);
        float float48 = funcionCuadratica3.getA();
        float float49 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "10.0x^2+1.0x" + "'", str9.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 200.0f + "'", float24 == 200.0f);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 10.0f + "'", float27 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 200.0f + "'", float33 == 200.0f);
        org.junit.Assert.assertTrue("'" + float35 + "' != '" + (-9.0f) + "'", float35 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 2 + "'", int36 == 2);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 200.0f + "'", float38 == 200.0f);
        org.junit.Assert.assertTrue("'" + int45 + "' != '" + 0 + "'", int45 == 0);
        org.junit.Assert.assertTrue("'" + boolean46 + "' != '" + false + "'", boolean46 == false);
        org.junit.Assert.assertTrue("'" + boolean47 + "' != '" + false + "'", boolean47 == false);
        org.junit.Assert.assertTrue("'" + float48 + "' != '" + 10.0f + "'", float48 == 10.0f);
        org.junit.Assert.assertTrue("'" + float49 + "' != '" + 1.0f + "'", float49 == 1.0f);
    }

    @Test
    public void test085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test085");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        float float4 = funcionCuadratica3.getC();
        java.lang.String str5 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica6 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica6.setB((float) 10);
        float float10 = funcionCuadratica6.eval((float) 10);
        int int11 = funcionCuadratica6.numRaices();
        boolean boolean12 = funcionCuadratica3.equals(funcionCuadratica6);
        int int13 = funcionCuadratica3.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+-1.0x" + "'", str5.equals("1.0x^2+-1.0x"));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 200.0f + "'", float10 == 200.0f);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 2 + "'", int11 == 2);
        org.junit.Assert.assertTrue("'" + boolean12 + "' != '" + false + "'", boolean12 == false);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
    }

    @Test
    public void test086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test086");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        float float10 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test087");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) (-1L));
        funcionCuadratica0.setA((float) ' ');
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + (-9.0f) + "'", float4 == (-9.0f));
    }

    @Test
    public void test088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test088");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        float float4 = funcionCuadratica3.getC();
        java.lang.String str5 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica6 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica6.setB((float) 10);
        float float10 = funcionCuadratica6.eval((float) 10);
        int int11 = funcionCuadratica6.numRaices();
        boolean boolean12 = funcionCuadratica3.equals(funcionCuadratica6);
        int int13 = funcionCuadratica6.numRaices();
        funcionCuadratica6.setB(10000.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+-1.0x" + "'", str5.equals("1.0x^2+-1.0x"));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 200.0f + "'", float10 == 200.0f);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 2 + "'", int11 == 2);
        org.junit.Assert.assertTrue("'" + boolean12 + "' != '" + false + "'", boolean12 == false);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
    }

    @Test
    public void test089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test089");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        funcionCuadratica3.setB(200.0f);
        java.lang.String str8 = funcionCuadratica3.toString();
        try {
            java.lang.Object obj9 = funcionCuadratica3.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "100110.0x^2+200.0x+11.0" + "'", str8.equals("100110.0x^2+200.0x+11.0"));
    }

    @Test
    public void test090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test090");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 1);
        float float14 = funcionCuadratica0.getB();
        java.lang.Class<?> wildcardClass15 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 10.0f + "'", float14 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test091");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getA();
        float float5 = funcionCuadratica0.getC();
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica((-1.0f), (float) 100, (float) 2);
        boolean boolean10 = funcionCuadratica0.equals(funcionCuadratica9);
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica14);
        funcionCuadratica14.setA((float) 'a');
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean10 + "' != '" + false + "'", boolean10 == false);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
    }

    @Test
    public void test092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test092");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        float float10 = funcionCuadratica0.getC();
        float float11 = funcionCuadratica0.getB();
        float float12 = funcionCuadratica0.getC();
        java.lang.Object obj13 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 10.0f + "'", float11 == 10.0f);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertNotNull(obj13);
    }

    @Test
    public void test093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test093");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        int int13 = funcionCuadratica0.numRaices();
        float float14 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-1.0f) + "'", float14 == (-1.0f));
    }

    @Test
    public void test094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test094");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.determinante();
        float float6 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) 0);
        int int9 = funcionCuadratica0.numRaices();
        float float10 = funcionCuadratica0.getB();
        java.lang.Object obj11 = funcionCuadratica0.raices();
        funcionCuadratica0.setC(100110.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + (-40.0f) + "'", float5 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertNotNull(obj11);
    }

    @Test
    public void test095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test095");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-40.0f));
        int int23 = funcionCuadratica0.numRaices();
        float float24 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 10.0f + "'", float20 == 10.0f);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2 + "'", int23 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 100.0f + "'", float24 == 100.0f);
    }

    @Test
    public void test096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test096");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        java.lang.String str9 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        java.lang.Class<?> wildcardClass12 = funcionCuadratica10.getClass();
        funcionCuadratica10.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float24 = funcionCuadratica15.eval((float) 10L);
        int int25 = funcionCuadratica15.numRaices();
        java.lang.String str26 = funcionCuadratica15.toString();
        float float27 = funcionCuadratica15.getB();
        boolean boolean28 = funcionCuadratica10.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica29 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica29.setB((float) 10);
        float float33 = funcionCuadratica29.eval((float) 10);
        float float35 = funcionCuadratica29.eval((-1.0f));
        int int36 = funcionCuadratica29.numRaices();
        float float38 = funcionCuadratica29.eval((float) 10L);
        funcionCuadratica29.setA((float) (byte) 100);
        funcionCuadratica29.setC((float) (byte) 1);
        funcionCuadratica29.setC((float) (short) 10);
        int int45 = funcionCuadratica29.numRaices();
        boolean boolean46 = funcionCuadratica15.equals(funcionCuadratica29);
        boolean boolean47 = funcionCuadratica3.equals(funcionCuadratica29);
        java.lang.String str48 = funcionCuadratica3.toString();
        funcionCuadratica3.setC((float) (-1));
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "10.0x^2+1.0x" + "'", str9.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 200.0f + "'", float24 == 200.0f);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 10.0f + "'", float27 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 200.0f + "'", float33 == 200.0f);
        org.junit.Assert.assertTrue("'" + float35 + "' != '" + (-9.0f) + "'", float35 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 2 + "'", int36 == 2);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 200.0f + "'", float38 == 200.0f);
        org.junit.Assert.assertTrue("'" + int45 + "' != '" + 0 + "'", int45 == 0);
        org.junit.Assert.assertTrue("'" + boolean46 + "' != '" + false + "'", boolean46 == false);
        org.junit.Assert.assertTrue("'" + boolean47 + "' != '" + false + "'", boolean47 == false);
        org.junit.Assert.assertTrue("'" + str48 + "' != '" + "10.0x^2+1.0x" + "'", str48.equals("10.0x^2+1.0x"));
    }

    @Test
    public void test097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test097");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (byte) 1);
        float float6 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4.0f) + "'", float6 == (-4.0f));
    }

    @Test
    public void test098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test098");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        try {
            java.lang.Object obj4 = funcionCuadratica0.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
    }

    @Test
    public void test099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test099");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) (byte) 100);
        float float10 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-300.0f) + "'", float10 == (-300.0f));
    }

    @Test
    public void test100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test100");
        try {
            byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 0, (float) 10, (float) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test101");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        float float3 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 0.0f + "'", float3 == 0.0f);
    }

    @Test
    public void test102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test102");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.60363397E11f, (float) 'a', 11000.0f);
    }

    @Test
    public void test103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test103");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 100, 8.1206009E12f, (float) (short) 100);
    }

    @Test
    public void test104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test104");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        boolean boolean24 = funcionCuadratica0.equals(funcionCuadratica15);
        float float25 = funcionCuadratica15.getA();
        float float26 = funcionCuadratica15.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + true + "'", boolean24 == true);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 1.0f + "'", float25 == 1.0f);
        org.junit.Assert.assertTrue("'" + float26 + "' != '" + 1.0f + "'", float26 == 1.0f);
    }

    @Test
    public void test105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test105");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setC((float) 1L);
        float float5 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
    }

    @Test
    public void test106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test106");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        java.lang.String str17 = funcionCuadratica10.toString();
        funcionCuadratica10.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str21 = funcionCuadratica20.toString();
        funcionCuadratica20.setC((float) 10);
        boolean boolean24 = funcionCuadratica10.equals(funcionCuadratica20);
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica25.setB((float) 10);
        float float29 = funcionCuadratica25.eval((float) 10);
        float float31 = funcionCuadratica25.eval((-1.0f));
        java.lang.String str32 = funcionCuadratica25.toString();
        float float33 = funcionCuadratica25.getB();
        boolean boolean34 = funcionCuadratica10.equals(funcionCuadratica25);
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica10);
        float float36 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "1.0x^2+10.0x" + "'", str17.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2" + "'", str21.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 200.0f + "'", float29 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + (-9.0f) + "'", float31 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + true + "'", boolean34 == true);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + true + "'", boolean35 == true);
        org.junit.Assert.assertTrue("'" + float36 + "' != '" + 1.0f + "'", float36 == 1.0f);
    }

    @Test
    public void test107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test107");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        float float10 = funcionCuadratica0.getB();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) (byte) 10, (float) (short) 0, (float) (byte) 10);
        float float15 = funcionCuadratica14.getB();
        java.lang.String str16 = funcionCuadratica14.toString();
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 10.0f + "'", float10 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 0.0f + "'", float15 == 0.0f);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "10.0x^2+10.0" + "'", str16.equals("10.0x^2+10.0"));
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
    }

    @Test
    public void test108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test108");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        float float10 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((-4.0f));
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 10.0f + "'", float10 == 10.0f);
    }

    @Test
    public void test109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test109");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        funcionCuadratica8.setC((float) 10);
        java.lang.Class<?> wildcardClass22 = funcionCuadratica8.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test110");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        float float10 = funcionCuadratica0.getC();
        float float11 = funcionCuadratica0.getB();
        float float12 = funcionCuadratica0.getC();
        float float13 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 10.0f + "'", float11 == 10.0f);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 1.0f + "'", float13 == 1.0f);
    }

    @Test
    public void test111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test111");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica3.getClass();
        java.lang.String str5 = funcionCuadratica3.toString();
        int int6 = funcionCuadratica3.numRaices();
        float float7 = funcionCuadratica3.determinante();
        java.lang.Object obj8 = funcionCuadratica3.raices();
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "10.0x^2+100.0x" + "'", str5.equals("10.0x^2+100.0x"));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6 == 2);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 10000.0f + "'", float7 == 10000.0f);
        org.junit.Assert.assertNotNull(obj8);
    }

    @Test
    public void test112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test112");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10000.0f, 10010.0f, 0.0f);
        float float4 = funcionCuadratica3.determinante();
        float float5 = funcionCuadratica3.getA();
        java.lang.Class<?> wildcardClass6 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.00200096E8f + "'", float4 == 1.00200096E8f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 10000.0f + "'", float5 == 10000.0f);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test113");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) (byte) 0);
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str12 = funcionCuadratica11.toString();
        java.lang.Class<?> wildcardClass13 = funcionCuadratica11.getClass();
        float float14 = funcionCuadratica11.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        java.lang.Object obj24 = funcionCuadratica15.raices();
        int int25 = funcionCuadratica15.numRaices();
        boolean boolean26 = funcionCuadratica11.equals(funcionCuadratica15);
        float float27 = funcionCuadratica11.getC();
        boolean boolean28 = funcionCuadratica0.equals(funcionCuadratica11);
        int int29 = funcionCuadratica11.numRaices();
        java.lang.Object obj30 = funcionCuadratica11.raices();
        float float31 = funcionCuadratica11.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2" + "'", str12.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 1.0f + "'", float14 == 1.0f);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertNotNull(obj24);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + boolean26 + "' != '" + false + "'", boolean26 == false);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 0.0f + "'", float27 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1 + "'", int29 == 1);
        org.junit.Assert.assertNotNull(obj30);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + 0.0f + "'", float31 == 0.0f);
    }

    @Test
    public void test114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test114");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        java.lang.String str11 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.getC();
        funcionCuadratica0.setA((float) (short) 1);
        java.lang.Object obj15 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertNotNull(obj15);
    }

    @Test
    public void test115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test115");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10L, (float) 1, (float) 10L);
        float float5 = funcionCuadratica3.eval((float) (byte) 100);
        float float7 = funcionCuadratica3.eval((-9.0f));
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 100110.0f + "'", float5 == 100110.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 811.0f + "'", float7 == 811.0f);
    }

    @Test
    public void test116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test116");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (-1L));
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getC();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
    }

    @Test
    public void test117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test117");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10.0f, 0.0f, (float) 1L);
        funcionCuadratica3.setB(11.0f);
    }

    @Test
    public void test118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test118");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-300.0f), (float) (short) -1, (float) 1L);
        float float5 = funcionCuadratica3.eval((float) 0L);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 1.0f + "'", float5 == 1.0f);
    }

    @Test
    public void test119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test119");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setC((float) 1L);
        float float6 = funcionCuadratica0.eval(1.02512616E8f);
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 1.05088369E16f + "'", float6 == 1.05088369E16f);
    }

    @Test
    public void test120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test120");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
    }

    @Test
    public void test121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test121");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        java.lang.Object obj8 = funcionCuadratica0.raices();
        float float10 = funcionCuadratica0.eval((float) 0L);
        java.lang.Object obj11 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(obj8);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertNotNull(obj11);
    }

    @Test
    public void test122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test122");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) '4');
        float float11 = funcionCuadratica0.determinante();
        java.lang.String str12 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-108.0f) + "'", float11 == (-108.0f));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2+10.0x+52.0" + "'", str12.equals("1.0x^2+10.0x+52.0"));
    }

    @Test
    public void test123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test123");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
    }

    @Test
    public void test124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test124");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        funcionCuadratica0.setB(500.0f);
        java.lang.Object obj7 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(obj7);
    }

    @Test
    public void test125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test125");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str8 = funcionCuadratica7.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica7.getClass();
        funcionCuadratica7.setC((float) (-1L));
        funcionCuadratica7.setB(11000.0f);
        boolean boolean14 = funcionCuadratica3.equals(funcionCuadratica7);
        try {
            java.lang.Object obj15 = funcionCuadratica3.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
    }

    @Test
    public void test126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test126");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setA((float) 2);
        float float9 = funcionCuadratica3.eval(100.0f);
        float float10 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 20100.0f + "'", float9 == 20100.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 2.0f + "'", float10 == 2.0f);
    }

    @Test
    public void test127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test127");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.determinante();
        float float6 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) 0);
        int int9 = funcionCuadratica0.numRaices();
        float float10 = funcionCuadratica0.getB();
        java.lang.Object obj11 = funcionCuadratica0.raices();
        funcionCuadratica0.setC((-44000.0f));
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + (-40.0f) + "'", float5 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertNotNull(obj11);
    }

    @Test
    public void test128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test128");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setA((float) (short) 1);
        funcionCuadratica3.setB((float) 2);
        funcionCuadratica3.setC(8.1046128E7f);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
    }

    @Test
    public void test129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test129");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setA((float) (short) 1);
        java.lang.Class<?> wildcardClass11 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test130");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        float float11 = funcionCuadratica0.eval(0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 0.0f + "'", float11 == 0.0f);
    }

    @Test
    public void test131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test131");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        funcionCuadratica0.setB(11000.0f);
        float float7 = funcionCuadratica0.getA();
        java.lang.Class<?> wildcardClass8 = funcionCuadratica0.getClass();
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str10 = funcionCuadratica9.toString();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica9.getClass();
        boolean boolean12 = funcionCuadratica0.equals(funcionCuadratica9);
        funcionCuadratica0.setC(200.0f);
        java.lang.String str15 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 1.0f + "'", float7 == 1.0f);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2" + "'", str10.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + boolean12 + "' != '" + false + "'", boolean12 == false);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "1.0x^2+11000.0x+200.0" + "'", str15.equals("1.0x^2+11000.0x+200.0"));
    }

    @Test
    public void test132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test132");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        java.lang.String str17 = funcionCuadratica10.toString();
        funcionCuadratica10.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str21 = funcionCuadratica20.toString();
        funcionCuadratica20.setC((float) 10);
        boolean boolean24 = funcionCuadratica10.equals(funcionCuadratica20);
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica25.setB((float) 10);
        float float29 = funcionCuadratica25.eval((float) 10);
        float float31 = funcionCuadratica25.eval((-1.0f));
        java.lang.String str32 = funcionCuadratica25.toString();
        float float33 = funcionCuadratica25.getB();
        boolean boolean34 = funcionCuadratica10.equals(funcionCuadratica25);
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica10);
        funcionCuadratica10.setC(99921.0f);
        float float38 = funcionCuadratica10.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "1.0x^2+10.0x" + "'", str17.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2" + "'", str21.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 200.0f + "'", float29 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + (-9.0f) + "'", float31 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + true + "'", boolean34 == true);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + true + "'", boolean35 == true);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 1.0f + "'", float38 == 1.0f);
    }

    @Test
    public void test133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test133");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.String str9 = funcionCuadratica0.toString();
        java.lang.String str10 = funcionCuadratica0.toString();
        try {
            funcionCuadratica0.setA((float) (byte) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "1.0x^2+10.0x" + "'", str9.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
    }

    @Test
    public void test134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test134");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(199.0f, 100100.0f, 1.60359435E12f);
        float float4 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 100100.0f + "'", float4 == 100100.0f);
    }

    @Test
    public void test135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test135");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        java.lang.Object obj20 = funcionCuadratica8.raices();
        float float21 = funcionCuadratica8.getB();
        float float22 = funcionCuadratica8.getA();
        float float23 = funcionCuadratica8.determinante();
        funcionCuadratica8.setC(0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertNotNull(obj20);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 10.0f + "'", float21 == 10.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 1.0f + "'", float22 == 1.0f);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 100.0f + "'", float23 == 100.0f);
    }

    @Test
    public void test136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test136");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        boolean boolean24 = funcionCuadratica0.equals(funcionCuadratica15);
        java.lang.Object obj25 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + true + "'", boolean24 == true);
        org.junit.Assert.assertNotNull(obj25);
    }

    @Test
    public void test137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test137");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setA((float) (short) 1);
        funcionCuadratica3.setB((float) 2);
        funcionCuadratica3.setC((float) (short) 100);
        try {
            java.lang.Object obj15 = funcionCuadratica3.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
    }

    @Test
    public void test138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test138");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        java.lang.Object obj8 = funcionCuadratica0.raices();
        float float10 = funcionCuadratica0.eval((float) 0L);
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str12 = funcionCuadratica11.toString();
        java.lang.Class<?> wildcardClass13 = funcionCuadratica11.getClass();
        float float14 = funcionCuadratica11.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        java.lang.Object obj24 = funcionCuadratica15.raices();
        int int25 = funcionCuadratica15.numRaices();
        boolean boolean26 = funcionCuadratica11.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica27 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica27.setB((float) 10);
        float float31 = funcionCuadratica27.eval((float) 10);
        float float33 = funcionCuadratica27.eval((-1.0f));
        int int34 = funcionCuadratica27.numRaices();
        float float36 = funcionCuadratica27.eval((float) 10L);
        int int37 = funcionCuadratica27.numRaices();
        boolean boolean38 = funcionCuadratica15.equals(funcionCuadratica27);
        boolean boolean39 = funcionCuadratica0.equals(funcionCuadratica15);
        float float41 = funcionCuadratica15.eval(0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(obj8);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2" + "'", str12.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 1.0f + "'", float14 == 1.0f);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertNotNull(obj24);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + boolean26 + "' != '" + false + "'", boolean26 == false);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + 200.0f + "'", float31 == 200.0f);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + (-9.0f) + "'", float33 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 2 + "'", int34 == 2);
        org.junit.Assert.assertTrue("'" + float36 + "' != '" + 200.0f + "'", float36 == 200.0f);
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 2 + "'", int37 == 2);
        org.junit.Assert.assertTrue("'" + boolean38 + "' != '" + true + "'", boolean38 == true);
        org.junit.Assert.assertTrue("'" + boolean39 + "' != '" + false + "'", boolean39 == false);
        org.junit.Assert.assertTrue("'" + float41 + "' != '" + 0.0f + "'", float41 == 0.0f);
    }

    @Test
    public void test139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test139");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        int int13 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str15 = funcionCuadratica14.toString();
        java.lang.Class<?> wildcardClass16 = funcionCuadratica14.getClass();
        java.lang.Class<?> wildcardClass17 = funcionCuadratica14.getClass();
        java.lang.Object obj18 = funcionCuadratica14.raices();
        int int19 = funcionCuadratica14.numRaices();
        boolean boolean20 = funcionCuadratica0.equals(funcionCuadratica14);
        float float22 = funcionCuadratica0.eval((float) 10);
        java.lang.Object obj23 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "1.0x^2" + "'", str15.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(obj18);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1 + "'", int19 == 1);
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 199.0f + "'", float22 == 199.0f);
        org.junit.Assert.assertNotNull(obj23);
    }

    @Test
    public void test140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test140");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        int int10 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
    }

    @Test
    public void test141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test141");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        java.lang.String str8 = funcionCuadratica0.toString();
        int int9 = funcionCuadratica0.numRaices();
        int int10 = funcionCuadratica0.numRaices();
        float float11 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1 + "'", int10 == 1);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 0.0f + "'", float11 == 0.0f);
    }

    @Test
    public void test142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test142");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        int int4 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 0 + "'", int4 == 0);
    }

    @Test
    public void test143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test143");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        try {
            java.lang.Object obj15 = funcionCuadratica10.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
    }

    @Test
    public void test144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test144");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        java.lang.String str10 = funcionCuadratica0.toString();
        float float11 = funcionCuadratica0.getB();
        int int12 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 10.0f + "'", float11 == 10.0f);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
    }

    @Test
    public void test145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test145");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 10, (float) (short) 0, (float) (byte) 10);
        float float4 = funcionCuadratica3.determinante();
        float float6 = funcionCuadratica3.eval((float) (short) 0);
        try {
            java.lang.Object obj7 = funcionCuadratica3.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + (-400.0f) + "'", float4 == (-400.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 10.0f + "'", float6 == 10.0f);
    }

    @Test
    public void test146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test146");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        java.lang.String str19 = funcionCuadratica5.toString();
        float float21 = funcionCuadratica5.eval(1.0f);
        funcionCuadratica5.setC(121.0f);
        float float24 = funcionCuadratica5.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "1.0x^2+10.0x" + "'", str19.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 11.0f + "'", float21 == 11.0f);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 10.0f + "'", float24 == 10.0f);
    }

    @Test
    public void test147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test147");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        funcionCuadratica10.setC(89999.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
    }

    @Test
    public void test148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test148");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        try {
            java.lang.Object obj5 = funcionCuadratica0.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test149");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        funcionCuadratica0.setA((float) 10L);
        java.lang.String str22 = funcionCuadratica0.toString();
        funcionCuadratica0.setB((float) (short) 1);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "10.0x^2+10.0x" + "'", str22.equals("10.0x^2+10.0x"));
    }

    @Test
    public void test150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test150");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        float float4 = funcionCuadratica3.getC();
        float float6 = funcionCuadratica3.eval((float) (byte) 10);
        float float7 = funcionCuadratica3.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 90.0f + "'", float6 == 90.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 1.0f + "'", float7 == 1.0f);
    }

    @Test
    public void test151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test151");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        boolean boolean24 = funcionCuadratica0.equals(funcionCuadratica15);
        java.lang.String str25 = funcionCuadratica15.toString();
        java.lang.String str26 = funcionCuadratica15.toString();
        funcionCuadratica15.setA(500.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + true + "'", boolean24 == true);
        org.junit.Assert.assertTrue("'" + str25 + "' != '" + "1.0x^2+10.0x" + "'", str25.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
    }

    @Test
    public void test152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test152");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 10, 1.0f, 90.0f);
        java.lang.String str4 = funcionCuadratica3.toString();
        try {
            java.lang.Object obj5 = funcionCuadratica3.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "10.0x^2+1.0x+90.0" + "'", str4.equals("10.0x^2+1.0x+90.0"));
    }

    @Test
    public void test153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test153");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        java.lang.String str10 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.eval(0.0f);
        float float13 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 10.0f + "'", float13 == 10.0f);
    }

    @Test
    public void test154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test154");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        funcionCuadratica0.setB(32.0f);
        java.lang.Class<?> wildcardClass20 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test155");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.determinante();
        funcionCuadratica0.setA(1.00200096E8f);
        float float7 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 0.0f + "'", float3 == 0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 1.00200096E8f + "'", float7 == 1.00200096E8f);
    }

    @Test
    public void test156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test156");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.0f, (float) (short) 1, (-9.0f));
        funcionCuadratica3.setC(32.0f);
    }

    @Test
    public void test157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test157");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        java.lang.String str9 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        java.lang.Class<?> wildcardClass12 = funcionCuadratica10.getClass();
        funcionCuadratica10.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float24 = funcionCuadratica15.eval((float) 10L);
        int int25 = funcionCuadratica15.numRaices();
        java.lang.String str26 = funcionCuadratica15.toString();
        float float27 = funcionCuadratica15.getB();
        boolean boolean28 = funcionCuadratica10.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica29 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica29.setB((float) 10);
        float float33 = funcionCuadratica29.eval((float) 10);
        float float35 = funcionCuadratica29.eval((-1.0f));
        int int36 = funcionCuadratica29.numRaices();
        float float38 = funcionCuadratica29.eval((float) 10L);
        funcionCuadratica29.setA((float) (byte) 100);
        funcionCuadratica29.setC((float) (byte) 1);
        funcionCuadratica29.setC((float) (short) 10);
        int int45 = funcionCuadratica29.numRaices();
        boolean boolean46 = funcionCuadratica15.equals(funcionCuadratica29);
        boolean boolean47 = funcionCuadratica3.equals(funcionCuadratica29);
        funcionCuadratica3.setA((-10.0f));
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "10.0x^2+1.0x" + "'", str9.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 200.0f + "'", float24 == 200.0f);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 10.0f + "'", float27 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 200.0f + "'", float33 == 200.0f);
        org.junit.Assert.assertTrue("'" + float35 + "' != '" + (-9.0f) + "'", float35 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 2 + "'", int36 == 2);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 200.0f + "'", float38 == 200.0f);
        org.junit.Assert.assertTrue("'" + int45 + "' != '" + 0 + "'", int45 == 0);
        org.junit.Assert.assertTrue("'" + boolean46 + "' != '" + false + "'", boolean46 == false);
        org.junit.Assert.assertTrue("'" + boolean47 + "' != '" + false + "'", boolean47 == false);
    }

    @Test
    public void test158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test158");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getB();
        funcionCuadratica3.setA(20100.0f);
        java.lang.String str12 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "20100.0x^2+1.0x" + "'", str12.equals("20100.0x^2+1.0x"));
    }

    @Test
    public void test159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test159");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(11.0f, (float) 0L, (float) 100);
    }

    @Test
    public void test160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test160");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        java.lang.String str7 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "100110.0x^2+-1.0x+11.0" + "'", str7.equals("100110.0x^2+-1.0x+11.0"));
    }

    @Test
    public void test161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test161");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        boolean boolean11 = funcionCuadratica3.equals(funcionCuadratica10);
        java.lang.Object obj12 = funcionCuadratica10.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica13 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str14 = funcionCuadratica13.toString();
        funcionCuadratica13.setC((float) 10);
        java.lang.Class<?> wildcardClass17 = funcionCuadratica13.getClass();
        funcionCuadratica13.setC(0.0f);
        funcionCuadratica13.setB(20100.0f);
        boolean boolean22 = funcionCuadratica10.equals(funcionCuadratica13);
        float float24 = funcionCuadratica10.eval((-108.0f));
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + boolean11 + "' != '" + false + "'", boolean11 == false);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "1.0x^2" + "'", str14.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + boolean22 + "' != '" + false + "'", boolean22 == false);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 11772.0f + "'", float24 == 11772.0f);
    }

    @Test
    public void test162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test162");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        java.lang.String str19 = funcionCuadratica0.toString();
        float float20 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "1.0x^2+-1.0" + "'", str19.equals("1.0x^2+-1.0"));
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 4.0f + "'", float20 == 4.0f);
    }

    @Test
    public void test163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test163");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        funcionCuadratica0.setA(2.13428676E17f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
    }

    @Test
    public void test164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test164");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        boolean boolean11 = funcionCuadratica3.equals(funcionCuadratica10);
        float float13 = funcionCuadratica10.eval(1.00200096E8f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + boolean11 + "' != '" + false + "'", boolean11 == false);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 1.00400594E16f + "'", float13 == 1.00400594E16f);
    }

    @Test
    public void test165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test165");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getB();
        funcionCuadratica3.setA(20100.0f);
        float float13 = funcionCuadratica3.eval(9000.0f);
        float float14 = funcionCuadratica3.getC();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 1.62809997E12f + "'", float13 == 1.62809997E12f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 0.0f + "'", float14 == 0.0f);
    }

    @Test
    public void test166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test166");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setB(121.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
    }

    @Test
    public void test167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test167");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) (byte) 100);
        funcionCuadratica0.setB(0.0f);
        java.lang.Class<?> wildcardClass12 = funcionCuadratica0.getClass();
        float float14 = funcionCuadratica0.eval(1.60359798E15f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 2.5715264E30f + "'", float14 == 2.5715264E30f);
    }

    @Test
    public void test168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test168");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setA((float) 2);
        float float9 = funcionCuadratica3.eval(100.0f);
        java.lang.Class<?> wildcardClass10 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 20100.0f + "'", float9 == 20100.0f);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test169");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        int int9 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
    }

    @Test
    public void test170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test170");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        java.lang.String str9 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        java.lang.Class<?> wildcardClass12 = funcionCuadratica10.getClass();
        funcionCuadratica10.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float24 = funcionCuadratica15.eval((float) 10L);
        int int25 = funcionCuadratica15.numRaices();
        java.lang.String str26 = funcionCuadratica15.toString();
        float float27 = funcionCuadratica15.getB();
        boolean boolean28 = funcionCuadratica10.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica29 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica29.setB((float) 10);
        float float33 = funcionCuadratica29.eval((float) 10);
        float float35 = funcionCuadratica29.eval((-1.0f));
        int int36 = funcionCuadratica29.numRaices();
        float float38 = funcionCuadratica29.eval((float) 10L);
        funcionCuadratica29.setA((float) (byte) 100);
        funcionCuadratica29.setC((float) (byte) 1);
        funcionCuadratica29.setC((float) (short) 10);
        int int45 = funcionCuadratica29.numRaices();
        boolean boolean46 = funcionCuadratica15.equals(funcionCuadratica29);
        boolean boolean47 = funcionCuadratica3.equals(funcionCuadratica29);
        java.lang.String str48 = funcionCuadratica3.toString();
        funcionCuadratica3.setB(1.62809997E12f);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "10.0x^2+1.0x" + "'", str9.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 200.0f + "'", float24 == 200.0f);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 10.0f + "'", float27 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 200.0f + "'", float33 == 200.0f);
        org.junit.Assert.assertTrue("'" + float35 + "' != '" + (-9.0f) + "'", float35 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 2 + "'", int36 == 2);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 200.0f + "'", float38 == 200.0f);
        org.junit.Assert.assertTrue("'" + int45 + "' != '" + 0 + "'", int45 == 0);
        org.junit.Assert.assertTrue("'" + boolean46 + "' != '" + false + "'", boolean46 == false);
        org.junit.Assert.assertTrue("'" + boolean47 + "' != '" + false + "'", boolean47 == false);
        org.junit.Assert.assertTrue("'" + str48 + "' != '" + "10.0x^2+1.0x" + "'", str48.equals("10.0x^2+1.0x"));
    }

    @Test
    public void test171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test171");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10.0f, 0.0f, (float) 1L);
        int int4 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setC(2.13428676E17f);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 0 + "'", int4 == 0);
    }

    @Test
    public void test172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test172");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 10, 1.0f, 90.0f);
        float float4 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
    }

    @Test
    public void test173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test173");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
    }

    @Test
    public void test174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test174");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getB();
        float float10 = funcionCuadratica3.getA();
        funcionCuadratica3.setB(20100.0f);
        java.lang.Object obj13 = funcionCuadratica3.raices();
        funcionCuadratica3.setB(179700.0f);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 10.0f + "'", float10 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
    }

    @Test
    public void test175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test175");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        funcionCuadratica0.setB(11.0f);
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 11.0f + "'", float8 == 11.0f);
    }

    @Test
    public void test176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test176");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        int int3 = funcionCuadratica0.numRaices();
        float float4 = funcionCuadratica0.getB();
        float float5 = funcionCuadratica0.getC();
        funcionCuadratica0.setC((float) (byte) 10);
        try {
            funcionCuadratica0.setA((float) (byte) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 2 + "'", int3 == 2);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 10.0f + "'", float4 == 10.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
    }

    @Test
    public void test177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test177");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.String str4 = funcionCuadratica0.toString();
        float float6 = funcionCuadratica0.eval(8.1046128E7f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "1.0x^2" + "'", str4.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 6.5684749E15f + "'", float6 == 6.5684749E15f);
    }

    @Test
    public void test178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test178");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        float float10 = funcionCuadratica0.getA();
        java.lang.Object obj11 = funcionCuadratica0.raices();
        float float12 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 1.0f + "'", float10 == 1.0f);
        org.junit.Assert.assertNotNull(obj11);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
    }

    @Test
    public void test179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test179");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-4.0f), 100110.0f, 8.1090048E7f);
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        java.lang.String str9 = funcionCuadratica4.toString();
        funcionCuadratica4.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica12 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica12.setB((float) 10);
        float float16 = funcionCuadratica12.eval((float) 10);
        float float18 = funcionCuadratica12.eval((-1.0f));
        int int19 = funcionCuadratica12.numRaices();
        float float21 = funcionCuadratica12.eval((float) 10L);
        float float22 = funcionCuadratica12.getB();
        boolean boolean23 = funcionCuadratica4.equals(funcionCuadratica12);
        java.lang.Object obj24 = funcionCuadratica12.raices();
        float float25 = funcionCuadratica12.getB();
        float float26 = funcionCuadratica12.getA();
        float float27 = funcionCuadratica12.determinante();
        boolean boolean28 = funcionCuadratica3.equals(funcionCuadratica12);
        funcionCuadratica3.setB(Float.POSITIVE_INFINITY);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "1.0x^2+10.0x" + "'", str9.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + 200.0f + "'", float16 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + (-9.0f) + "'", float18 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2 + "'", int19 == 2);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 200.0f + "'", float21 == 200.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 10.0f + "'", float22 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean23 + "' != '" + false + "'", boolean23 == false);
        org.junit.Assert.assertNotNull(obj24);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 10.0f + "'", float25 == 10.0f);
        org.junit.Assert.assertTrue("'" + float26 + "' != '" + 1.0f + "'", float26 == 1.0f);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 100.0f + "'", float27 == 100.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
    }

    @Test
    public void test180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test180");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10010.0f, (float) (-1), (-300.0f));
        funcionCuadratica3.setB(0.0f);
    }

    @Test
    public void test181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test181");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        funcionCuadratica0.setA((float) 10L);
        java.lang.Object obj22 = funcionCuadratica0.raices();
        java.lang.Object obj23 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertNotNull(obj22);
        org.junit.Assert.assertNotNull(obj23);
    }

    @Test
    public void test182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test182");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        float float4 = funcionCuadratica3.getC();
        java.lang.String str5 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica6 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica6.setB((float) 10);
        float float10 = funcionCuadratica6.eval((float) 10);
        int int11 = funcionCuadratica6.numRaices();
        boolean boolean12 = funcionCuadratica3.equals(funcionCuadratica6);
        float float13 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+-1.0x" + "'", str5.equals("1.0x^2+-1.0x"));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 200.0f + "'", float10 == 200.0f);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 2 + "'", int11 == 2);
        org.junit.Assert.assertTrue("'" + boolean12 + "' != '" + false + "'", boolean12 == false);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + (-1.0f) + "'", float13 == (-1.0f));
    }

    @Test
    public void test183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test183");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.00200096E8f, 200.0f, (float) 2);
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        int int11 = funcionCuadratica4.numRaices();
        float float13 = funcionCuadratica4.eval((float) 10L);
        funcionCuadratica4.setA((float) (byte) 100);
        funcionCuadratica4.setC((float) (byte) 1);
        float float18 = funcionCuadratica4.getB();
        float float19 = funcionCuadratica4.determinante();
        int int20 = funcionCuadratica4.numRaices();
        boolean boolean21 = funcionCuadratica3.equals(funcionCuadratica4);
        java.lang.String str22 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 2 + "'", int11 == 2);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 200.0f + "'", float13 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + (-300.0f) + "'", float19 == (-300.0f));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20 == 0);
        org.junit.Assert.assertTrue("'" + boolean21 + "' != '" + false + "'", boolean21 == false);
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.00200096E8x^2+200.0x+2.0" + "'", str22.equals("1.00200096E8x^2+200.0x+2.0"));
    }

    @Test
    public void test184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test184");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float7 = funcionCuadratica3.eval((float) 2);
        float float8 = funcionCuadratica3.getC();
        float float9 = funcionCuadratica3.determinante();
        float float10 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 400449.0f + "'", float7 == 400449.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 11.0f + "'", float8 == 11.0f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + (-4404839.0f) + "'", float9 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 100110.0f + "'", float10 == 100110.0f);
    }

    @Test
    public void test185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test185");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        float float9 = funcionCuadratica3.eval(1.0f);
        int int10 = funcionCuadratica3.numRaices();
        float float12 = funcionCuadratica3.eval((float) (-1L));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 11.0f + "'", float9 == 11.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 9.0f + "'", float12 == 9.0f);
    }

    @Test
    public void test186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test186");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getC();
        float float5 = funcionCuadratica0.getA();
        float float6 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 0.0f + "'", float3 == 0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 1.0f + "'", float5 == 1.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 1.0f + "'", float6 == 1.0f);
    }

    @Test
    public void test187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test187");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        float float19 = funcionCuadratica14.eval(0.0f);
        java.lang.String str20 = funcionCuadratica14.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 0.0f + "'", float19 == 0.0f);
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "10.0x^2+1.0x" + "'", str20.equals("10.0x^2+1.0x"));
    }

    @Test
    public void test188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test188");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        int int13 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str15 = funcionCuadratica14.toString();
        java.lang.Class<?> wildcardClass16 = funcionCuadratica14.getClass();
        java.lang.Class<?> wildcardClass17 = funcionCuadratica14.getClass();
        java.lang.Object obj18 = funcionCuadratica14.raices();
        int int19 = funcionCuadratica14.numRaices();
        boolean boolean20 = funcionCuadratica0.equals(funcionCuadratica14);
        float float21 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "1.0x^2" + "'", str15.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(obj18);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1 + "'", int19 == 1);
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-1.0f) + "'", float21 == (-1.0f));
    }

    @Test
    public void test189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test189");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        float float5 = funcionCuadratica0.getB();
        java.lang.Object obj6 = funcionCuadratica0.raices();
        java.lang.String str7 = funcionCuadratica0.toString();
        int int8 = funcionCuadratica0.numRaices();
        int int9 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2" + "'", str7.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8 == 1);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
    }

    @Test
    public void test190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test190");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setC(10000.0f);
        try {
            java.lang.Object obj11 = funcionCuadratica3.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
    }

    @Test
    public void test191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test191");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) (-1L));
        funcionCuadratica0.setB((float) (short) -1);
        float float7 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + (-9.0f) + "'", float4 == (-9.0f));
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + (-1.0f) + "'", float7 == (-1.0f));
    }

    @Test
    public void test192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test192");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        int int10 = funcionCuadratica0.numRaices();
        float float11 = funcionCuadratica0.getB();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica15.setB((float) 1);
        funcionCuadratica15.setB((float) (byte) 1);
        int int20 = funcionCuadratica15.numRaices();
        funcionCuadratica15.setA((float) (short) 1);
        funcionCuadratica15.setB((float) 2);
        funcionCuadratica15.setC((float) (short) 100);
        boolean boolean27 = funcionCuadratica0.equals(funcionCuadratica15);
        try {
            java.lang.Object obj28 = funcionCuadratica15.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 10.0f + "'", float11 == 10.0f);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2 + "'", int20 == 2);
        org.junit.Assert.assertTrue("'" + boolean27 + "' != '" + false + "'", boolean27 == false);
    }

    @Test
    public void test193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test193");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        java.lang.String str9 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        java.lang.Class<?> wildcardClass12 = funcionCuadratica10.getClass();
        funcionCuadratica10.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float24 = funcionCuadratica15.eval((float) 10L);
        int int25 = funcionCuadratica15.numRaices();
        java.lang.String str26 = funcionCuadratica15.toString();
        float float27 = funcionCuadratica15.getB();
        boolean boolean28 = funcionCuadratica10.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica29 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica29.setB((float) 10);
        float float33 = funcionCuadratica29.eval((float) 10);
        float float35 = funcionCuadratica29.eval((-1.0f));
        int int36 = funcionCuadratica29.numRaices();
        float float38 = funcionCuadratica29.eval((float) 10L);
        funcionCuadratica29.setA((float) (byte) 100);
        funcionCuadratica29.setC((float) (byte) 1);
        funcionCuadratica29.setC((float) (short) 10);
        int int45 = funcionCuadratica29.numRaices();
        boolean boolean46 = funcionCuadratica15.equals(funcionCuadratica29);
        boolean boolean47 = funcionCuadratica3.equals(funcionCuadratica29);
        float float48 = funcionCuadratica3.getA();
        funcionCuadratica3.setB((-9.0f));
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "10.0x^2+1.0x" + "'", str9.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 200.0f + "'", float24 == 200.0f);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 10.0f + "'", float27 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 200.0f + "'", float33 == 200.0f);
        org.junit.Assert.assertTrue("'" + float35 + "' != '" + (-9.0f) + "'", float35 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 2 + "'", int36 == 2);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 200.0f + "'", float38 == 200.0f);
        org.junit.Assert.assertTrue("'" + int45 + "' != '" + 0 + "'", int45 == 0);
        org.junit.Assert.assertTrue("'" + boolean46 + "' != '" + false + "'", boolean46 == false);
        org.junit.Assert.assertTrue("'" + boolean47 + "' != '" + false + "'", boolean47 == false);
        org.junit.Assert.assertTrue("'" + float48 + "' != '" + 10.0f + "'", float48 == 10.0f);
    }

    @Test
    public void test194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test194");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getA();
        float float5 = funcionCuadratica0.getC();
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica((-1.0f), (float) 100, (float) 2);
        boolean boolean10 = funcionCuadratica0.equals(funcionCuadratica9);
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica14);
        funcionCuadratica0.setB(1.60363397E11f);
        float float18 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.eval(0.0f);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean10 + "' != '" + false + "'", boolean10 == false);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 2.5716419E22f + "'", float18 == 2.5716419E22f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 0.0f + "'", float20 == 0.0f);
    }

    @Test
    public void test195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test195");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.determinante();
        java.lang.Object obj5 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 0.0f + "'", float3 == 0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertNotNull(obj5);
    }

    @Test
    public void test196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test196");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1575.0f, 0.0f, (float) 10L);
    }

    @Test
    public void test197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test197");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        java.lang.Object obj6 = funcionCuadratica0.raices();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test198");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        float float5 = funcionCuadratica0.getB();
        java.lang.Object obj6 = funcionCuadratica0.raices();
        java.lang.String str7 = funcionCuadratica0.toString();
        int int8 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2" + "'", str7.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8 == 1);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
    }

    @Test
    public void test199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test199");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) (byte) 100);
        int int10 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setA((float) '4');
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
    }

    @Test
    public void test200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test200");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-40.0f));
        java.lang.Class<?> wildcardClass23 = funcionCuadratica0.getClass();
        float float24 = funcionCuadratica0.getA();
        float float25 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 10.0f + "'", float20 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + (-40.0f) + "'", float24 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 10.0f + "'", float25 == 10.0f);
    }

    @Test
    public void test201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test201");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        int int3 = funcionCuadratica0.numRaices();
        float float4 = funcionCuadratica0.getB();
        float float5 = funcionCuadratica0.getC();
        float float6 = funcionCuadratica0.getC();
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 2 + "'", int3 == 2);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 10.0f + "'", float4 == 10.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 1.0f + "'", float8 == 1.0f);
    }

    @Test
    public void test202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test202");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.getB();
        int int20 = funcionCuadratica0.numRaices();
        java.lang.String str21 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 10.0f + "'", float19 == 10.0f);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2 + "'", int20 == 2);
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2+10.0x" + "'", str21.equals("1.0x^2+10.0x"));
    }

    @Test
    public void test203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test203");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        float float10 = funcionCuadratica0.getA();
        funcionCuadratica0.setC((float) 100);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 1.0f + "'", float10 == 1.0f);
    }

    @Test
    public void test204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test204");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        java.lang.String str17 = funcionCuadratica10.toString();
        funcionCuadratica10.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str21 = funcionCuadratica20.toString();
        funcionCuadratica20.setC((float) 10);
        boolean boolean24 = funcionCuadratica10.equals(funcionCuadratica20);
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica25.setB((float) 10);
        float float29 = funcionCuadratica25.eval((float) 10);
        float float31 = funcionCuadratica25.eval((-1.0f));
        java.lang.String str32 = funcionCuadratica25.toString();
        float float33 = funcionCuadratica25.getB();
        boolean boolean34 = funcionCuadratica10.equals(funcionCuadratica25);
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica10);
        funcionCuadratica0.setA(1.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "1.0x^2+10.0x" + "'", str17.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2" + "'", str21.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 200.0f + "'", float29 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + (-9.0f) + "'", float31 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + true + "'", boolean34 == true);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + true + "'", boolean35 == true);
    }

    @Test
    public void test205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test205");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        java.lang.Object obj5 = funcionCuadratica0.raices();
        java.lang.Class<?> wildcardClass6 = obj5.getClass();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertNotNull(obj5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test206");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.determinante();
        float float6 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) 0);
        int int9 = funcionCuadratica0.numRaices();
        float float10 = funcionCuadratica0.getB();
        java.lang.Object obj11 = funcionCuadratica0.raices();
        java.lang.Class<?> wildcardClass12 = obj11.getClass();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + (-40.0f) + "'", float5 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertNotNull(obj11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test207");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass6 = funcionCuadratica0.getClass();
        float float7 = funcionCuadratica0.determinante();
        float float8 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 100.0f + "'", float7 == 100.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
    }

    @Test
    public void test208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test208");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10010.0f, 200.0f, 8.1206009E12f);
    }

    @Test
    public void test209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test209");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(121.0f, 2.13428676E17f, 2.5715264E30f);
        java.lang.String str4 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "121.0x^2+2.13428676E17x+2.5715264E30" + "'", str4.equals("121.0x^2+2.13428676E17x+2.5715264E30"));
    }

    @Test
    public void test210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test210");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getB();
        funcionCuadratica3.setA(20100.0f);
        float float13 = funcionCuadratica3.eval(20100.0f);
        int int14 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setC(9000.0f);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 8.1206009E12f + "'", float13 == 8.1206009E12f);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14 == 2);
    }

    @Test
    public void test211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test211");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        java.lang.String str8 = funcionCuadratica0.toString();
        int int9 = funcionCuadratica0.numRaices();
        int int10 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setB((float) (short) 10);
        java.lang.String str13 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1 + "'", int10 == 1);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "1.0x^2+10.0x" + "'", str13.equals("1.0x^2+10.0x"));
    }

    @Test
    public void test212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test212");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        funcionCuadratica3.setB(200.0f);
        float float9 = funcionCuadratica3.eval((-1.0f));
        funcionCuadratica3.setA(4.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 99921.0f + "'", float9 == 99921.0f);
    }

    @Test
    public void test213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test213");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-300.0f), (float) (short) -1, (float) 1L);
        float float4 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + (-1.0f) + "'", float4 == (-1.0f));
    }

    @Test
    public void test214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test214");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(99921.0f, 0.0f, (float) 2);
        int int4 = funcionCuadratica3.numRaices();
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 0 + "'", int4 == 0);
    }

    @Test
    public void test215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test215");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10000.0f, 10010.0f, 0.0f);
        float float4 = funcionCuadratica3.determinante();
        java.lang.Class<?> wildcardClass5 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.00200096E8f + "'", float4 == 1.00200096E8f);
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test216");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        int int9 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setC((-40.0f));
        funcionCuadratica0.setB(400449.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
    }

    @Test
    public void test217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test217");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica8);
        funcionCuadratica0.setC((-1.0286567E25f));
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
    }

    @Test
    public void test218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test218");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.16756784E8f, 6.5684749E15f, 1.60359798E15f);
        java.lang.String str4 = funcionCuadratica3.toString();
        funcionCuadratica3.setA(1.02512616E8f);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "1.16756784E8x^2+6.5684749E15x+1.60359798E15" + "'", str4.equals("1.16756784E8x^2+6.5684749E15x+1.60359798E15"));
    }

    @Test
    public void test219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test219");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Object obj2 = funcionCuadratica0.raices();
        java.lang.String str3 = funcionCuadratica0.toString();
        funcionCuadratica0.setB(20100.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(obj2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "1.0x^2" + "'", str3.equals("1.0x^2"));
    }

    @Test
    public void test220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test220");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getB();
        funcionCuadratica3.setA(20100.0f);
        java.lang.Class<?> wildcardClass12 = funcionCuadratica3.getClass();
        float float13 = funcionCuadratica3.getC();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 0.0f + "'", float13 == 0.0f);
    }

    @Test
    public void test221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test221");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        java.lang.String str10 = funcionCuadratica0.toString();
        float float11 = funcionCuadratica0.getB();
        float float12 = funcionCuadratica0.getA();
        java.lang.Object obj13 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 10.0f + "'", float11 == 10.0f);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 1.0f + "'", float12 == 1.0f);
        org.junit.Assert.assertNotNull(obj13);
    }

    @Test
    public void test222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test222");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) (byte) 100);
        funcionCuadratica0.setB(0.0f);
        java.lang.Class<?> wildcardClass12 = funcionCuadratica0.getClass();
        try {
            java.lang.Object obj13 = funcionCuadratica0.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test223");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        java.lang.String str11 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.getC();
        funcionCuadratica0.setA((-4404839.0f));
        funcionCuadratica0.setC((float) '4');
        funcionCuadratica0.setB((-300.0f));
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
    }

    @Test
    public void test224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test224");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        java.lang.Object obj8 = funcionCuadratica0.raices();
        float float9 = funcionCuadratica0.getC();
        funcionCuadratica0.setC(20100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(obj8);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
    }

    @Test
    public void test225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test225");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        funcionCuadratica0.setB(11.0f);
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+11.0x" + "'", str8.equals("1.0x^2+11.0x"));
    }

    @Test
    public void test226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test226");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setA((float) 2);
        float float9 = funcionCuadratica3.eval((-300.0f));
        java.lang.String str10 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 179700.0f + "'", float9 == 179700.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "2.0x^2+1.0x" + "'", str10.equals("2.0x^2+1.0x"));
    }

    @Test
    public void test227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test227");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        java.lang.Object obj20 = funcionCuadratica8.raices();
        float float21 = funcionCuadratica8.getB();
        float float22 = funcionCuadratica8.determinante();
        java.lang.String str23 = funcionCuadratica8.toString();
        float float25 = funcionCuadratica8.eval(1.99685125E12f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertNotNull(obj20);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 10.0f + "'", float21 == 10.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 100.0f + "'", float22 == 100.0f);
        org.junit.Assert.assertTrue("'" + str23 + "' != '" + "1.0x^2+10.0x" + "'", str23.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 3.987415E24f + "'", float25 == 3.987415E24f);
    }

    @Test
    public void test228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test228");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        java.lang.Object obj5 = funcionCuadratica0.raices();
        float float6 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertNotNull(obj5);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 1.0f + "'", float6 == 1.0f);
    }

    @Test
    public void test229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test229");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        java.lang.String str8 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(Float.POSITIVE_INFINITY);
        funcionCuadratica0.setC(1.99685125E12f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
    }

    @Test
    public void test230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test230");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 1);
        funcionCuadratica0.setA(6578021.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
    }

    @Test
    public void test231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test231");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getA();
        float float7 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 0 + "'", int5 == 0);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 1.0f + "'", float6 == 1.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 0.0f + "'", float7 == 0.0f);
    }

    @Test
    public void test232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test232");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) '#', (float) 'a', (float) (short) 1);
        float float4 = funcionCuadratica3.getC();
        funcionCuadratica3.setB((float) 100L);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
    }

    @Test
    public void test233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test233");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float23 = funcionCuadratica15.getB();
        int int24 = funcionCuadratica15.numRaices();
        funcionCuadratica15.setB((-40.0f));
        float float27 = funcionCuadratica15.getC();
        boolean boolean28 = funcionCuadratica0.equals(funcionCuadratica15);
        float float30 = funcionCuadratica15.eval(11772.0f);
        float float31 = funcionCuadratica15.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2 + "'", int24 == 2);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 0.0f + "'", float27 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + float30 + "' != '" + 1.38109104E8f + "'", float30 == 1.38109104E8f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + 1.0f + "'", float31 == 1.0f);
    }

    @Test
    public void test234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test234");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        float float20 = funcionCuadratica0.eval((-300.0f));
        int int21 = funcionCuadratica0.numRaices();
        int int22 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 89999.0f + "'", float20 == 89999.0f);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
    }

    @Test
    public void test235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test235");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.Object obj7 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertNotNull(obj7);
    }

    @Test
    public void test236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test236");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        float float4 = funcionCuadratica0.eval(11772.0f);
        float float6 = funcionCuadratica0.eval(1575.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.38579984E8f + "'", float4 == 1.38579984E8f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 2480625.0f + "'", float6 == 2480625.0f);
    }

    @Test
    public void test237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test237");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.determinante();
        float float6 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) 0);
        java.lang.Object obj9 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + (-40.0f) + "'", float5 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(obj9);
    }

    @Test
    public void test238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test238");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10L, (float) 1, (float) 10L);
        float float5 = funcionCuadratica3.eval((float) (byte) 100);
        byThey.practico5.FuncionCuadratica funcionCuadratica6 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str7 = funcionCuadratica6.toString();
        java.lang.Class<?> wildcardClass8 = funcionCuadratica6.getClass();
        funcionCuadratica6.setC((float) (-1L));
        funcionCuadratica6.setB(11000.0f);
        float float13 = funcionCuadratica6.getA();
        java.lang.Class<?> wildcardClass14 = funcionCuadratica6.getClass();
        funcionCuadratica6.setC(9000.0f);
        boolean boolean17 = funcionCuadratica3.equals(funcionCuadratica6);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 100110.0f + "'", float5 == 100110.0f);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2" + "'", str7.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 1.0f + "'", float13 == 1.0f);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
    }

    @Test
    public void test239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test239");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica7.setB((float) 10);
        float float11 = funcionCuadratica7.eval((float) 10);
        float float13 = funcionCuadratica7.eval((-1.0f));
        java.lang.String str14 = funcionCuadratica7.toString();
        funcionCuadratica7.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica17 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str18 = funcionCuadratica17.toString();
        funcionCuadratica17.setC((float) 10);
        boolean boolean21 = funcionCuadratica7.equals(funcionCuadratica17);
        int int22 = funcionCuadratica17.numRaices();
        boolean boolean23 = funcionCuadratica0.equals(funcionCuadratica17);
        funcionCuadratica17.setC(4.04009984E8f);
        funcionCuadratica17.setA(121.0f);
        funcionCuadratica17.setB((-10.0f));
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 200.0f + "'", float11 == 200.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + (-9.0f) + "'", float13 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "1.0x^2+10.0x" + "'", str14.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2" + "'", str18.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean21 + "' != '" + false + "'", boolean21 == false);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22 == 0);
        org.junit.Assert.assertTrue("'" + boolean23 + "' != '" + false + "'", boolean23 == false);
    }

    @Test
    public void test240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test240");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        int int4 = funcionCuadratica3.numRaices();
        float float5 = funcionCuadratica3.getA();
        int int6 = funcionCuadratica3.numRaices();
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2 + "'", int4 == 2);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 10.0f + "'", float5 == 10.0f);
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6 == 2);
    }

    @Test
    public void test241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test241");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-300.0f));
        funcionCuadratica0.setC((-1400376.0f));
        funcionCuadratica0.setA(1.00802796E33f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 10.0f + "'", float19 == 10.0f);
    }

    @Test
    public void test242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test242");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        int int11 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica(11000.0f, (float) '4', (float) 10L);
        float float17 = funcionCuadratica15.eval((-4404839.0f));
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica15);
        funcionCuadratica0.setB(1.02512616E8f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 2 + "'", int11 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 2.13428676E17f + "'", float17 == 2.13428676E17f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
    }

    @Test
    public void test243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test243");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 1);
        float float14 = funcionCuadratica0.getB();
        float float15 = funcionCuadratica0.determinante();
        java.lang.String str16 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 10.0f + "'", float14 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + (-300.0f) + "'", float15 == (-300.0f));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "100.0x^2+10.0x+1.0" + "'", str16.equals("100.0x^2+10.0x+1.0"));
    }

    @Test
    public void test244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test244");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        java.lang.String str17 = funcionCuadratica10.toString();
        funcionCuadratica10.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str21 = funcionCuadratica20.toString();
        funcionCuadratica20.setC((float) 10);
        boolean boolean24 = funcionCuadratica10.equals(funcionCuadratica20);
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica25.setB((float) 10);
        float float29 = funcionCuadratica25.eval((float) 10);
        float float31 = funcionCuadratica25.eval((-1.0f));
        java.lang.String str32 = funcionCuadratica25.toString();
        float float33 = funcionCuadratica25.getB();
        boolean boolean34 = funcionCuadratica10.equals(funcionCuadratica25);
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica10);
        funcionCuadratica10.setC(99921.0f);
        float float38 = funcionCuadratica10.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "1.0x^2+10.0x" + "'", str17.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2" + "'", str21.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 200.0f + "'", float29 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + (-9.0f) + "'", float31 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + true + "'", boolean34 == true);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + true + "'", boolean35 == true);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + (-399584.0f) + "'", float38 == (-399584.0f));
    }

    @Test
    public void test245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test245");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setC((float) 1L);
        float float6 = funcionCuadratica0.eval(1210010.0f);
        funcionCuadratica0.setB(2480625.0f);
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 1.46412418E12f + "'", float6 == 1.46412418E12f);
    }

    @Test
    public void test246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test246");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.getB();
        float float21 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 10.0f + "'", float20 == 10.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 0.0f + "'", float21 == 0.0f);
    }

    @Test
    public void test247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test247");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        int int3 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setA(100.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica((float) ' ', 0.0f, 99921.0f);
        boolean boolean10 = funcionCuadratica0.equals(funcionCuadratica9);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 1 + "'", int3 == 1);
        org.junit.Assert.assertTrue("'" + boolean10 + "' != '" + false + "'", boolean10 == false);
    }

    @Test
    public void test248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test248");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        java.lang.String str19 = funcionCuadratica5.toString();
        float float21 = funcionCuadratica5.eval(1.0f);
        funcionCuadratica5.setB((float) (short) 10);
        int int24 = funcionCuadratica5.numRaices();
        funcionCuadratica5.setC(2.5715264E30f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "1.0x^2+10.0x" + "'", str19.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 11.0f + "'", float21 == 11.0f);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2 + "'", int24 == 2);
    }

    @Test
    public void test249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test249");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        funcionCuadratica3.setB(200.0f);
        float float9 = funcionCuadratica3.eval((float) (byte) 100);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.00112E9f + "'", float9 == 1.00112E9f);
    }

    @Test
    public void test250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test250");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-4.0f), 100110.0f, 8.1090048E7f);
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        java.lang.String str9 = funcionCuadratica4.toString();
        funcionCuadratica4.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica12 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica12.setB((float) 10);
        float float16 = funcionCuadratica12.eval((float) 10);
        float float18 = funcionCuadratica12.eval((-1.0f));
        int int19 = funcionCuadratica12.numRaices();
        float float21 = funcionCuadratica12.eval((float) 10L);
        float float22 = funcionCuadratica12.getB();
        boolean boolean23 = funcionCuadratica4.equals(funcionCuadratica12);
        java.lang.Object obj24 = funcionCuadratica12.raices();
        float float25 = funcionCuadratica12.getB();
        float float26 = funcionCuadratica12.getA();
        float float27 = funcionCuadratica12.determinante();
        boolean boolean28 = funcionCuadratica3.equals(funcionCuadratica12);
        java.lang.Class<?> wildcardClass29 = funcionCuadratica12.getClass();
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "1.0x^2+10.0x" + "'", str9.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + 200.0f + "'", float16 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + (-9.0f) + "'", float18 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2 + "'", int19 == 2);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 200.0f + "'", float21 == 200.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 10.0f + "'", float22 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean23 + "' != '" + false + "'", boolean23 == false);
        org.junit.Assert.assertNotNull(obj24);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 10.0f + "'", float25 == 10.0f);
        org.junit.Assert.assertTrue("'" + float26 + "' != '" + 1.0f + "'", float26 == 1.0f);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 100.0f + "'", float27 == 100.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test251");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        java.lang.Object obj10 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(obj10);
    }

    @Test
    public void test252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test252");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 1);
        float float14 = funcionCuadratica0.getB();
        float float15 = funcionCuadratica0.determinante();
        funcionCuadratica0.setC(1.05088369E16f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 10.0f + "'", float14 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + (-300.0f) + "'", float15 == (-300.0f));
    }

    @Test
    public void test253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test253");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica3.getClass();
        java.lang.Class<?> wildcardClass5 = funcionCuadratica3.getClass();
        funcionCuadratica3.setA(179700.0f);
        int int8 = funcionCuadratica3.numRaices();
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
    }

    @Test
    public void test254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test254");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.00112E9f, 1.00802796E33f, 1.16756784E8f);
    }

    @Test
    public void test255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test255");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10L, (float) 1, (float) 10L);
        float float5 = funcionCuadratica3.eval((float) (byte) 100);
        java.lang.Class<?> wildcardClass6 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 100110.0f + "'", float5 == 100110.0f);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test256");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        int int9 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setB((-40.0f));
        float float13 = funcionCuadratica0.eval(1.00112E9f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 1.0022412E18f + "'", float13 == 1.0022412E18f);
    }

    @Test
    public void test257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test257");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) (byte) 0);
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str12 = funcionCuadratica11.toString();
        java.lang.Class<?> wildcardClass13 = funcionCuadratica11.getClass();
        float float14 = funcionCuadratica11.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        java.lang.Object obj24 = funcionCuadratica15.raices();
        int int25 = funcionCuadratica15.numRaices();
        boolean boolean26 = funcionCuadratica11.equals(funcionCuadratica15);
        float float27 = funcionCuadratica11.getC();
        boolean boolean28 = funcionCuadratica0.equals(funcionCuadratica11);
        int int29 = funcionCuadratica11.numRaices();
        java.lang.Object obj30 = funcionCuadratica11.raices();
        funcionCuadratica11.setC((-3780.0f));
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2" + "'", str12.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 1.0f + "'", float14 == 1.0f);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertNotNull(obj24);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + boolean26 + "' != '" + false + "'", boolean26 == false);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 0.0f + "'", float27 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1 + "'", int29 == 1);
        org.junit.Assert.assertNotNull(obj30);
    }

    @Test
    public void test258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test258");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        funcionCuadratica0.setB(11.0f);
        int int7 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setC(110.0f);
        java.lang.String str10 = funcionCuadratica0.toString();
        float float11 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+11.0x+110.0" + "'", str10.equals("1.0x^2+11.0x+110.0"));
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-319.0f) + "'", float11 == (-319.0f));
    }

    @Test
    public void test259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test259");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        java.lang.String str10 = funcionCuadratica0.toString();
        float float11 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-319.0f));
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 10.0f + "'", float11 == 10.0f);
    }

    @Test
    public void test260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test260");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float5 = funcionCuadratica0.eval((float) (byte) 100);
        java.lang.String str6 = funcionCuadratica0.toString();
        float float7 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11000.0f + "'", float5 == 11000.0f);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "1.0x^2+10.0x" + "'", str6.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 10.0f + "'", float7 == 10.0f);
    }

    @Test
    public void test261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test261");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        java.lang.Object obj8 = funcionCuadratica0.raices();
        float float10 = funcionCuadratica0.eval((float) 0L);
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str12 = funcionCuadratica11.toString();
        java.lang.Class<?> wildcardClass13 = funcionCuadratica11.getClass();
        float float14 = funcionCuadratica11.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        java.lang.Object obj24 = funcionCuadratica15.raices();
        int int25 = funcionCuadratica15.numRaices();
        boolean boolean26 = funcionCuadratica11.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica27 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica27.setB((float) 10);
        float float31 = funcionCuadratica27.eval((float) 10);
        float float33 = funcionCuadratica27.eval((-1.0f));
        int int34 = funcionCuadratica27.numRaices();
        float float36 = funcionCuadratica27.eval((float) 10L);
        int int37 = funcionCuadratica27.numRaices();
        boolean boolean38 = funcionCuadratica15.equals(funcionCuadratica27);
        boolean boolean39 = funcionCuadratica0.equals(funcionCuadratica15);
        java.lang.Object obj40 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(obj8);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2" + "'", str12.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 1.0f + "'", float14 == 1.0f);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertNotNull(obj24);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + boolean26 + "' != '" + false + "'", boolean26 == false);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + 200.0f + "'", float31 == 200.0f);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + (-9.0f) + "'", float33 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 2 + "'", int34 == 2);
        org.junit.Assert.assertTrue("'" + float36 + "' != '" + 200.0f + "'", float36 == 200.0f);
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 2 + "'", int37 == 2);
        org.junit.Assert.assertTrue("'" + boolean38 + "' != '" + true + "'", boolean38 == true);
        org.junit.Assert.assertTrue("'" + boolean39 + "' != '" + false + "'", boolean39 == false);
        org.junit.Assert.assertNotNull(obj40);
    }

    @Test
    public void test262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test262");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.determinante();
        funcionCuadratica0.setB(0.0f);
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 0.0f + "'", float3 == 0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
    }

    @Test
    public void test263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test263");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        float float10 = funcionCuadratica0.getB();
        float float12 = funcionCuadratica0.eval(100110.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 10.0f + "'", float10 == 10.0f);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 1.00230134E10f + "'", float12 == 1.00230134E10f);
    }

    @Test
    public void test264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test264");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        java.lang.Object obj20 = funcionCuadratica8.raices();
        float float21 = funcionCuadratica8.getB();
        float float22 = funcionCuadratica8.getA();
        float float23 = funcionCuadratica8.determinante();
        float float24 = funcionCuadratica8.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertNotNull(obj20);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 10.0f + "'", float21 == 10.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 1.0f + "'", float22 == 1.0f);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 100.0f + "'", float23 == 100.0f);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 1.0f + "'", float24 == 1.0f);
    }

    @Test
    public void test265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test265");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-40.0f));
        java.lang.Class<?> wildcardClass23 = funcionCuadratica0.getClass();
        int int24 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 10.0f + "'", float20 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2 + "'", int24 == 2);
    }

    @Test
    public void test266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test266");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        float float6 = funcionCuadratica0.getC();
        float float8 = funcionCuadratica0.eval((-1.0f));
        java.lang.Object obj9 = funcionCuadratica0.raices();
        java.lang.Class<?> wildcardClass10 = obj9.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + (-9.0f) + "'", float8 == (-9.0f));
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test267");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica7.setB((float) 10);
        float float11 = funcionCuadratica7.eval((float) 10);
        float float13 = funcionCuadratica7.eval((-1.0f));
        java.lang.String str14 = funcionCuadratica7.toString();
        funcionCuadratica7.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica17 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str18 = funcionCuadratica17.toString();
        funcionCuadratica17.setC((float) 10);
        boolean boolean21 = funcionCuadratica7.equals(funcionCuadratica17);
        int int22 = funcionCuadratica17.numRaices();
        boolean boolean23 = funcionCuadratica0.equals(funcionCuadratica17);
        funcionCuadratica17.setC(4.04009984E8f);
        funcionCuadratica17.setC(1.0022412E18f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 200.0f + "'", float11 == 200.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + (-9.0f) + "'", float13 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "1.0x^2+10.0x" + "'", str14.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2" + "'", str18.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean21 + "' != '" + false + "'", boolean21 == false);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22 == 0);
        org.junit.Assert.assertTrue("'" + boolean23 + "' != '" + false + "'", boolean23 == false);
    }

    @Test
    public void test268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test268");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(Float.POSITIVE_INFINITY, 179700.0f, (float) (byte) 1);
        float float4 = funcionCuadratica3.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
    }

    @Test
    public void test269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test269");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica3.getClass();
        funcionCuadratica3.setB(121.0f);
        float float7 = funcionCuadratica3.getA();
        funcionCuadratica3.setB(Float.POSITIVE_INFINITY);
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 10.0f + "'", float7 == 10.0f);
    }

    @Test
    public void test270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test270");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        java.lang.Object obj20 = funcionCuadratica8.raices();
        float float21 = funcionCuadratica8.getB();
        float float23 = funcionCuadratica8.eval(90.0f);
        float float24 = funcionCuadratica8.getB();
        float float25 = funcionCuadratica8.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertNotNull(obj20);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 10.0f + "'", float21 == 10.0f);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 9000.0f + "'", float23 == 9000.0f);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 10.0f + "'", float24 == 10.0f);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 100.0f + "'", float25 == 100.0f);
    }

    @Test
    public void test271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test271");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        java.lang.String str11 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.getC();
        java.lang.String str13 = funcionCuadratica0.toString();
        java.lang.String str14 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) ' ');
        float float17 = funcionCuadratica0.getA();
        java.lang.Class<?> wildcardClass18 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) 100L);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "1.0x^2+10.0x" + "'", str13.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "1.0x^2+10.0x" + "'", str14.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 32.0f + "'", float17 == 32.0f);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test272");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        java.lang.String str11 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.getC();
        java.lang.String str13 = funcionCuadratica0.toString();
        java.lang.String str14 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) ' ');
        float float17 = funcionCuadratica0.getA();
        java.lang.Class<?> wildcardClass18 = funcionCuadratica0.getClass();
        java.lang.Object obj19 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "1.0x^2+10.0x" + "'", str13.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "1.0x^2+10.0x" + "'", str14.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 32.0f + "'", float17 == 32.0f);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertNotNull(obj19);
    }

    @Test
    public void test273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test273");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.String str4 = funcionCuadratica3.toString();
        funcionCuadratica3.setA((float) '4');
        float float7 = funcionCuadratica3.getC();
        java.lang.String str8 = funcionCuadratica3.toString();
        float float10 = funcionCuadratica3.eval((float) (short) -1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "10.0x^2+100.0x" + "'", str4.equals("10.0x^2+100.0x"));
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 0.0f + "'", float7 == 0.0f);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "52.0x^2+100.0x" + "'", str8.equals("52.0x^2+100.0x"));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-48.0f) + "'", float10 == (-48.0f));
    }

    @Test
    public void test274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test274");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica3.getClass();
        java.lang.Class<?> wildcardClass5 = funcionCuadratica3.getClass();
        funcionCuadratica3.setC(0.0f);
        float float8 = funcionCuadratica3.getB();
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 100.0f + "'", float8 == 100.0f);
    }

    @Test
    public void test275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test275");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) '4');
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica11.setB((float) 10);
        float float15 = funcionCuadratica11.eval((float) 10);
        float float17 = funcionCuadratica11.eval((-1.0f));
        int int18 = funcionCuadratica11.numRaices();
        float float20 = funcionCuadratica11.eval((float) 10L);
        int int21 = funcionCuadratica11.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica25.setB((float) 1);
        boolean boolean28 = funcionCuadratica11.equals(funcionCuadratica25);
        java.lang.String str29 = funcionCuadratica11.toString();
        funcionCuadratica11.setC((-1.0f));
        boolean boolean32 = funcionCuadratica0.equals(funcionCuadratica11);
        int int33 = funcionCuadratica11.numRaices();
        java.lang.Class<?> wildcardClass34 = funcionCuadratica11.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 200.0f + "'", float15 == 200.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + (-9.0f) + "'", float17 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 200.0f + "'", float20 == 200.0f);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + str29 + "' != '" + "1.0x^2+10.0x" + "'", str29.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + boolean32 + "' != '" + false + "'", boolean32 == false);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 2 + "'", int33 == 2);
        org.junit.Assert.assertNotNull(wildcardClass34);
    }

    @Test
    public void test276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test276");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica0.getClass();
        float float12 = funcionCuadratica0.getA();
        float float13 = funcionCuadratica0.determinante();
        int int14 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 1.0f + "'", float12 == 1.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 100.0f + "'", float13 == 100.0f);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14 == 2);
    }

    @Test
    public void test277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test277");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        java.lang.String str17 = funcionCuadratica10.toString();
        funcionCuadratica10.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str21 = funcionCuadratica20.toString();
        funcionCuadratica20.setC((float) 10);
        boolean boolean24 = funcionCuadratica10.equals(funcionCuadratica20);
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica25.setB((float) 10);
        float float29 = funcionCuadratica25.eval((float) 10);
        float float31 = funcionCuadratica25.eval((-1.0f));
        java.lang.String str32 = funcionCuadratica25.toString();
        float float33 = funcionCuadratica25.getB();
        boolean boolean34 = funcionCuadratica10.equals(funcionCuadratica25);
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica10);
        java.lang.Object obj36 = funcionCuadratica0.raices();
        java.lang.String str37 = funcionCuadratica0.toString();
        java.lang.String str38 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "1.0x^2+10.0x" + "'", str17.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2" + "'", str21.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 200.0f + "'", float29 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + (-9.0f) + "'", float31 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + true + "'", boolean34 == true);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + true + "'", boolean35 == true);
        org.junit.Assert.assertNotNull(obj36);
        org.junit.Assert.assertTrue("'" + str37 + "' != '" + "1.0x^2+10.0x" + "'", str37.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str38 + "' != '" + "1.0x^2+10.0x" + "'", str38.equals("1.0x^2+10.0x"));
    }

    @Test
    public void test278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test278");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        float float10 = funcionCuadratica0.getA();
        java.lang.String str11 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 1.0f + "'", float10 == 1.0f);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
    }

    @Test
    public void test279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test279");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setA((float) 2);
        funcionCuadratica3.setC((float) ' ');
        java.lang.Class<?> wildcardClass10 = funcionCuadratica3.getClass();
        float float11 = funcionCuadratica3.getA();
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 2.0f + "'", float11 == 2.0f);
    }

    @Test
    public void test280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test280");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica3.getClass();
        java.lang.String str5 = funcionCuadratica3.toString();
        int int6 = funcionCuadratica3.numRaices();
        float float7 = funcionCuadratica3.getC();
        float float8 = funcionCuadratica3.getA();
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "10.0x^2+100.0x" + "'", str5.equals("10.0x^2+100.0x"));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6 == 2);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 0.0f + "'", float7 == 0.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
    }

    @Test
    public void test281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test281");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        java.lang.Object obj11 = funcionCuadratica0.raices();
        java.lang.Class<?> wildcardClass12 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertNotNull(obj11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test282");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        int int9 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica13 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        float float14 = funcionCuadratica13.getC();
        float float16 = funcionCuadratica13.eval((float) (byte) 10);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica13);
        float float18 = funcionCuadratica0.determinante();
        funcionCuadratica0.setA(2480625.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 0.0f + "'", float14 == 0.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + 90.0f + "'", float16 == 90.0f);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 100.0f + "'", float18 == 100.0f);
    }

    @Test
    public void test283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test283");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        java.lang.String str11 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.getB();
        float float13 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 10.0f + "'", float13 == 10.0f);
    }

    @Test
    public void test284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test284");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass15 = funcionCuadratica14.getClass();
        boolean boolean16 = funcionCuadratica0.equals(funcionCuadratica14);
        float float17 = funcionCuadratica14.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertTrue("'" + boolean16 + "' != '" + false + "'", boolean16 == false);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
    }

    @Test
    public void test285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test285");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10010.0f, (float) (-1), (-300.0f));
        funcionCuadratica3.setC((float) ' ');
        float float6 = funcionCuadratica3.determinante();
        funcionCuadratica3.setA(9.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-1281279.0f) + "'", float6 == (-1281279.0f));
    }

    @Test
    public void test286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test286");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setB(2.13428676E17f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
    }

    @Test
    public void test287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test287");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        int int5 = funcionCuadratica0.numRaices();
        java.lang.String str6 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 2 + "'", int5 == 2);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "1.0x^2+-1.0" + "'", str6.equals("1.0x^2+-1.0"));
    }

    @Test
    public void test288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test288");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        float float3 = funcionCuadratica0.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        java.lang.String str11 = funcionCuadratica4.toString();
        float float12 = funcionCuadratica4.getB();
        java.lang.Object obj13 = funcionCuadratica4.raices();
        int int14 = funcionCuadratica4.numRaices();
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica4);
        int int16 = funcionCuadratica0.numRaices();
        float float17 = funcionCuadratica0.determinante();
        funcionCuadratica0.setC(1.46412418E12f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 1.0f + "'", float3 == 1.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14 == 2);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1 + "'", int16 == 1);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 0.0f + "'", float17 == 0.0f);
    }

    @Test
    public void test289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test289");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        boolean boolean24 = funcionCuadratica0.equals(funcionCuadratica15);
        int int25 = funcionCuadratica15.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica29 = new byThey.practico5.FuncionCuadratica((float) 10L, (float) 1, (float) 10L);
        boolean boolean30 = funcionCuadratica15.equals(funcionCuadratica29);
        float float31 = funcionCuadratica29.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + true + "'", boolean24 == true);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + boolean30 + "' != '" + false + "'", boolean30 == false);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + 1.0f + "'", float31 == 1.0f);
    }

    @Test
    public void test290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test290");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        float float4 = funcionCuadratica3.getC();
        java.lang.String str5 = funcionCuadratica3.toString();
        java.lang.Object obj6 = funcionCuadratica3.raices();
        float float8 = funcionCuadratica3.eval(1.00230134E10f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+-1.0x" + "'", str5.equals("1.0x^2+-1.0x"));
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 1.0046079E20f + "'", float8 == 1.0046079E20f);
    }

    @Test
    public void test291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test291");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        boolean boolean24 = funcionCuadratica0.equals(funcionCuadratica15);
        float float25 = funcionCuadratica0.getC();
        java.lang.String str26 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + true + "'", boolean24 == true);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 0.0f + "'", float25 == 0.0f);
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
    }

    @Test
    public void test292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test292");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        funcionCuadratica14.setB((float) 2);
        java.lang.Object obj20 = funcionCuadratica14.raices();
        java.lang.Object obj21 = funcionCuadratica14.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertNotNull(obj20);
        org.junit.Assert.assertNotNull(obj21);
    }

    @Test
    public void test293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test293");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        float float12 = funcionCuadratica0.eval(1.99685125E12f);
        float float13 = funcionCuadratica0.determinante();
        float float14 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 3.987415E24f + "'", float12 == 3.987415E24f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 4.04009984E8f + "'", float13 == 4.04009984E8f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 0.0f + "'", float14 == 0.0f);
    }

    @Test
    public void test294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test294");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        funcionCuadratica0.setA((float) '4');
        funcionCuadratica0.setC((-40.0f));
        java.lang.Object obj9 = funcionCuadratica0.raices();
        float float11 = funcionCuadratica0.eval(3.987415E24f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + Float.POSITIVE_INFINITY + "'", float11 == Float.POSITIVE_INFINITY);
    }

    @Test
    public void test295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test295");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 10, 11.0f, 1.0f);
        float float5 = funcionCuadratica3.eval((-400.0f));
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 1595601.0f + "'", float5 == 1595601.0f);
    }

    @Test
    public void test296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test296");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        int int13 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str15 = funcionCuadratica14.toString();
        java.lang.Class<?> wildcardClass16 = funcionCuadratica14.getClass();
        java.lang.Class<?> wildcardClass17 = funcionCuadratica14.getClass();
        java.lang.Object obj18 = funcionCuadratica14.raices();
        int int19 = funcionCuadratica14.numRaices();
        boolean boolean20 = funcionCuadratica0.equals(funcionCuadratica14);
        int int21 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setA((float) 1);
        java.lang.Object obj24 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "1.0x^2" + "'", str15.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(obj18);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1 + "'", int19 == 1);
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertNotNull(obj24);
    }

    @Test
    public void test297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test297");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(Float.POSITIVE_INFINITY, 179700.0f, (float) (byte) 1);
        float float4 = funcionCuadratica3.getA();
        funcionCuadratica3.setA(9000.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + Float.POSITIVE_INFINITY + "'", float4 == Float.POSITIVE_INFINITY);
    }

    @Test
    public void test298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test298");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setA((float) 2);
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica(10000.0f, 10010.0f, 0.0f);
        float float12 = funcionCuadratica11.determinante();
        float float13 = funcionCuadratica11.getA();
        float float14 = funcionCuadratica11.getC();
        boolean boolean15 = funcionCuadratica3.equals(funcionCuadratica11);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 1.00200096E8f + "'", float12 == 1.00200096E8f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 10000.0f + "'", float13 == 10000.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 0.0f + "'", float14 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
    }

    @Test
    public void test299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test299");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica3.getClass();
        java.lang.String str5 = funcionCuadratica3.toString();
        int int6 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setB(2.13428676E17f);
        int int9 = funcionCuadratica3.numRaices();
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "10.0x^2+100.0x" + "'", str5.equals("10.0x^2+100.0x"));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6 == 2);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
    }

    @Test
    public void test300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test300");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.String str4 = funcionCuadratica0.toString();
        java.lang.Object obj5 = funcionCuadratica0.raices();
        funcionCuadratica0.setB(2480625.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "1.0x^2" + "'", str4.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(obj5);
    }

    @Test
    public void test301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test301");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        float float10 = funcionCuadratica0.eval(0.0f);
        java.lang.Class<?> wildcardClass11 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test302");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 1);
        funcionCuadratica0.setB(6.594416E25f);
        float float17 = funcionCuadratica0.eval((-44000.0f));
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + (-2.901543E30f) + "'", float17 == (-2.901543E30f));
    }

    @Test
    public void test303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test303");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-9.0f), 8.1090048E7f, (-40.0f));
    }

    @Test
    public void test304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test304");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        java.lang.String str17 = funcionCuadratica10.toString();
        funcionCuadratica10.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str21 = funcionCuadratica20.toString();
        funcionCuadratica20.setC((float) 10);
        boolean boolean24 = funcionCuadratica10.equals(funcionCuadratica20);
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica25.setB((float) 10);
        float float29 = funcionCuadratica25.eval((float) 10);
        float float31 = funcionCuadratica25.eval((-1.0f));
        java.lang.String str32 = funcionCuadratica25.toString();
        float float33 = funcionCuadratica25.getB();
        boolean boolean34 = funcionCuadratica10.equals(funcionCuadratica25);
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica10);
        float float36 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "1.0x^2+10.0x" + "'", str17.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2" + "'", str21.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 200.0f + "'", float29 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + (-9.0f) + "'", float31 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + true + "'", boolean34 == true);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + true + "'", boolean35 == true);
        org.junit.Assert.assertTrue("'" + float36 + "' != '" + 10.0f + "'", float36 == 10.0f);
    }

    @Test
    public void test305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test305");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setA((float) 2);
        java.lang.Object obj8 = funcionCuadratica3.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica12 = new byThey.practico5.FuncionCuadratica((-4.0f), 100110.0f, 8.1090048E7f);
        funcionCuadratica12.setC((-4.0f));
        boolean boolean15 = funcionCuadratica3.equals(funcionCuadratica12);
        org.junit.Assert.assertNotNull(obj8);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
    }

    @Test
    public void test306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test306");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        float float5 = funcionCuadratica0.getB();
        java.lang.Object obj6 = funcionCuadratica0.raices();
        funcionCuadratica0.setC((float) (short) -1);
        float float9 = funcionCuadratica0.getB();
        float float10 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
    }

    @Test
    public void test307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test307");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        int int9 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setA(811.0f);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
        org.junit.Assert.assertNotNull(obj12);
    }

    @Test
    public void test308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test308");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        float float10 = funcionCuadratica0.getC();
        float float11 = funcionCuadratica0.getB();
        float float12 = funcionCuadratica0.determinante();
        funcionCuadratica0.setA(1.05088369E16f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 10.0f + "'", float11 == 10.0f);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 100.0f + "'", float12 == 100.0f);
    }

    @Test
    public void test309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test309");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) -1, (float) 2, (float) 1);
        float float4 = funcionCuadratica3.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
    }

    @Test
    public void test310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test310");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        float float12 = funcionCuadratica0.eval(1.99685125E12f);
        float float13 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 3.987415E24f + "'", float12 == 3.987415E24f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 20100.0f + "'", float13 == 20100.0f);
    }

    @Test
    public void test311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test311");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass10 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) ' ');
        java.lang.Object obj13 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(obj13);
    }

    @Test
    public void test312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test312");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        float float5 = funcionCuadratica0.getB();
        java.lang.Object obj6 = funcionCuadratica0.raices();
        java.lang.String str7 = funcionCuadratica0.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica((float) (short) 100, (float) 0L, (float) (byte) 100);
        float float12 = funcionCuadratica11.getA();
        boolean boolean13 = funcionCuadratica0.equals(funcionCuadratica11);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2" + "'", str7.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 100.0f + "'", float12 == 100.0f);
        org.junit.Assert.assertTrue("'" + boolean13 + "' != '" + false + "'", boolean13 == false);
    }

    @Test
    public void test313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test313");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) '4');
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica11.setB((float) 10);
        float float15 = funcionCuadratica11.eval((float) 10);
        float float17 = funcionCuadratica11.eval((-1.0f));
        int int18 = funcionCuadratica11.numRaices();
        float float20 = funcionCuadratica11.eval((float) 10L);
        int int21 = funcionCuadratica11.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica25.setB((float) 1);
        boolean boolean28 = funcionCuadratica11.equals(funcionCuadratica25);
        java.lang.String str29 = funcionCuadratica11.toString();
        funcionCuadratica11.setC((-1.0f));
        boolean boolean32 = funcionCuadratica0.equals(funcionCuadratica11);
        float float33 = funcionCuadratica11.getB();
        funcionCuadratica11.setB(2.5716419E22f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 200.0f + "'", float15 == 200.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + (-9.0f) + "'", float17 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 200.0f + "'", float20 == 200.0f);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + str29 + "' != '" + "1.0x^2+10.0x" + "'", str29.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + boolean32 + "' != '" + false + "'", boolean32 == false);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
    }

    @Test
    public void test314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test314");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        java.lang.String str11 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.getB();
        float float13 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass14 = funcionCuadratica0.getClass();
        funcionCuadratica0.setA(121.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 0.0f + "'", float13 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test315");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getB();
        float float10 = funcionCuadratica3.getA();
        funcionCuadratica3.setB(20100.0f);
        java.lang.Object obj13 = funcionCuadratica3.raices();
        float float14 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 10.0f + "'", float10 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 10.0f + "'", float14 == 10.0f);
    }

    @Test
    public void test316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test316");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) (byte) 0);
        funcionCuadratica0.setC((float) 0L);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
    }

    @Test
    public void test317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test317");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 2, (-40.0f), (float) ' ');
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        java.lang.String str11 = funcionCuadratica4.toString();
        float float12 = funcionCuadratica4.getB();
        java.lang.Object obj13 = funcionCuadratica4.raices();
        boolean boolean14 = funcionCuadratica3.equals(funcionCuadratica4);
        float float15 = funcionCuadratica4.getC();
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 0.0f + "'", float15 == 0.0f);
    }

    @Test
    public void test318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test318");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
    }

    @Test
    public void test319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test319");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass10 = funcionCuadratica0.getClass();
        float float11 = funcionCuadratica0.getA();
        java.lang.String str12 = funcionCuadratica0.toString();
        float float14 = funcionCuadratica0.eval(104.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 1.0f + "'", float11 == 1.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2+10.0x" + "'", str12.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 11856.0f + "'", float14 == 11856.0f);
    }

    @Test
    public void test320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test320");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        float float10 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 20100.0f + "'", float10 == 20100.0f);
    }

    @Test
    public void test321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test321");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        java.lang.String str19 = funcionCuadratica5.toString();
        float float20 = funcionCuadratica5.getC();
        funcionCuadratica5.setC((float) 2);
        float float23 = funcionCuadratica5.getC();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "1.0x^2+10.0x" + "'", str19.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 0.0f + "'", float20 == 0.0f);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 2.0f + "'", float23 == 2.0f);
    }

    @Test
    public void test322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test322");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.getB();
        java.lang.Object obj21 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 10.0f + "'", float20 == 10.0f);
        org.junit.Assert.assertNotNull(obj21);
    }

    @Test
    public void test323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test323");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, 121.0f, 1.99685125E12f);
        float float5 = funcionCuadratica3.eval((float) '#');
        funcionCuadratica3.setA(1595601.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 1.99697393E12f + "'", float5 == 1.99697393E12f);
    }

    @Test
    public void test324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test324");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        byThey.practico5.FuncionCuadratica funcionCuadratica19 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica19.setB((float) 10);
        float float23 = funcionCuadratica19.eval((float) 10);
        float float25 = funcionCuadratica19.eval((-1.0f));
        int int26 = funcionCuadratica19.numRaices();
        float float28 = funcionCuadratica19.eval((float) 10L);
        funcionCuadratica19.setA((float) (byte) 100);
        funcionCuadratica19.setC((float) (byte) 1);
        funcionCuadratica19.setC((float) (short) 10);
        int int35 = funcionCuadratica19.numRaices();
        boolean boolean36 = funcionCuadratica5.equals(funcionCuadratica19);
        float float37 = funcionCuadratica5.determinante();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 200.0f + "'", float23 == 200.0f);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + (-9.0f) + "'", float25 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2 + "'", int26 == 2);
        org.junit.Assert.assertTrue("'" + float28 + "' != '" + 200.0f + "'", float28 == 200.0f);
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 0 + "'", int35 == 0);
        org.junit.Assert.assertTrue("'" + boolean36 + "' != '" + false + "'", boolean36 == false);
        org.junit.Assert.assertTrue("'" + float37 + "' != '" + 100.0f + "'", float37 == 100.0f);
    }

    @Test
    public void test325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test325");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-1.0f), (float) 100, (float) 2);
        float float4 = funcionCuadratica3.getB();
        java.lang.String str5 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 100.0f + "'", float4 == 100.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "-1.0x^2+100.0x+2.0" + "'", str5.equals("-1.0x^2+100.0x+2.0"));
    }

    @Test
    public void test326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test326");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        java.lang.String str10 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.eval(0.0f);
        java.lang.Class<?> wildcardClass13 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test327");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10010.0f, (float) (-1), (-300.0f));
        funcionCuadratica3.setC((float) ' ');
        float float6 = funcionCuadratica3.determinante();
        float float8 = funcionCuadratica3.eval(6.5684749E15f);
        funcionCuadratica3.setC((-1281279.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-1281279.0f) + "'", float6 == (-1281279.0f));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 4.318801E35f + "'", float8 == 4.318801E35f);
    }

    @Test
    public void test328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test328");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        float float4 = funcionCuadratica3.getC();
        java.lang.String str5 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica6 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica6.setB((float) 10);
        float float10 = funcionCuadratica6.eval((float) 10);
        int int11 = funcionCuadratica6.numRaices();
        boolean boolean12 = funcionCuadratica3.equals(funcionCuadratica6);
        int int13 = funcionCuadratica6.numRaices();
        float float14 = funcionCuadratica6.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+-1.0x" + "'", str5.equals("1.0x^2+-1.0x"));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 200.0f + "'", float10 == 200.0f);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 2 + "'", int11 == 2);
        org.junit.Assert.assertTrue("'" + boolean12 + "' != '" + false + "'", boolean12 == false);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 0.0f + "'", float14 == 0.0f);
    }

    @Test
    public void test329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test329");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float5 = funcionCuadratica0.eval((float) (byte) 100);
        java.lang.Object obj6 = funcionCuadratica0.raices();
        float float7 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11000.0f + "'", float5 == 11000.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 1.0f + "'", float7 == 1.0f);
    }

    @Test
    public void test330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test330");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        float float10 = funcionCuadratica0.getB();
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica11.setB((float) 10);
        float float15 = funcionCuadratica11.eval((float) 10);
        float float17 = funcionCuadratica11.eval((-1.0f));
        int int18 = funcionCuadratica11.numRaices();
        float float19 = funcionCuadratica11.getB();
        java.lang.Class<?> wildcardClass20 = funcionCuadratica11.getClass();
        float float21 = funcionCuadratica11.determinante();
        boolean boolean22 = funcionCuadratica0.equals(funcionCuadratica11);
        java.lang.Object obj23 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 10.0f + "'", float10 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 200.0f + "'", float15 == 200.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + (-9.0f) + "'", float17 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 10.0f + "'", float19 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 100.0f + "'", float21 == 100.0f);
        org.junit.Assert.assertTrue("'" + boolean22 + "' != '" + true + "'", boolean22 == true);
        org.junit.Assert.assertNotNull(obj23);
    }

    @Test
    public void test331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test331");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 'a', (float) 10, (float) (short) 10);
        float float4 = funcionCuadratica3.determinante();
        java.lang.Class<?> wildcardClass5 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + (-3780.0f) + "'", float4 == (-3780.0f));
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test332");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        int int9 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setB((-40.0f));
        float float12 = funcionCuadratica0.getC();
        float float13 = funcionCuadratica0.getA();
        funcionCuadratica0.setC(8.1046128E7f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 1.0f + "'", float13 == 1.0f);
    }

    @Test
    public void test333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test333");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(11000.0f, (float) '4', (float) 10L);
        float float4 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 11000.0f + "'", float4 == 11000.0f);
    }

    @Test
    public void test334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test334");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        float float2 = funcionCuadratica0.getB();
        float float4 = funcionCuadratica0.eval(1.60359435E12f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + float2 + "' != '" + 0.0f + "'", float2 == 0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 2.5715147E24f + "'", float4 == 2.5715147E24f);
    }

    @Test
    public void test335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test335");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        float float11 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 0.0f + "'", float11 == 0.0f);
    }

    @Test
    public void test336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test336");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        int int13 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str15 = funcionCuadratica14.toString();
        java.lang.Class<?> wildcardClass16 = funcionCuadratica14.getClass();
        java.lang.Class<?> wildcardClass17 = funcionCuadratica14.getClass();
        java.lang.Object obj18 = funcionCuadratica14.raices();
        int int19 = funcionCuadratica14.numRaices();
        boolean boolean20 = funcionCuadratica0.equals(funcionCuadratica14);
        int int21 = funcionCuadratica0.numRaices();
        float float22 = funcionCuadratica0.getC();
        int int23 = funcionCuadratica0.numRaices();
        float float24 = funcionCuadratica0.getC();
        float float25 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "1.0x^2" + "'", str15.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(obj18);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1 + "'", int19 == 1);
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + (-1.0f) + "'", float22 == (-1.0f));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2 + "'", int23 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + (-1.0f) + "'", float24 == (-1.0f));
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + (-1.0f) + "'", float25 == (-1.0f));
    }

    @Test
    public void test337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test337");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        float float6 = funcionCuadratica0.getC();
        float float8 = funcionCuadratica0.eval((-1.0f));
        byThey.practico5.FuncionCuadratica funcionCuadratica12 = new byThey.practico5.FuncionCuadratica((float) 10L, (float) 1, (float) 10L);
        float float13 = funcionCuadratica12.getB();
        funcionCuadratica12.setB((float) (short) -1);
        boolean boolean16 = funcionCuadratica0.equals(funcionCuadratica12);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + (-9.0f) + "'", float8 == (-9.0f));
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 1.0f + "'", float13 == 1.0f);
        org.junit.Assert.assertTrue("'" + boolean16 + "' != '" + false + "'", boolean16 == false);
    }

    @Test
    public void test338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test338");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 10, (float) (short) 0, (float) (byte) 10);
        float float4 = funcionCuadratica3.getB();
        float float5 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 10.0f + "'", float5 == 10.0f);
    }

    @Test
    public void test339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test339");
        try {
            byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(Float.NaN, (float) 0L, (float) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test340");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        float float20 = funcionCuadratica0.eval((-300.0f));
        int int21 = funcionCuadratica0.numRaices();
        float float22 = funcionCuadratica0.getC();
        funcionCuadratica0.setB(104.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 89999.0f + "'", float20 == 89999.0f);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + (-1.0f) + "'", float22 == (-1.0f));
    }

    @Test
    public void test341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test341");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica3.getClass();
        java.lang.Class<?> wildcardClass5 = funcionCuadratica3.getClass();
        funcionCuadratica3.setC(0.0f);
        int int8 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setA((float) (short) 10);
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
    }

    @Test
    public void test342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test342");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        int int15 = funcionCuadratica10.numRaices();
        java.lang.String str16 = funcionCuadratica10.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica17 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica17.setB((float) 10);
        float float20 = funcionCuadratica17.determinante();
        float float22 = funcionCuadratica17.eval((float) (byte) 100);
        java.lang.Object obj23 = funcionCuadratica17.raices();
        float float24 = funcionCuadratica17.determinante();
        float float25 = funcionCuadratica17.getA();
        float float26 = funcionCuadratica17.getC();
        float float27 = funcionCuadratica17.getA();
        boolean boolean28 = funcionCuadratica10.equals(funcionCuadratica17);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0" + "'", str16.equals("1.0x^2+10.0"));
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 100.0f + "'", float20 == 100.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 11000.0f + "'", float22 == 11000.0f);
        org.junit.Assert.assertNotNull(obj23);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 100.0f + "'", float24 == 100.0f);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 1.0f + "'", float25 == 1.0f);
        org.junit.Assert.assertTrue("'" + float26 + "' != '" + 0.0f + "'", float26 == 0.0f);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 1.0f + "'", float27 == 1.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
    }

    @Test
    public void test343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test343");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        int int3 = funcionCuadratica0.numRaices();
        float float4 = funcionCuadratica0.getB();
        float float5 = funcionCuadratica0.getC();
        float float6 = funcionCuadratica0.getC();
        float float7 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 2 + "'", int3 == 2);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 10.0f + "'", float4 == 10.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 0.0f + "'", float7 == 0.0f);
    }

    @Test
    public void test344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test344");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(20100.0f, (float) (short) -1, (float) 100);
        funcionCuadratica3.setB(10.0f);
    }

    @Test
    public void test345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test345");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica11.setB((float) 1);
        funcionCuadratica11.setB((float) (byte) 1);
        int int16 = funcionCuadratica11.numRaices();
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica11);
        java.lang.Class<?> wildcardClass18 = funcionCuadratica11.getClass();
        funcionCuadratica11.setB((float) (short) 0);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2 + "'", int16 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test346");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(11000.0f, (float) '4', (float) 10L);
        float float5 = funcionCuadratica3.eval((-4404839.0f));
        float float6 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 2.13428676E17f + "'", float5 == 2.13428676E17f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 52.0f + "'", float6 == 52.0f);
    }

    @Test
    public void test347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test347");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float5 = funcionCuadratica0.eval((float) (byte) 100);
        java.lang.Object obj6 = funcionCuadratica0.raices();
        float float7 = funcionCuadratica0.determinante();
        funcionCuadratica0.setC((float) (short) 100);
        float float11 = funcionCuadratica0.eval(3.22921071E11f);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11000.0f + "'", float5 == 11000.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 100.0f + "'", float7 == 100.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 1.0427802E23f + "'", float11 == 1.0427802E23f);
    }

    @Test
    public void test348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test348");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        funcionCuadratica0.setA((float) 10L);
        java.lang.String str22 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass23 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass24 = funcionCuadratica0.getClass();
        java.lang.String str25 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "10.0x^2+10.0x" + "'", str22.equals("10.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + str25 + "' != '" + "10.0x^2+10.0x" + "'", str25.equals("10.0x^2+10.0x"));
    }

    @Test
    public void test349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test349");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        funcionCuadratica8.setC((float) 10);
        funcionCuadratica8.setA((float) 1);
        byThey.practico5.FuncionCuadratica funcionCuadratica24 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str25 = funcionCuadratica24.toString();
        java.lang.Class<?> wildcardClass26 = funcionCuadratica24.getClass();
        java.lang.Class<?> wildcardClass27 = funcionCuadratica24.getClass();
        java.lang.Object obj28 = funcionCuadratica24.raices();
        int int29 = funcionCuadratica24.numRaices();
        float float30 = funcionCuadratica24.getC();
        java.lang.Class<?> wildcardClass31 = funcionCuadratica24.getClass();
        float float32 = funcionCuadratica24.determinante();
        boolean boolean33 = funcionCuadratica8.equals(funcionCuadratica24);
        java.lang.Object obj34 = funcionCuadratica8.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertTrue("'" + str25 + "' != '" + "1.0x^2" + "'", str25.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertNotNull(obj28);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1 + "'", int29 == 1);
        org.junit.Assert.assertTrue("'" + float30 + "' != '" + 0.0f + "'", float30 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass31);
        org.junit.Assert.assertTrue("'" + float32 + "' != '" + 0.0f + "'", float32 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean33 + "' != '" + false + "'", boolean33 == false);
        org.junit.Assert.assertNotNull(obj34);
    }

    @Test
    public void test350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test350");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        funcionCuadratica0.setA((float) 10L);
        java.lang.String str22 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass23 = funcionCuadratica0.getClass();
        funcionCuadratica0.setA(2.0f);
        float float27 = funcionCuadratica0.eval(100110.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "10.0x^2+10.0x" + "'", str22.equals("10.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 2.00450253E10f + "'", float27 == 2.00450253E10f);
    }

    @Test
    public void test351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test351");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-40.0f));
        java.lang.Class<?> wildcardClass23 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass24 = funcionCuadratica0.getClass();
        funcionCuadratica0.setA(11000.0f);
        java.lang.String str27 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 10.0f + "'", float20 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + str27 + "' != '" + "11000.0x^2+10.0x" + "'", str27.equals("11000.0x^2+10.0x"));
    }

    @Test
    public void test352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test352");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        int int15 = funcionCuadratica10.numRaices();
        funcionCuadratica10.setC(11000.0f);
        float float18 = funcionCuadratica10.determinante();
        java.lang.Class<?> wildcardClass19 = funcionCuadratica10.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + (-44000.0f) + "'", float18 == (-44000.0f));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test353");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) '#', (float) (short) 1, 0.0f);
        funcionCuadratica3.setA(9000.0f);
        float float6 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 9000.0f + "'", float6 == 9000.0f);
    }

    @Test
    public void test354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test354");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setC((float) 1L);
        float float6 = funcionCuadratica0.eval(1210010.0f);
        float float8 = funcionCuadratica0.eval((float) (byte) 0);
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 1.46412418E12f + "'", float6 == 1.46412418E12f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 1.0f + "'", float8 == 1.0f);
    }

    @Test
    public void test355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test355");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 100, 0.0f, (float) 10L);
        float float5 = funcionCuadratica3.eval(110.0f);
        funcionCuadratica3.setB((float) 1);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 1210010.0f + "'", float5 == 1210010.0f);
    }

    @Test
    public void test356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test356");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.38579984E8f, (-9.0f), 90.0f);
    }

    @Test
    public void test357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test357");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        java.lang.String str11 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.getC();
        funcionCuadratica0.setA((-4404839.0f));
        funcionCuadratica0.setC((float) '4');
        float float17 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 52.0f + "'", float17 == 52.0f);
    }

    @Test
    public void test358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test358");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getB();
        float float10 = funcionCuadratica3.getA();
        funcionCuadratica3.setB(20100.0f);
        java.lang.Object obj13 = funcionCuadratica3.raices();
        java.lang.Object obj14 = funcionCuadratica3.raices();
        java.lang.Class<?> wildcardClass15 = obj14.getClass();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 10.0f + "'", float10 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertNotNull(obj14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test359");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        float float20 = funcionCuadratica0.eval(2.5715264E30f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + Float.POSITIVE_INFINITY + "'", float20 == Float.POSITIVE_INFINITY);
    }

    @Test
    public void test360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test360");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.16756784E8f, 6.5684749E15f, 1.60359798E15f);
        java.lang.String str4 = funcionCuadratica3.toString();
        float float5 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "1.16756784E8x^2+6.5684749E15x+1.60359798E15" + "'", str4.equals("1.16756784E8x^2+6.5684749E15x+1.60359798E15"));
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 6.5684749E15f + "'", float5 == 6.5684749E15f);
    }

    @Test
    public void test361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test361");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        float float10 = funcionCuadratica0.eval((float) 1L);
        float float11 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 11.0f + "'", float10 == 11.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 0.0f + "'", float11 == 0.0f);
    }

    @Test
    public void test362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test362");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str8 = funcionCuadratica7.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica7.getClass();
        funcionCuadratica7.setC((float) (-1L));
        funcionCuadratica7.setB(11000.0f);
        boolean boolean14 = funcionCuadratica3.equals(funcionCuadratica7);
        float float15 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 100110.0f + "'", float15 == 100110.0f);
    }

    @Test
    public void test363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test363");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        funcionCuadratica0.setA((float) (short) 1);
        float float20 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 1.0f + "'", float20 == 1.0f);
    }

    @Test
    public void test364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test364");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        funcionCuadratica0.setB(11000.0f);
        float float7 = funcionCuadratica0.getA();
        java.lang.Class<?> wildcardClass8 = funcionCuadratica0.getClass();
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str10 = funcionCuadratica9.toString();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica9.getClass();
        boolean boolean12 = funcionCuadratica0.equals(funcionCuadratica9);
        java.lang.Object obj13 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 1.0f + "'", float7 == 1.0f);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2" + "'", str10.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + boolean12 + "' != '" + false + "'", boolean12 == false);
        org.junit.Assert.assertNotNull(obj13);
    }

    @Test
    public void test365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test365");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getA();
        float float5 = funcionCuadratica0.getC();
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica((-1.0f), (float) 100, (float) 2);
        boolean boolean10 = funcionCuadratica0.equals(funcionCuadratica9);
        java.lang.Object obj11 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean10 + "' != '" + false + "'", boolean10 == false);
        org.junit.Assert.assertNotNull(obj11);
    }

    @Test
    public void test366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test366");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(99921.0f, Float.NEGATIVE_INFINITY, 9000.0f);
        java.lang.String str4 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "99921.0x^2+-Infinityx+9000.0" + "'", str4.equals("99921.0x^2+-Infinityx+9000.0"));
    }

    @Test
    public void test367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test367");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(89999.0f, 1.38579984E8f, 2.13428676E17f);
    }

    @Test
    public void test368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test368");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getB();
        boolean boolean18 = funcionCuadratica0.equals(funcionCuadratica5);
        float float20 = funcionCuadratica0.eval((-300.0f));
        int int21 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setA(32.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 10.0f + "'", float17 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean18 + "' != '" + false + "'", boolean18 == false);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 89999.0f + "'", float20 == 89999.0f);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
    }

    @Test
    public void test369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test369");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) '4');
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica11.setB((float) 10);
        float float15 = funcionCuadratica11.eval((float) 10);
        float float17 = funcionCuadratica11.eval((-1.0f));
        int int18 = funcionCuadratica11.numRaices();
        float float20 = funcionCuadratica11.eval((float) 10L);
        int int21 = funcionCuadratica11.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica25.setB((float) 1);
        boolean boolean28 = funcionCuadratica11.equals(funcionCuadratica25);
        java.lang.String str29 = funcionCuadratica11.toString();
        funcionCuadratica11.setC((-1.0f));
        boolean boolean32 = funcionCuadratica0.equals(funcionCuadratica11);
        float float34 = funcionCuadratica0.eval(1.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 200.0f + "'", float15 == 200.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + (-9.0f) + "'", float17 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 200.0f + "'", float20 == 200.0f);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + str29 + "' != '" + "1.0x^2+10.0x" + "'", str29.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + boolean32 + "' != '" + false + "'", boolean32 == false);
        org.junit.Assert.assertTrue("'" + float34 + "' != '" + 63.0f + "'", float34 == 63.0f);
    }

    @Test
    public void test370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test370");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.String str9 = funcionCuadratica0.toString();
        float float11 = funcionCuadratica0.eval(8.1206009E12f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "1.0x^2+10.0x" + "'", str9.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 6.594416E25f + "'", float11 == 6.594416E25f);
    }

    @Test
    public void test371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test371");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        int int3 = funcionCuadratica0.numRaices();
        float float4 = funcionCuadratica0.getB();
        funcionCuadratica0.setB(2.5124822E21f);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 2 + "'", int3 == 2);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 10.0f + "'", float4 == 10.0f);
    }

    @Test
    public void test372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test372");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.16756784E8f, 1.0f, (float) (byte) 10);
    }

    @Test
    public void test373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test373");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica3.getClass();
        funcionCuadratica3.setB(121.0f);
        float float7 = funcionCuadratica3.getA();
        java.lang.Object obj8 = funcionCuadratica3.raices();
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 10.0f + "'", float7 == 10.0f);
        org.junit.Assert.assertNotNull(obj8);
    }

    @Test
    public void test374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test374");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        funcionCuadratica3.setB((float) ' ');
        java.lang.String str6 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "100110.0x^2+32.0x+11.0" + "'", str6.equals("100110.0x^2+32.0x+11.0"));
    }

    @Test
    public void test375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test375");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-44000.0f), 200.0f, (float) (byte) 1);
    }

    @Test
    public void test376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test376");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) (byte) 100);
        int int10 = funcionCuadratica0.numRaices();
        int int11 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
    }

    @Test
    public void test377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test377");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        float float5 = funcionCuadratica0.getB();
        java.lang.Object obj6 = funcionCuadratica0.raices();
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((-300.0f));
        funcionCuadratica0.setA(3.22921071E11f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2" + "'", str7.equals("1.0x^2"));
    }

    @Test
    public void test378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test378");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        boolean boolean24 = funcionCuadratica0.equals(funcionCuadratica15);
        float float25 = funcionCuadratica15.getA();
        float float26 = funcionCuadratica15.getB();
        int int27 = funcionCuadratica15.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + true + "'", boolean24 == true);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 1.0f + "'", float25 == 1.0f);
        org.junit.Assert.assertTrue("'" + float26 + "' != '" + 10.0f + "'", float26 == 10.0f);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 2 + "'", int27 == 2);
    }

    @Test
    public void test379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test379");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (byte) 0, 0.0f);
        funcionCuadratica3.setC((float) (byte) 100);
        int int6 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setB(402000.0f);
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
    }

    @Test
    public void test380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test380");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval((float) '4');
        java.lang.String str6 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 2.70697376E8f + "'", float5 == 2.70697376E8f);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "100110.0x^2+-1.0x+11.0" + "'", str6.equals("100110.0x^2+-1.0x+11.0"));
    }

    @Test
    public void test381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test381");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.0f, (float) (short) 1, (-9.0f));
        int int4 = funcionCuadratica3.numRaices();
        float float5 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2 + "'", int4 == 2);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 1.0f + "'", float5 == 1.0f);
    }

    @Test
    public void test382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test382");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        funcionCuadratica0.setA((float) '4');
        int int7 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
    }

    @Test
    public void test383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test383");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) (byte) 100);
        funcionCuadratica0.setB(0.0f);
        java.lang.Class<?> wildcardClass12 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass13 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test384");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        float float3 = funcionCuadratica0.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        java.lang.String str11 = funcionCuadratica4.toString();
        float float12 = funcionCuadratica4.getB();
        java.lang.Object obj13 = funcionCuadratica4.raices();
        int int14 = funcionCuadratica4.numRaices();
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica4);
        int int16 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setA((-1281279.0f));
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 1.0f + "'", float3 == 1.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14 == 2);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1 + "'", int16 == 1);
    }

    @Test
    public void test385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test385");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        funcionCuadratica0.setA((float) '4');
        funcionCuadratica0.setC((-40.0f));
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        int int17 = funcionCuadratica10.numRaices();
        float float19 = funcionCuadratica10.eval((float) 10L);
        int int20 = funcionCuadratica10.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica24 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica24.setB((float) 1);
        boolean boolean27 = funcionCuadratica10.equals(funcionCuadratica24);
        java.lang.String str28 = funcionCuadratica24.toString();
        boolean boolean29 = funcionCuadratica0.equals(funcionCuadratica24);
        float float30 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 2 + "'", int17 == 2);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2 + "'", int20 == 2);
        org.junit.Assert.assertTrue("'" + boolean27 + "' != '" + false + "'", boolean27 == false);
        org.junit.Assert.assertTrue("'" + str28 + "' != '" + "10.0x^2+1.0x" + "'", str28.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + boolean29 + "' != '" + false + "'", boolean29 == false);
        org.junit.Assert.assertTrue("'" + float30 + "' != '" + 52.0f + "'", float30 == 52.0f);
    }

    @Test
    public void test386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test386");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        float float9 = funcionCuadratica0.determinante();
        int int10 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 4.04009984E8f + "'", float9 == 4.04009984E8f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
    }

    @Test
    public void test387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test387");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica11.setB((float) 1);
        funcionCuadratica11.setB((float) (byte) 1);
        int int16 = funcionCuadratica11.numRaices();
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica11);
        funcionCuadratica11.setA(1.60359435E12f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        float float21 = funcionCuadratica20.determinante();
        java.lang.String str22 = funcionCuadratica20.toString();
        int int23 = funcionCuadratica20.numRaices();
        boolean boolean24 = funcionCuadratica11.equals(funcionCuadratica20);
        java.lang.String str25 = funcionCuadratica20.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2 + "'", int16 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 0.0f + "'", float21 == 0.0f);
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2" + "'", str22.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23 == 1);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + str25 + "' != '" + "1.0x^2" + "'", str25.equals("1.0x^2"));
    }

    @Test
    public void test388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test388");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        funcionCuadratica0.setB(11.0f);
        float float7 = funcionCuadratica0.getC();
        int int8 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 0.0f + "'", float7 == 0.0f);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
    }

    @Test
    public void test389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test389");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str8 = funcionCuadratica7.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica7.getClass();
        funcionCuadratica7.setC((float) (-1L));
        funcionCuadratica7.setB(11000.0f);
        boolean boolean14 = funcionCuadratica3.equals(funcionCuadratica7);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        java.lang.String str20 = funcionCuadratica15.toString();
        funcionCuadratica15.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica23 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica23.setB((float) 10);
        float float27 = funcionCuadratica23.eval((float) 10);
        float float29 = funcionCuadratica23.eval((-1.0f));
        int int30 = funcionCuadratica23.numRaices();
        float float32 = funcionCuadratica23.eval((float) 10L);
        float float33 = funcionCuadratica23.getB();
        boolean boolean34 = funcionCuadratica15.equals(funcionCuadratica23);
        funcionCuadratica23.setC((float) 10);
        funcionCuadratica23.setA((float) 1);
        java.lang.Class<?> wildcardClass39 = funcionCuadratica23.getClass();
        boolean boolean40 = funcionCuadratica3.equals(funcionCuadratica23);
        float float42 = funcionCuadratica3.eval((float) ' ');
        java.lang.String str43 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "1.0x^2+10.0x" + "'", str20.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 200.0f + "'", float27 == 200.0f);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + (-9.0f) + "'", float29 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 2 + "'", int30 == 2);
        org.junit.Assert.assertTrue("'" + float32 + "' != '" + 200.0f + "'", float32 == 200.0f);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + false + "'", boolean34 == false);
        org.junit.Assert.assertNotNull(wildcardClass39);
        org.junit.Assert.assertTrue("'" + boolean40 + "' != '" + false + "'", boolean40 == false);
        org.junit.Assert.assertTrue("'" + float42 + "' != '" + 1.02512616E8f + "'", float42 == 1.02512616E8f);
        org.junit.Assert.assertTrue("'" + str43 + "' != '" + "100110.0x^2+-1.0x+11.0" + "'", str43.equals("100110.0x^2+-1.0x+11.0"));
    }

    @Test
    public void test390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test390");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-4.0f), 100110.0f, 8.1090048E7f);
        funcionCuadratica3.setB(121.0f);
        float float7 = funcionCuadratica3.eval(121.0f);
        funcionCuadratica3.setB((float) (byte) 0);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 8.1046128E7f + "'", float7 == 8.1046128E7f);
    }

    @Test
    public void test391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test391");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        funcionCuadratica0.setB(11000.0f);
        float float7 = funcionCuadratica0.getA();
        java.lang.Class<?> wildcardClass8 = funcionCuadratica0.getClass();
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str10 = funcionCuadratica9.toString();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica9.getClass();
        boolean boolean12 = funcionCuadratica0.equals(funcionCuadratica9);
        funcionCuadratica0.setA(402000.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 1.0f + "'", float7 == 1.0f);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2" + "'", str10.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + boolean12 + "' != '" + false + "'", boolean12 == false);
    }

    @Test
    public void test392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test392");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-40.0f));
        java.lang.Class<?> wildcardClass23 = funcionCuadratica0.getClass();
        float float24 = funcionCuadratica0.getA();
        float float26 = funcionCuadratica0.eval((-3780.0f));
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 10.0f + "'", float20 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + (-40.0f) + "'", float24 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float26 + "' != '" + (-5.7157382E8f) + "'", float26 == (-5.7157382E8f));
    }

    @Test
    public void test393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test393");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.0046079E20f, 0.0f, 100.0f);
    }

    @Test
    public void test394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test394");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(11856.0f, (float) (byte) 10, (float) 1);
    }

    @Test
    public void test395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test395");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 10, (float) (short) 0, (float) (byte) 10);
        float float4 = funcionCuadratica3.getB();
        float float6 = funcionCuadratica3.eval(1.00400594E16f);
        java.lang.String str7 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 1.00802796E33f + "'", float6 == 1.00802796E33f);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "10.0x^2+10.0" + "'", str7.equals("10.0x^2+10.0"));
    }

    @Test
    public void test396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test396");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getB();
        funcionCuadratica3.setA(1210010.0f);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
    }

    @Test
    public void test397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test397");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.String str4 = funcionCuadratica0.toString();
        java.lang.Object obj5 = funcionCuadratica0.raices();
        float float6 = funcionCuadratica0.determinante();
        float float7 = funcionCuadratica0.getC();
        java.lang.String str8 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "1.0x^2" + "'", str4.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(obj5);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 0.0f + "'", float7 == 0.0f);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
    }

    @Test
    public void test398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test398");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(52.0f, 10272.0f, 2.5716419E22f);
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica(20100.0f, (float) (short) -1, (float) 100);
        float float8 = funcionCuadratica7.getB();
        boolean boolean9 = funcionCuadratica3.equals(funcionCuadratica7);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        int int17 = funcionCuadratica10.numRaices();
        float float19 = funcionCuadratica10.eval((float) 10L);
        int int20 = funcionCuadratica10.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica24 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica24.setB((float) 1);
        boolean boolean27 = funcionCuadratica10.equals(funcionCuadratica24);
        funcionCuadratica10.setB(32.0f);
        float float30 = funcionCuadratica10.getB();
        funcionCuadratica10.setA(11.0f);
        java.lang.String str33 = funcionCuadratica10.toString();
        boolean boolean34 = funcionCuadratica7.equals(funcionCuadratica10);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + (-1.0f) + "'", float8 == (-1.0f));
        org.junit.Assert.assertTrue("'" + boolean9 + "' != '" + false + "'", boolean9 == false);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 2 + "'", int17 == 2);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2 + "'", int20 == 2);
        org.junit.Assert.assertTrue("'" + boolean27 + "' != '" + false + "'", boolean27 == false);
        org.junit.Assert.assertTrue("'" + float30 + "' != '" + 32.0f + "'", float30 == 32.0f);
        org.junit.Assert.assertTrue("'" + str33 + "' != '" + "11.0x^2+32.0x" + "'", str33.equals("11.0x^2+32.0x"));
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + false + "'", boolean34 == false);
    }

    @Test
    public void test399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test399");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        java.lang.Object obj20 = funcionCuadratica8.raices();
        float float21 = funcionCuadratica8.getB();
        float float22 = funcionCuadratica8.determinante();
        float float23 = funcionCuadratica8.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertNotNull(obj20);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 10.0f + "'", float21 == 10.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 100.0f + "'", float22 == 100.0f);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 0.0f + "'", float23 == 0.0f);
    }

    @Test
    public void test400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test400");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 1, Float.POSITIVE_INFINITY, 1.60363397E11f);
        float float4 = funcionCuadratica3.getC();
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        float float11 = funcionCuadratica5.eval((-1.0f));
        int int12 = funcionCuadratica5.numRaices();
        float float14 = funcionCuadratica5.eval((float) 10L);
        int int15 = funcionCuadratica5.numRaices();
        java.lang.String str16 = funcionCuadratica5.toString();
        float float17 = funcionCuadratica5.getC();
        funcionCuadratica5.setA((-4404839.0f));
        float float20 = funcionCuadratica5.determinante();
        boolean boolean21 = funcionCuadratica3.equals(funcionCuadratica5);
        int int22 = funcionCuadratica5.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.60363397E11f + "'", float4 == 1.60363397E11f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + (-9.0f) + "'", float11 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x" + "'", str16.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 0.0f + "'", float17 == 0.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 100.0f + "'", float20 == 100.0f);
        org.junit.Assert.assertTrue("'" + boolean21 + "' != '" + false + "'", boolean21 == false);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
    }

    @Test
    public void test401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test401");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setA((float) 2);
        int int8 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setA(2480625.0f);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
    }

    @Test
    public void test402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test402");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) '4');
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica11.setB((float) 10);
        float float15 = funcionCuadratica11.eval((float) 10);
        float float17 = funcionCuadratica11.eval((-1.0f));
        int int18 = funcionCuadratica11.numRaices();
        float float20 = funcionCuadratica11.eval((float) 10L);
        int int21 = funcionCuadratica11.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica25.setB((float) 1);
        boolean boolean28 = funcionCuadratica11.equals(funcionCuadratica25);
        java.lang.String str29 = funcionCuadratica11.toString();
        funcionCuadratica11.setC((-1.0f));
        boolean boolean32 = funcionCuadratica0.equals(funcionCuadratica11);
        int int33 = funcionCuadratica11.numRaices();
        int int34 = funcionCuadratica11.numRaices();
        float float36 = funcionCuadratica11.eval(121.0f);
        funcionCuadratica11.setB(10272.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 200.0f + "'", float15 == 200.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + (-9.0f) + "'", float17 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 200.0f + "'", float20 == 200.0f);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + str29 + "' != '" + "1.0x^2+10.0x" + "'", str29.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + boolean32 + "' != '" + false + "'", boolean32 == false);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 2 + "'", int33 == 2);
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 2 + "'", int34 == 2);
        org.junit.Assert.assertTrue("'" + float36 + "' != '" + 15850.0f + "'", float36 == 15850.0f);
    }

    @Test
    public void test403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test403");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        funcionCuadratica0.setB(11.0f);
        int int7 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setC(110.0f);
        java.lang.String str10 = funcionCuadratica0.toString();
        funcionCuadratica0.setB((float) (-1));
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+11.0x+110.0" + "'", str10.equals("1.0x^2+11.0x+110.0"));
    }

    @Test
    public void test404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test404");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setA(Float.POSITIVE_INFINITY);
        java.lang.Object obj14 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj14);
    }

    @Test
    public void test405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test405");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-4.628973E22f), 10.0f, (float) 1);
    }

    @Test
    public void test406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test406");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getA();
        float float5 = funcionCuadratica0.getC();
        float float7 = funcionCuadratica0.eval((float) (byte) 0);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str9 = funcionCuadratica8.toString();
        funcionCuadratica8.setC((float) 10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica15.setB((float) 1);
        funcionCuadratica15.setB((float) (byte) 1);
        int int20 = funcionCuadratica15.numRaices();
        float float21 = funcionCuadratica15.getB();
        float float22 = funcionCuadratica15.getA();
        float float24 = funcionCuadratica15.eval((float) 100L);
        boolean boolean25 = funcionCuadratica8.equals(funcionCuadratica15);
        boolean boolean26 = funcionCuadratica0.equals(funcionCuadratica15);
        float float27 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 0.0f + "'", float7 == 0.0f);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "1.0x^2" + "'", str9.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2 + "'", int20 == 2);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 1.0f + "'", float21 == 1.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 10.0f + "'", float22 == 10.0f);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 100100.0f + "'", float24 == 100100.0f);
        org.junit.Assert.assertTrue("'" + boolean25 + "' != '" + false + "'", boolean25 == false);
        org.junit.Assert.assertTrue("'" + boolean26 + "' != '" + false + "'", boolean26 == false);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 10.0f + "'", float27 == 10.0f);
    }

    @Test
    public void test407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test407");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(2480625.0f, 1.16756784E8f, 8.1090048E7f);
        funcionCuadratica3.setA(1.0046079E20f);
    }

    @Test
    public void test408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test408");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 10, (float) (short) 0, (float) (byte) 10);
        float float4 = funcionCuadratica3.determinante();
        float float6 = funcionCuadratica3.eval((float) (short) 0);
        java.lang.Class<?> wildcardClass7 = funcionCuadratica3.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + (-400.0f) + "'", float4 == (-400.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 10.0f + "'", float6 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test409");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        java.lang.String str17 = funcionCuadratica10.toString();
        funcionCuadratica10.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str21 = funcionCuadratica20.toString();
        funcionCuadratica20.setC((float) 10);
        boolean boolean24 = funcionCuadratica10.equals(funcionCuadratica20);
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica25.setB((float) 10);
        float float29 = funcionCuadratica25.eval((float) 10);
        float float31 = funcionCuadratica25.eval((-1.0f));
        java.lang.String str32 = funcionCuadratica25.toString();
        float float33 = funcionCuadratica25.getB();
        boolean boolean34 = funcionCuadratica10.equals(funcionCuadratica25);
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica10);
        int int36 = funcionCuadratica10.numRaices();
        java.lang.Class<?> wildcardClass37 = funcionCuadratica10.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "1.0x^2+10.0x" + "'", str17.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2" + "'", str21.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 200.0f + "'", float29 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + (-9.0f) + "'", float31 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + true + "'", boolean34 == true);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + true + "'", boolean35 == true);
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 2 + "'", int36 == 2);
        org.junit.Assert.assertNotNull(wildcardClass37);
    }

    @Test
    public void test410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test410");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        float float4 = funcionCuadratica0.eval((-4000100.0f));
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.60008E13f + "'", float4 == 1.60008E13f);
    }

    @Test
    public void test411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test411");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        boolean boolean24 = funcionCuadratica0.equals(funcionCuadratica15);
        java.lang.String str25 = funcionCuadratica15.toString();
        int int26 = funcionCuadratica15.numRaices();
        funcionCuadratica15.setC(1.00200096E8f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + true + "'", boolean24 == true);
        org.junit.Assert.assertTrue("'" + str25 + "' != '" + "1.0x^2+10.0x" + "'", str25.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2 + "'", int26 == 2);
    }

    @Test
    public void test412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test412");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float5 = funcionCuadratica0.eval((float) (byte) 100);
        java.lang.String str6 = funcionCuadratica0.toString();
        float float7 = funcionCuadratica0.determinante();
        float float8 = funcionCuadratica0.getB();
        int int9 = funcionCuadratica0.numRaices();
        float float10 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11000.0f + "'", float5 == 11000.0f);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "1.0x^2+10.0x" + "'", str6.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 100.0f + "'", float7 == 100.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 1.0f + "'", float10 == 1.0f);
    }

    @Test
    public void test413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test413");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float4 = funcionCuadratica3.getC();
        funcionCuadratica3.setA(32.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 11.0f + "'", float4 == 11.0f);
    }

    @Test
    public void test414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test414");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float5 = funcionCuadratica0.eval((float) (byte) 100);
        java.lang.Object obj6 = funcionCuadratica0.raices();
        float float7 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11000.0f + "'", float5 == 11000.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 0.0f + "'", float7 == 0.0f);
    }

    @Test
    public void test415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test415");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float6 = funcionCuadratica0.eval(100.0f);
        funcionCuadratica0.setC((float) (short) 1);
        float float9 = funcionCuadratica0.determinante();
        float float10 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 10010.0f + "'", float6 == 10010.0f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + (-4.0f) + "'", float9 == (-4.0f));
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
    }

    @Test
    public void test416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test416");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.eval(8.1206009E12f);
        funcionCuadratica0.setA(2.5715147E24f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 6.594416E25f + "'", float5 == 6.594416E25f);
    }

    @Test
    public void test417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test417");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        float float3 = funcionCuadratica0.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        java.lang.String str11 = funcionCuadratica4.toString();
        float float12 = funcionCuadratica4.getB();
        java.lang.Object obj13 = funcionCuadratica4.raices();
        int int14 = funcionCuadratica4.numRaices();
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica4);
        byThey.practico5.FuncionCuadratica funcionCuadratica16 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica16.setB((float) 10);
        float float20 = funcionCuadratica16.eval((float) 10);
        float float22 = funcionCuadratica16.eval((-1.0f));
        int int23 = funcionCuadratica16.numRaices();
        float float25 = funcionCuadratica16.eval((float) 10L);
        int int26 = funcionCuadratica16.numRaices();
        boolean boolean27 = funcionCuadratica4.equals(funcionCuadratica16);
        java.lang.Object obj28 = funcionCuadratica4.raices();
        float float29 = funcionCuadratica4.getA();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 1.0f + "'", float3 == 1.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14 == 2);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 200.0f + "'", float20 == 200.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + (-9.0f) + "'", float22 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2 + "'", int23 == 2);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 200.0f + "'", float25 == 200.0f);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2 + "'", int26 == 2);
        org.junit.Assert.assertTrue("'" + boolean27 + "' != '" + true + "'", boolean27 == true);
        org.junit.Assert.assertNotNull(obj28);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 1.0f + "'", float29 == 1.0f);
    }

    @Test
    public void test418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test418");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(121.0f, (float) (short) 1, (float) (short) 1);
        java.lang.String str4 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "121.0x^2+1.0x+1.0" + "'", str4.equals("121.0x^2+1.0x+1.0"));
    }

    @Test
    public void test419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test419");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getA();
        java.lang.Object obj5 = funcionCuadratica0.raices();
        float float6 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
        org.junit.Assert.assertNotNull(obj5);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 10.0f + "'", float6 == 10.0f);
    }

    @Test
    public void test420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test420");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float5 = funcionCuadratica0.eval((float) (byte) 100);
        java.lang.Object obj6 = funcionCuadratica0.raices();
        float float7 = funcionCuadratica0.determinante();
        int int8 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11000.0f + "'", float5 == 11000.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 100.0f + "'", float7 == 100.0f);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
    }

    @Test
    public void test421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test421");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        funcionCuadratica3.setA(2.70697376E8f);
    }

    @Test
    public void test422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test422");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        float float8 = funcionCuadratica0.determinante();
        funcionCuadratica0.setA((float) (short) 1);
        float float11 = funcionCuadratica0.getA();
        java.lang.String str12 = funcionCuadratica0.toString();
        float float13 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 0.0f + "'", float8 == 0.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 1.0f + "'", float11 == 1.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2" + "'", str12.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 1.0f + "'", float13 == 1.0f);
    }

    @Test
    public void test423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test423");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-4404839.0f), (float) 0, (float) 1);
        funcionCuadratica3.setB((float) (short) 100);
        float float7 = funcionCuadratica3.eval(1.02512616E8f);
        java.lang.Object obj8 = funcionCuadratica3.raices();
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + (-4.628973E22f) + "'", float7 == (-4.628973E22f));
        org.junit.Assert.assertNotNull(obj8);
    }

    @Test
    public void test424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test424");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        float float20 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-40.0f));
        java.lang.Class<?> wildcardClass23 = funcionCuadratica0.getClass();
        float float24 = funcionCuadratica0.getA();
        float float26 = funcionCuadratica0.eval((float) (byte) 10);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 10.0f + "'", float20 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + (-40.0f) + "'", float24 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float26 + "' != '" + (-3900.0f) + "'", float26 == (-3900.0f));
    }

    @Test
    public void test425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test425");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.determinante();
        funcionCuadratica0.setA((float) 10L);
        java.lang.Object obj22 = funcionCuadratica0.raices();
        float float24 = funcionCuadratica0.eval(200.0f);
        float float25 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 100.0f + "'", float19 == 100.0f);
        org.junit.Assert.assertNotNull(obj22);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 402000.0f + "'", float24 == 402000.0f);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 10.0f + "'", float25 == 10.0f);
    }

    @Test
    public void test426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test426");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        java.lang.String str8 = funcionCuadratica0.toString();
        int int9 = funcionCuadratica0.numRaices();
        int int10 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setB((float) (short) 10);
        float float13 = funcionCuadratica0.getB();
        float float14 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1 + "'", int10 == 1);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 10.0f + "'", float13 == 10.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 100.0f + "'", float14 == 100.0f);
    }

    @Test
    public void test427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test427");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        java.lang.String str17 = funcionCuadratica10.toString();
        funcionCuadratica10.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str21 = funcionCuadratica20.toString();
        funcionCuadratica20.setC((float) 10);
        boolean boolean24 = funcionCuadratica10.equals(funcionCuadratica20);
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica25.setB((float) 10);
        float float29 = funcionCuadratica25.eval((float) 10);
        float float31 = funcionCuadratica25.eval((-1.0f));
        java.lang.String str32 = funcionCuadratica25.toString();
        float float33 = funcionCuadratica25.getB();
        boolean boolean34 = funcionCuadratica10.equals(funcionCuadratica25);
        boolean boolean35 = funcionCuadratica0.equals(funcionCuadratica10);
        funcionCuadratica10.setC(99921.0f);
        float float38 = funcionCuadratica10.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "1.0x^2+10.0x" + "'", str17.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "1.0x^2" + "'", str21.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 200.0f + "'", float29 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + (-9.0f) + "'", float31 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str32 + "' != '" + "1.0x^2+10.0x" + "'", str32.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + true + "'", boolean34 == true);
        org.junit.Assert.assertTrue("'" + boolean35 + "' != '" + true + "'", boolean35 == true);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 10.0f + "'", float38 == 10.0f);
    }

    @Test
    public void test428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test428");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(20100.0f, 11000.0f, 500.0f);
        funcionCuadratica3.setC(1.99697393E12f);
        float float7 = funcionCuadratica3.eval(1.38109104E8f);
        float float8 = funcionCuadratica3.determinante();
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 3.8338992E20f + "'", float7 == 3.8338992E20f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + (-1.60556701E17f) + "'", float8 == (-1.60556701E17f));
    }

    @Test
    public void test429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test429");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        funcionCuadratica0.setB(32.0f);
        float float20 = funcionCuadratica0.getB();
        byThey.practico5.FuncionCuadratica funcionCuadratica21 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica21.setB((float) 10);
        float float25 = funcionCuadratica21.eval((float) 10);
        float float27 = funcionCuadratica21.eval((-1.0f));
        int int28 = funcionCuadratica21.numRaices();
        float float30 = funcionCuadratica21.eval((float) 10L);
        float float31 = funcionCuadratica21.getB();
        funcionCuadratica21.setC((-40.0f));
        funcionCuadratica21.setC((-1.0f));
        boolean boolean36 = funcionCuadratica0.equals(funcionCuadratica21);
        funcionCuadratica21.setB(104.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 32.0f + "'", float20 == 32.0f);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 200.0f + "'", float25 == 200.0f);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + (-9.0f) + "'", float27 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 2 + "'", int28 == 2);
        org.junit.Assert.assertTrue("'" + float30 + "' != '" + 200.0f + "'", float30 == 200.0f);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + 10.0f + "'", float31 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean36 + "' != '" + false + "'", boolean36 == false);
    }

    @Test
    public void test430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test430");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float23 = funcionCuadratica15.getB();
        int int24 = funcionCuadratica15.numRaices();
        funcionCuadratica15.setB((-40.0f));
        float float27 = funcionCuadratica15.getC();
        boolean boolean28 = funcionCuadratica0.equals(funcionCuadratica15);
        float float29 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2 + "'", int24 == 2);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 0.0f + "'", float27 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + 100.0f + "'", float29 == 100.0f);
    }

    @Test
    public void test431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test431");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setA((float) (byte) 100);
        funcionCuadratica0.setC((float) (byte) 1);
        float float14 = funcionCuadratica0.determinante();
        java.lang.String str15 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-300.0f) + "'", float14 == (-300.0f));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "100.0x^2+10.0x+1.0" + "'", str15.equals("100.0x^2+10.0x+1.0"));
    }

    @Test
    public void test432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test432");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 100, (float) 0L, (float) (byte) 100);
        float float4 = funcionCuadratica3.getA();
        funcionCuadratica3.setA((float) (-1));
        java.lang.Object obj7 = funcionCuadratica3.raices();
        float float9 = funcionCuadratica3.eval((float) 10L);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 100.0f + "'", float4 == 100.0f);
        org.junit.Assert.assertNotNull(obj7);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
    }

    @Test
    public void test433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test433");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str8 = funcionCuadratica7.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica7.getClass();
        funcionCuadratica7.setC((float) (-1L));
        funcionCuadratica7.setB(11000.0f);
        boolean boolean14 = funcionCuadratica3.equals(funcionCuadratica7);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        java.lang.String str20 = funcionCuadratica15.toString();
        funcionCuadratica15.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica23 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica23.setB((float) 10);
        float float27 = funcionCuadratica23.eval((float) 10);
        float float29 = funcionCuadratica23.eval((-1.0f));
        int int30 = funcionCuadratica23.numRaices();
        float float32 = funcionCuadratica23.eval((float) 10L);
        float float33 = funcionCuadratica23.getB();
        boolean boolean34 = funcionCuadratica15.equals(funcionCuadratica23);
        funcionCuadratica23.setC((float) 10);
        funcionCuadratica23.setA((float) 1);
        java.lang.Class<?> wildcardClass39 = funcionCuadratica23.getClass();
        boolean boolean40 = funcionCuadratica3.equals(funcionCuadratica23);
        byThey.practico5.FuncionCuadratica funcionCuadratica41 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica41.setB((float) 10);
        float float45 = funcionCuadratica41.eval((float) 10);
        float float47 = funcionCuadratica41.eval((-1.0f));
        int int48 = funcionCuadratica41.numRaices();
        float float50 = funcionCuadratica41.eval((float) 10L);
        int int51 = funcionCuadratica41.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica55 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica55.setB((float) 1);
        boolean boolean58 = funcionCuadratica41.equals(funcionCuadratica55);
        funcionCuadratica55.setB((float) 2);
        boolean boolean61 = funcionCuadratica23.equals(funcionCuadratica55);
        funcionCuadratica55.setB(11000.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "1.0x^2+10.0x" + "'", str20.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 200.0f + "'", float27 == 200.0f);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + (-9.0f) + "'", float29 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 2 + "'", int30 == 2);
        org.junit.Assert.assertTrue("'" + float32 + "' != '" + 200.0f + "'", float32 == 200.0f);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + false + "'", boolean34 == false);
        org.junit.Assert.assertNotNull(wildcardClass39);
        org.junit.Assert.assertTrue("'" + boolean40 + "' != '" + false + "'", boolean40 == false);
        org.junit.Assert.assertTrue("'" + float45 + "' != '" + 200.0f + "'", float45 == 200.0f);
        org.junit.Assert.assertTrue("'" + float47 + "' != '" + (-9.0f) + "'", float47 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int48 + "' != '" + 2 + "'", int48 == 2);
        org.junit.Assert.assertTrue("'" + float50 + "' != '" + 200.0f + "'", float50 == 200.0f);
        org.junit.Assert.assertTrue("'" + int51 + "' != '" + 2 + "'", int51 == 2);
        org.junit.Assert.assertTrue("'" + boolean58 + "' != '" + false + "'", boolean58 == false);
        org.junit.Assert.assertTrue("'" + boolean61 + "' != '" + false + "'", boolean61 == false);
    }

    @Test
    public void test434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test434");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        java.lang.String str11 = funcionCuadratica0.toString();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+20100.0x" + "'", str11.equals("1.0x^2+20100.0x"));
    }

    @Test
    public void test435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test435");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica8);
        float float17 = funcionCuadratica8.eval(Float.NaN);
        funcionCuadratica8.setB(8.1206009E12f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
        org.junit.Assert.assertEquals((float) float17, Float.NaN, 0);
    }

    @Test
    public void test436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test436");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        funcionCuadratica3.setB(200.0f);
        float float9 = funcionCuadratica3.eval((-1.0f));
        funcionCuadratica3.setA((float) 1);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 99921.0f + "'", float9 == 99921.0f);
    }

    @Test
    public void test437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test437");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        float float8 = funcionCuadratica0.determinante();
        float float9 = funcionCuadratica0.getC();
        float float10 = funcionCuadratica0.getC();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 0.0f + "'", float8 == 0.0f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
    }

    @Test
    public void test438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test438");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (short) 10, 1.0046079E20f, 9.0f);
    }

    @Test
    public void test439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test439");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(2480625.0f, (float) (byte) 10, (-1400376.0f));
        float float4 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 2480625.0f + "'", float4 == 2480625.0f);
    }

    @Test
    public void test440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test440");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-2.901543E30f), 11.0f, Float.NEGATIVE_INFINITY);
    }

    @Test
    public void test441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test441");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        int int9 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setC((-40.0f));
        java.lang.String str12 = funcionCuadratica0.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica16 = new byThey.practico5.FuncionCuadratica(11000.0f, (float) '4', (float) 10L);
        float float18 = funcionCuadratica16.eval((-4404839.0f));
        java.lang.String str19 = funcionCuadratica16.toString();
        boolean boolean20 = funcionCuadratica0.equals(funcionCuadratica16);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2+10.0x+-40.0" + "'", str12.equals("1.0x^2+10.0x+-40.0"));
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 2.13428676E17f + "'", float18 == 2.13428676E17f);
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "11000.0x^2+52.0x+10.0" + "'", str19.equals("11000.0x^2+52.0x+10.0"));
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
    }

    @Test
    public void test442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test442");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-4.0f), 32.0f, 4.04009984E8f);
    }

    @Test
    public void test443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test443");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 2, (-40.0f), (float) ' ');
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str5 = funcionCuadratica4.toString();
        java.lang.Class<?> wildcardClass6 = funcionCuadratica4.getClass();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica4.getClass();
        java.lang.Object obj8 = funcionCuadratica4.raices();
        int int9 = funcionCuadratica4.numRaices();
        float float10 = funcionCuadratica4.getC();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica4.getClass();
        java.lang.Object obj12 = funcionCuadratica4.raices();
        boolean boolean13 = funcionCuadratica3.equals(funcionCuadratica4);
        funcionCuadratica3.setB(1.99685125E12f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2" + "'", str5.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(obj8);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + boolean13 + "' != '" + false + "'", boolean13 == false);
    }

    @Test
    public void test444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test444");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float6 = funcionCuadratica3.determinante();
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str8 = funcionCuadratica7.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica7.getClass();
        funcionCuadratica7.setC((float) (-1L));
        funcionCuadratica7.setB(11000.0f);
        boolean boolean14 = funcionCuadratica3.equals(funcionCuadratica7);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        java.lang.String str20 = funcionCuadratica15.toString();
        funcionCuadratica15.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica23 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica23.setB((float) 10);
        float float27 = funcionCuadratica23.eval((float) 10);
        float float29 = funcionCuadratica23.eval((-1.0f));
        int int30 = funcionCuadratica23.numRaices();
        float float32 = funcionCuadratica23.eval((float) 10L);
        float float33 = funcionCuadratica23.getB();
        boolean boolean34 = funcionCuadratica15.equals(funcionCuadratica23);
        funcionCuadratica23.setC((float) 10);
        funcionCuadratica23.setA((float) 1);
        java.lang.Class<?> wildcardClass39 = funcionCuadratica23.getClass();
        boolean boolean40 = funcionCuadratica3.equals(funcionCuadratica23);
        funcionCuadratica3.setC((float) (short) -1);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-4404839.0f) + "'", float6 == (-4404839.0f));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "1.0x^2+10.0x" + "'", str20.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 200.0f + "'", float27 == 200.0f);
        org.junit.Assert.assertTrue("'" + float29 + "' != '" + (-9.0f) + "'", float29 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 2 + "'", int30 == 2);
        org.junit.Assert.assertTrue("'" + float32 + "' != '" + 200.0f + "'", float32 == 200.0f);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 10.0f + "'", float33 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + false + "'", boolean34 == false);
        org.junit.Assert.assertNotNull(wildcardClass39);
        org.junit.Assert.assertTrue("'" + boolean40 + "' != '" + false + "'", boolean40 == false);
    }

    @Test
    public void test445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test445");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-384.0f), (float) (byte) 100, (float) '4');
    }

    @Test
    public void test446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test446");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        float float19 = funcionCuadratica14.eval(0.0f);
        java.lang.Class<?> wildcardClass20 = funcionCuadratica14.getClass();
        float float21 = funcionCuadratica14.determinante();
        java.lang.Class<?> wildcardClass22 = funcionCuadratica14.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 0.0f + "'", float19 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 1.0f + "'", float21 == 1.0f);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test447");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.determinante();
        float float6 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) 0);
        int int9 = funcionCuadratica0.numRaices();
        float float10 = funcionCuadratica0.getB();
        java.lang.Object obj11 = funcionCuadratica0.raices();
        funcionCuadratica0.setC(2.0f);
        funcionCuadratica0.setB(4.0f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + (-40.0f) + "'", float5 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertNotNull(obj11);
    }

    @Test
    public void test448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test448");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        float float8 = funcionCuadratica0.determinante();
        float float9 = funcionCuadratica0.determinante();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        float float11 = funcionCuadratica0.determinante();
        float float12 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 0.0f + "'", float8 == 0.0f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 0.0f + "'", float11 == 0.0f);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
    }

    @Test
    public void test449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test449");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica7.setB((float) 10);
        float float11 = funcionCuadratica7.eval((float) 10);
        float float13 = funcionCuadratica7.eval((-1.0f));
        java.lang.String str14 = funcionCuadratica7.toString();
        funcionCuadratica7.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica17 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str18 = funcionCuadratica17.toString();
        funcionCuadratica17.setC((float) 10);
        boolean boolean21 = funcionCuadratica7.equals(funcionCuadratica17);
        int int22 = funcionCuadratica17.numRaices();
        boolean boolean23 = funcionCuadratica0.equals(funcionCuadratica17);
        funcionCuadratica17.setC(4.04009984E8f);
        float float27 = funcionCuadratica17.eval((-3780.0f));
        float float28 = funcionCuadratica17.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 200.0f + "'", float11 == 200.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + (-9.0f) + "'", float13 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "1.0x^2+10.0x" + "'", str14.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2" + "'", str18.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean21 + "' != '" + false + "'", boolean21 == false);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22 == 0);
        org.junit.Assert.assertTrue("'" + boolean23 + "' != '" + false + "'", boolean23 == false);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 4.18298368E8f + "'", float27 == 4.18298368E8f);
        org.junit.Assert.assertTrue("'" + float28 + "' != '" + 1.0f + "'", float28 == 1.0f);
    }

    @Test
    public void test450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test450");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        int int13 = funcionCuadratica0.numRaices();
        float float15 = funcionCuadratica0.eval(1.99685125E12f);
        java.lang.String str16 = funcionCuadratica0.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica20 = new byThey.practico5.FuncionCuadratica((float) 2, (-40.0f), (float) ' ');
        java.lang.Object obj21 = funcionCuadratica20.raices();
        java.lang.String str22 = funcionCuadratica20.toString();
        boolean boolean23 = funcionCuadratica0.equals(funcionCuadratica20);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 3.987415E24f + "'", float15 == 3.987415E24f);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "1.0x^2+10.0x+-1.0" + "'", str16.equals("1.0x^2+10.0x+-1.0"));
        org.junit.Assert.assertNotNull(obj21);
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "2.0x^2+-40.0x+32.0" + "'", str22.equals("2.0x^2+-40.0x+32.0"));
        org.junit.Assert.assertTrue("'" + boolean23 + "' != '" + false + "'", boolean23 == false);
    }

    @Test
    public void test451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test451");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        java.lang.String str10 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.eval(0.0f);
        funcionCuadratica0.setB((-40.0f));
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
    }

    @Test
    public void test452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test452");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        float float5 = funcionCuadratica0.determinante();
        float float6 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) 0);
        int int9 = funcionCuadratica0.numRaices();
        float float10 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + (-40.0f) + "'", float5 == (-40.0f));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 1.0f + "'", float10 == 1.0f);
    }

    @Test
    public void test453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test453");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(2480625.0f, 1.16756784E8f, 8.1090048E7f);
        funcionCuadratica3.setB(9000.0f);
    }

    @Test
    public void test454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test454");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-1.0f), (float) 100, (float) 2);
        float float4 = funcionCuadratica3.getB();
        int int5 = funcionCuadratica3.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 100.0f + "'", float4 == 100.0f);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 2 + "'", int5 == 2);
    }

    @Test
    public void test455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test455");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.60363397E11f, 3.987415E24f, 110.0f);
        float float5 = funcionCuadratica3.eval(10272.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 4.095873E28f + "'", float5 == 4.095873E28f);
    }

    @Test
    public void test456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test456");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        float float3 = funcionCuadratica0.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        java.lang.String str11 = funcionCuadratica4.toString();
        float float12 = funcionCuadratica4.getB();
        java.lang.Object obj13 = funcionCuadratica4.raices();
        int int14 = funcionCuadratica4.numRaices();
        boolean boolean15 = funcionCuadratica0.equals(funcionCuadratica4);
        byThey.practico5.FuncionCuadratica funcionCuadratica16 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica16.setB((float) 10);
        float float20 = funcionCuadratica16.eval((float) 10);
        float float22 = funcionCuadratica16.eval((-1.0f));
        int int23 = funcionCuadratica16.numRaices();
        float float25 = funcionCuadratica16.eval((float) 10L);
        int int26 = funcionCuadratica16.numRaices();
        boolean boolean27 = funcionCuadratica4.equals(funcionCuadratica16);
        float float28 = funcionCuadratica4.getC();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 1.0f + "'", float3 == 1.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2+10.0x" + "'", str11.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14 == 2);
        org.junit.Assert.assertTrue("'" + boolean15 + "' != '" + false + "'", boolean15 == false);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 200.0f + "'", float20 == 200.0f);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + (-9.0f) + "'", float22 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2 + "'", int23 == 2);
        org.junit.Assert.assertTrue("'" + float25 + "' != '" + 200.0f + "'", float25 == 200.0f);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2 + "'", int26 == 2);
        org.junit.Assert.assertTrue("'" + boolean27 + "' != '" + true + "'", boolean27 == true);
        org.junit.Assert.assertTrue("'" + float28 + "' != '" + 0.0f + "'", float28 == 0.0f);
    }

    @Test
    public void test457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test457");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(11.0f, 1.02512616E8f, (-400.0f));
    }

    @Test
    public void test458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test458");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        java.lang.String str10 = funcionCuadratica0.toString();
        java.lang.Object obj11 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica((-4404839.0f), (float) 0, (float) 1);
        boolean boolean16 = funcionCuadratica0.equals(funcionCuadratica15);
        int int17 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(obj11);
        org.junit.Assert.assertTrue("'" + boolean16 + "' != '" + false + "'", boolean16 == false);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 2 + "'", int17 == 2);
    }

    @Test
    public void test459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test459");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        float float9 = funcionCuadratica0.getC();
        java.lang.String str10 = funcionCuadratica0.toString();
        float float12 = funcionCuadratica0.eval(0.0f);
        funcionCuadratica0.setB(0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x" + "'", str10.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
    }

    @Test
    public void test460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test460");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(10000.0f, 1575.0f, 2480625.0f);
    }

    @Test
    public void test461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test461");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getA();
        float float5 = funcionCuadratica0.getC();
        float float7 = funcionCuadratica0.eval((float) (byte) 0);
        funcionCuadratica0.setC(2.13428676E17f);
        float float10 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 0.0f + "'", float7 == 0.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-8.537147E17f) + "'", float10 == (-8.537147E17f));
    }

    @Test
    public void test462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test462");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        funcionCuadratica3.setB(3.987415E24f);
        float float8 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 3.987415E24f + "'", float8 == 3.987415E24f);
    }

    @Test
    public void test463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test463");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        int int5 = funcionCuadratica0.numRaices();
        float float6 = funcionCuadratica0.getC();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica0.getClass();
        java.lang.String str8 = funcionCuadratica0.toString();
        int int9 = funcionCuadratica0.numRaices();
        float float10 = funcionCuadratica0.determinante();
        float float11 = funcionCuadratica0.determinante();
        int int12 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setA(1.16756784E8f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 0.0f + "'", float6 == 0.0f);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9 == 1);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 0.0f + "'", float11 == 0.0f);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1 + "'", int12 == 1);
    }

    @Test
    public void test464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test464");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        byThey.practico5.FuncionCuadratica funcionCuadratica6 = new byThey.practico5.FuncionCuadratica((float) (short) 1, Float.POSITIVE_INFINITY, 1.60363397E11f);
        float float7 = funcionCuadratica6.getC();
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        int int18 = funcionCuadratica8.numRaices();
        java.lang.String str19 = funcionCuadratica8.toString();
        float float20 = funcionCuadratica8.getC();
        funcionCuadratica8.setA((-4404839.0f));
        float float23 = funcionCuadratica8.determinante();
        boolean boolean24 = funcionCuadratica6.equals(funcionCuadratica8);
        boolean boolean25 = funcionCuadratica0.equals(funcionCuadratica6);
        java.lang.Class<?> wildcardClass26 = funcionCuadratica6.getClass();
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 1.60363397E11f + "'", float7 == 1.60363397E11f);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "1.0x^2+10.0x" + "'", str19.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 0.0f + "'", float20 == 0.0f);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 100.0f + "'", float23 == 100.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + false + "'", boolean24 == false);
        org.junit.Assert.assertTrue("'" + boolean25 + "' != '" + false + "'", boolean25 == false);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test465");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        boolean boolean24 = funcionCuadratica0.equals(funcionCuadratica15);
        java.lang.String str25 = funcionCuadratica15.toString();
        funcionCuadratica15.setC(200.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + true + "'", boolean24 == true);
        org.junit.Assert.assertTrue("'" + str25 + "' != '" + "1.0x^2+10.0x" + "'", str25.equals("1.0x^2+10.0x"));
    }

    @Test
    public void test466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test466");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        float float9 = funcionCuadratica3.eval(1.0f);
        int int10 = funcionCuadratica3.numRaices();
        float float11 = funcionCuadratica3.determinante();
        funcionCuadratica3.setA(11.0f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 11.0f + "'", float9 == 11.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 1.0f + "'", float11 == 1.0f);
    }

    @Test
    public void test467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test467");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-1281279.0f), (-9.0f), 0.0f);
    }

    @Test
    public void test468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test468");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (-1L));
        float float8 = funcionCuadratica3.getC();
        java.lang.String str9 = funcionCuadratica3.toString();
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 0.0f + "'", float8 == 0.0f);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "10.0x^2+-1.0x" + "'", str9.equals("10.0x^2+-1.0x"));
    }

    @Test
    public void test469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test469");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC((float) (-1L));
        funcionCuadratica0.setB(11000.0f);
        float float7 = funcionCuadratica0.getB();
        funcionCuadratica0.setC(3.22921071E11f);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 11000.0f + "'", float7 == 11000.0f);
    }

    @Test
    public void test470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test470");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        funcionCuadratica0.setA((float) '4');
        funcionCuadratica0.setC((-40.0f));
        java.lang.Object obj9 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica10.setB((float) 10);
        float float14 = funcionCuadratica10.eval((float) 10);
        float float16 = funcionCuadratica10.eval((-1.0f));
        int int17 = funcionCuadratica10.numRaices();
        float float19 = funcionCuadratica10.eval((float) 10L);
        int int20 = funcionCuadratica10.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica24 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica24.setB((float) 1);
        boolean boolean27 = funcionCuadratica10.equals(funcionCuadratica24);
        java.lang.String str28 = funcionCuadratica24.toString();
        boolean boolean29 = funcionCuadratica0.equals(funcionCuadratica24);
        byThey.practico5.FuncionCuadratica funcionCuadratica30 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica30.setB((float) 10);
        float float34 = funcionCuadratica30.eval((float) 10);
        float float36 = funcionCuadratica30.eval((-1.0f));
        int int37 = funcionCuadratica30.numRaices();
        float float38 = funcionCuadratica30.getB();
        java.lang.Class<?> wildcardClass39 = funcionCuadratica30.getClass();
        float float40 = funcionCuadratica30.getC();
        boolean boolean41 = funcionCuadratica24.equals(funcionCuadratica30);
        float float42 = funcionCuadratica24.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 200.0f + "'", float14 == 200.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + (-9.0f) + "'", float16 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 2 + "'", int17 == 2);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2 + "'", int20 == 2);
        org.junit.Assert.assertTrue("'" + boolean27 + "' != '" + false + "'", boolean27 == false);
        org.junit.Assert.assertTrue("'" + str28 + "' != '" + "10.0x^2+1.0x" + "'", str28.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + boolean29 + "' != '" + false + "'", boolean29 == false);
        org.junit.Assert.assertTrue("'" + float34 + "' != '" + 200.0f + "'", float34 == 200.0f);
        org.junit.Assert.assertTrue("'" + float36 + "' != '" + (-9.0f) + "'", float36 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 2 + "'", int37 == 2);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 10.0f + "'", float38 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass39);
        org.junit.Assert.assertTrue("'" + float40 + "' != '" + 0.0f + "'", float40 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean41 + "' != '" + false + "'", boolean41 == false);
        org.junit.Assert.assertTrue("'" + float42 + "' != '" + 1.0f + "'", float42 == 1.0f);
    }

    @Test
    public void test471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test471");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        java.lang.String str8 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass10 = funcionCuadratica0.getClass();
        float float11 = funcionCuadratica0.getA();
        java.lang.String str12 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(121.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2+10.0x" + "'", str8.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 1.0f + "'", float11 == 1.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2+10.0x" + "'", str12.equals("1.0x^2+10.0x"));
    }

    @Test
    public void test472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test472");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) (byte) 100);
        funcionCuadratica0.setB(0.0f);
        float float12 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
    }

    @Test
    public void test473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test473");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float4 = funcionCuadratica3.getC();
        funcionCuadratica3.setB(1.60363397E11f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 11.0f + "'", float4 == 11.0f);
    }

    @Test
    public void test474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test474");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 100L, 1.0f, 2.5716419E22f);
        float float4 = funcionCuadratica3.determinante();
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str6 = funcionCuadratica5.toString();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica5.getClass();
        funcionCuadratica5.setC((float) (-1L));
        funcionCuadratica5.setB(11000.0f);
        float float12 = funcionCuadratica5.getA();
        java.lang.Class<?> wildcardClass13 = funcionCuadratica5.getClass();
        funcionCuadratica5.setC(9000.0f);
        boolean boolean16 = funcionCuadratica3.equals(funcionCuadratica5);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + (-1.0286567E25f) + "'", float4 == (-1.0286567E25f));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "1.0x^2" + "'", str6.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 1.0f + "'", float12 == 1.0f);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + boolean16 + "' != '" + false + "'", boolean16 == false);
    }

    @Test
    public void test475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test475");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setA((float) '4');
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
    }

    @Test
    public void test476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test476");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(2.0f, 1.0f, 32.0f);
    }

    @Test
    public void test477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test477");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        funcionCuadratica0.setA(10.0f);
        funcionCuadratica0.setA((-3780.0f));
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
    }

    @Test
    public void test478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test478");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        float float1 = funcionCuadratica0.determinante();
        int int2 = funcionCuadratica0.numRaices();
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getC();
        byThey.practico5.FuncionCuadratica funcionCuadratica5 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica5.setB((float) 10);
        float float9 = funcionCuadratica5.eval((float) 10);
        funcionCuadratica5.setA((float) '4');
        funcionCuadratica5.setC((-40.0f));
        java.lang.Object obj14 = funcionCuadratica5.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float24 = funcionCuadratica15.eval((float) 10L);
        int int25 = funcionCuadratica15.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica29 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica29.setB((float) 1);
        boolean boolean32 = funcionCuadratica15.equals(funcionCuadratica29);
        java.lang.String str33 = funcionCuadratica29.toString();
        boolean boolean34 = funcionCuadratica5.equals(funcionCuadratica29);
        byThey.practico5.FuncionCuadratica funcionCuadratica35 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica35.setB((float) 10);
        float float39 = funcionCuadratica35.eval((float) 10);
        float float41 = funcionCuadratica35.eval((-1.0f));
        int int42 = funcionCuadratica35.numRaices();
        float float43 = funcionCuadratica35.getB();
        java.lang.Class<?> wildcardClass44 = funcionCuadratica35.getClass();
        float float45 = funcionCuadratica35.getC();
        boolean boolean46 = funcionCuadratica29.equals(funcionCuadratica35);
        boolean boolean47 = funcionCuadratica0.equals(funcionCuadratica35);
        org.junit.Assert.assertTrue("'" + float1 + "' != '" + 0.0f + "'", float1 == 0.0f);
        org.junit.Assert.assertTrue("'" + int2 + "' != '" + 1 + "'", int2 == 1);
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 0.0f + "'", float3 == 0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj14);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 200.0f + "'", float24 == 200.0f);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + boolean32 + "' != '" + false + "'", boolean32 == false);
        org.junit.Assert.assertTrue("'" + str33 + "' != '" + "10.0x^2+1.0x" + "'", str33.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + boolean34 + "' != '" + false + "'", boolean34 == false);
        org.junit.Assert.assertTrue("'" + float39 + "' != '" + 200.0f + "'", float39 == 200.0f);
        org.junit.Assert.assertTrue("'" + float41 + "' != '" + (-9.0f) + "'", float41 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int42 + "' != '" + 2 + "'", int42 == 2);
        org.junit.Assert.assertTrue("'" + float43 + "' != '" + 10.0f + "'", float43 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass44);
        org.junit.Assert.assertTrue("'" + float45 + "' != '" + 0.0f + "'", float45 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean46 + "' != '" + false + "'", boolean46 == false);
        org.junit.Assert.assertTrue("'" + boolean47 + "' != '" + false + "'", boolean47 == false);
    }

    @Test
    public void test479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test479");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(1.00200096E8f, 200.0f, (float) 2);
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        int int11 = funcionCuadratica4.numRaices();
        float float13 = funcionCuadratica4.eval((float) 10L);
        funcionCuadratica4.setA((float) (byte) 100);
        funcionCuadratica4.setC((float) (byte) 1);
        float float18 = funcionCuadratica4.getB();
        float float19 = funcionCuadratica4.determinante();
        int int20 = funcionCuadratica4.numRaices();
        boolean boolean21 = funcionCuadratica3.equals(funcionCuadratica4);
        float float23 = funcionCuadratica4.eval(3.8338992E20f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 2 + "'", int11 == 2);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 200.0f + "'", float13 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + (-300.0f) + "'", float19 == (-300.0f));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20 == 0);
        org.junit.Assert.assertTrue("'" + boolean21 + "' != '" + false + "'", boolean21 == false);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + Float.POSITIVE_INFINITY + "'", float23 == Float.POSITIVE_INFINITY);
    }

    @Test
    public void test480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test480");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        int int3 = funcionCuadratica0.numRaices();
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        java.lang.Object obj5 = funcionCuadratica0.raices();
        funcionCuadratica0.setC((float) 10L);
        float float8 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 2 + "'", int3 == 2);
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(obj5);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
    }

    @Test
    public void test481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test481");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getB();
        float float10 = funcionCuadratica3.getA();
        float float11 = funcionCuadratica3.getA();
        float float13 = funcionCuadratica3.eval(811.0f);
        float float14 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 1.0f + "'", float9 == 1.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 10.0f + "'", float10 == 10.0f);
        org.junit.Assert.assertTrue("'" + float11 + "' != '" + 10.0f + "'", float11 == 10.0f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 6578021.0f + "'", float13 == 6578021.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 10.0f + "'", float14 == 10.0f);
    }

    @Test
    public void test482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test482");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        int int10 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica14);
        java.lang.String str18 = funcionCuadratica0.toString();
        float float19 = funcionCuadratica0.getB();
        funcionCuadratica0.setA((-300.0f));
        java.lang.Class<?> wildcardClass22 = funcionCuadratica0.getClass();
        float float23 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2 + "'", int10 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "1.0x^2+10.0x" + "'", str18.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 10.0f + "'", float19 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
    }

    @Test
    public void test483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test483");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        funcionCuadratica0.setC((float) (-1));
        funcionCuadratica0.setA((float) (short) 100);
        java.lang.String str17 = funcionCuadratica0.toString();
        int int18 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "100.0x^2+10.0x+-1.0" + "'", str17.equals("100.0x^2+10.0x+-1.0"));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
    }

    @Test
    public void test484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test484");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float3 = funcionCuadratica0.determinante();
        float float4 = funcionCuadratica0.getA();
        float float5 = funcionCuadratica0.getA();
        java.lang.Object obj6 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float3 + "' != '" + 100.0f + "'", float3 == 100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.0f + "'", float4 == 1.0f);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 1.0f + "'", float5 == 1.0f);
        org.junit.Assert.assertNotNull(obj6);
    }

    @Test
    public void test485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test485");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        java.lang.String str9 = funcionCuadratica3.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        java.lang.Class<?> wildcardClass12 = funcionCuadratica10.getClass();
        funcionCuadratica10.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        int int22 = funcionCuadratica15.numRaices();
        float float24 = funcionCuadratica15.eval((float) 10L);
        int int25 = funcionCuadratica15.numRaices();
        java.lang.String str26 = funcionCuadratica15.toString();
        float float27 = funcionCuadratica15.getB();
        boolean boolean28 = funcionCuadratica10.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica29 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica29.setB((float) 10);
        float float33 = funcionCuadratica29.eval((float) 10);
        float float35 = funcionCuadratica29.eval((-1.0f));
        int int36 = funcionCuadratica29.numRaices();
        float float38 = funcionCuadratica29.eval((float) 10L);
        funcionCuadratica29.setA((float) (byte) 100);
        funcionCuadratica29.setC((float) (byte) 1);
        funcionCuadratica29.setC((float) (short) 10);
        int int45 = funcionCuadratica29.numRaices();
        boolean boolean46 = funcionCuadratica15.equals(funcionCuadratica29);
        boolean boolean47 = funcionCuadratica3.equals(funcionCuadratica29);
        funcionCuadratica29.setB((float) (byte) 1);
        try {
            java.lang.Object obj50 = funcionCuadratica29.raices();
            org.junit.Assert.fail("Expected exception of type byThey.practico5.PolinomioException; message: esta función no tiene raíces reales.");
        } catch (byThey.practico5.PolinomioException e) {
        }
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "10.0x^2+1.0x" + "'", str9.equals("10.0x^2+1.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22 == 2);
        org.junit.Assert.assertTrue("'" + float24 + "' != '" + 200.0f + "'", float24 == 200.0f);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 10.0f + "'", float27 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + 200.0f + "'", float33 == 200.0f);
        org.junit.Assert.assertTrue("'" + float35 + "' != '" + (-9.0f) + "'", float35 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 2 + "'", int36 == 2);
        org.junit.Assert.assertTrue("'" + float38 + "' != '" + 200.0f + "'", float38 == 200.0f);
        org.junit.Assert.assertTrue("'" + int45 + "' != '" + 0 + "'", int45 == 0);
        org.junit.Assert.assertTrue("'" + boolean46 + "' != '" + false + "'", boolean46 == false);
        org.junit.Assert.assertTrue("'" + boolean47 + "' != '" + false + "'", boolean47 == false);
    }

    @Test
    public void test486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test486");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(32.0f, (float) (short) -1, 90.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica4.setB((float) 10);
        float float8 = funcionCuadratica4.eval((float) 10);
        float float10 = funcionCuadratica4.eval((-1.0f));
        int int11 = funcionCuadratica4.numRaices();
        java.lang.String str12 = funcionCuadratica4.toString();
        java.lang.Class<?> wildcardClass13 = funcionCuadratica4.getClass();
        java.lang.Object obj14 = funcionCuadratica4.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica18 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.Class<?> wildcardClass19 = funcionCuadratica18.getClass();
        boolean boolean20 = funcionCuadratica4.equals(funcionCuadratica18);
        float float21 = funcionCuadratica4.getC();
        boolean boolean22 = funcionCuadratica3.equals(funcionCuadratica4);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 200.0f + "'", float8 == 200.0f);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + (-9.0f) + "'", float10 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 2 + "'", int11 == 2);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2+10.0x" + "'", str12.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(obj14);
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + 0.0f + "'", float21 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean22 + "' != '" + false + "'", boolean22 == false);
    }

    @Test
    public void test487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test487");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        funcionCuadratica0.setA(10.0f);
        float float22 = funcionCuadratica0.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 10.0f + "'", float22 == 10.0f);
    }

    @Test
    public void test488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test488");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        int int9 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica13 = new byThey.practico5.FuncionCuadratica((float) (byte) 1, (float) (-1L), (float) (byte) 0);
        float float14 = funcionCuadratica13.getC();
        float float16 = funcionCuadratica13.eval((float) (byte) 10);
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica13);
        float float18 = funcionCuadratica0.determinante();
        java.lang.Object obj19 = funcionCuadratica0.raices();
        funcionCuadratica0.setC(179700.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9 == 2);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 0.0f + "'", float14 == 0.0f);
        org.junit.Assert.assertTrue("'" + float16 + "' != '" + 90.0f + "'", float16 == 90.0f);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 100.0f + "'", float18 == 100.0f);
        org.junit.Assert.assertNotNull(obj19);
    }

    @Test
    public void test489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test489");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        float float2 = funcionCuadratica0.getB();
        float float4 = funcionCuadratica0.eval(1.00200096E8f);
        int int5 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + float2 + "' != '" + 0.0f + "'", float2 == 0.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 1.00400594E16f + "'", float4 == 1.00400594E16f);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5 == 1);
    }

    @Test
    public void test490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test490");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) (byte) 0);
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str12 = funcionCuadratica11.toString();
        java.lang.Class<?> wildcardClass13 = funcionCuadratica11.getClass();
        float float14 = funcionCuadratica11.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        java.lang.Object obj24 = funcionCuadratica15.raices();
        int int25 = funcionCuadratica15.numRaices();
        boolean boolean26 = funcionCuadratica11.equals(funcionCuadratica15);
        float float27 = funcionCuadratica11.getC();
        boolean boolean28 = funcionCuadratica0.equals(funcionCuadratica11);
        java.lang.String str29 = funcionCuadratica0.toString();
        java.lang.Object obj30 = funcionCuadratica0.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2" + "'", str12.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 1.0f + "'", float14 == 1.0f);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertNotNull(obj24);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + boolean26 + "' != '" + false + "'", boolean26 == false);
        org.junit.Assert.assertTrue("'" + float27 + "' != '" + 0.0f + "'", float27 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + str29 + "' != '" + "1.0x^2+10.0x" + "'", str29.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(obj30);
    }

    @Test
    public void test491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test491");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((-108.0f), (-40.0f), 811.0f);
    }

    @Test
    public void test492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test492");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica11.setB((float) 1);
        funcionCuadratica11.setB((float) (byte) 1);
        int int16 = funcionCuadratica11.numRaices();
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica11);
        java.lang.Class<?> wildcardClass18 = funcionCuadratica0.getClass();
        float float19 = funcionCuadratica0.getA();
        java.lang.Class<?> wildcardClass20 = funcionCuadratica0.getClass();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2 + "'", int16 == 2);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 1.0f + "'", float19 == 1.0f);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test493");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        java.lang.Class<?> wildcardClass3 = funcionCuadratica0.getClass();
        java.lang.Object obj4 = funcionCuadratica0.raices();
        float float5 = funcionCuadratica0.getB();
        java.lang.Object obj6 = funcionCuadratica0.raices();
        byThey.practico5.FuncionCuadratica funcionCuadratica7 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str8 = funcionCuadratica7.toString();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica7.getClass();
        java.lang.Class<?> wildcardClass10 = funcionCuadratica7.getClass();
        java.lang.Object obj11 = funcionCuadratica7.raices();
        float float12 = funcionCuadratica7.getB();
        java.lang.Object obj13 = funcionCuadratica7.raices();
        java.lang.String str14 = funcionCuadratica7.toString();
        funcionCuadratica7.setA((-300.0f));
        boolean boolean17 = funcionCuadratica0.equals(funcionCuadratica7);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(obj4);
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertNotNull(obj6);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(obj11);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 0.0f + "'", float12 == 0.0f);
        org.junit.Assert.assertNotNull(obj13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "1.0x^2" + "'", str14.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
    }

    @Test
    public void test494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test494");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        byThey.practico5.FuncionCuadratica funcionCuadratica8 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica8.setB((float) 10);
        float float12 = funcionCuadratica8.eval((float) 10);
        float float14 = funcionCuadratica8.eval((-1.0f));
        int int15 = funcionCuadratica8.numRaices();
        float float17 = funcionCuadratica8.eval((float) 10L);
        float float18 = funcionCuadratica8.getB();
        boolean boolean19 = funcionCuadratica0.equals(funcionCuadratica8);
        java.lang.Object obj20 = funcionCuadratica8.raices();
        float float22 = funcionCuadratica8.eval(1.0f);
        java.lang.Object obj23 = funcionCuadratica8.raices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 200.0f + "'", float12 == 200.0f);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + (-9.0f) + "'", float14 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15 == 2);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + 200.0f + "'", float17 == 200.0f);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 10.0f + "'", float18 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean19 + "' != '" + false + "'", boolean19 == false);
        org.junit.Assert.assertNotNull(obj20);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 11.0f + "'", float22 == 11.0f);
        org.junit.Assert.assertNotNull(obj23);
    }

    @Test
    public void test495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test495");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        java.lang.Object obj8 = funcionCuadratica0.raices();
        float float9 = funcionCuadratica0.getC();
        funcionCuadratica0.setC(0.0f);
        float float12 = funcionCuadratica0.getB();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(obj8);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 0.0f + "'", float9 == 0.0f);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 10.0f + "'", float12 == 10.0f);
    }

    @Test
    public void test496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test496");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 100, (float) ' ', 9.0f);
    }

    @Test
    public void test497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test497");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        float float8 = funcionCuadratica0.getB();
        funcionCuadratica0.setC((float) '4');
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica11.setB((float) 10);
        float float15 = funcionCuadratica11.eval((float) 10);
        float float17 = funcionCuadratica11.eval((-1.0f));
        int int18 = funcionCuadratica11.numRaices();
        float float20 = funcionCuadratica11.eval((float) 10L);
        int int21 = funcionCuadratica11.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica25 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica25.setB((float) 1);
        boolean boolean28 = funcionCuadratica11.equals(funcionCuadratica25);
        java.lang.String str29 = funcionCuadratica11.toString();
        funcionCuadratica11.setC((-1.0f));
        boolean boolean32 = funcionCuadratica0.equals(funcionCuadratica11);
        int int33 = funcionCuadratica11.numRaices();
        float float35 = funcionCuadratica11.eval((-1.0f));
        funcionCuadratica11.setA(100.0f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 200.0f + "'", float15 == 200.0f);
        org.junit.Assert.assertTrue("'" + float17 + "' != '" + (-9.0f) + "'", float17 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18 == 2);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + 200.0f + "'", float20 == 200.0f);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + boolean28 + "' != '" + false + "'", boolean28 == false);
        org.junit.Assert.assertTrue("'" + str29 + "' != '" + "1.0x^2+10.0x" + "'", str29.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + boolean32 + "' != '" + false + "'", boolean32 == false);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 2 + "'", int33 == 2);
        org.junit.Assert.assertTrue("'" + float35 + "' != '" + (-10.0f) + "'", float35 == (-10.0f));
    }

    @Test
    public void test498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test498");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float9 = funcionCuadratica0.eval((float) 10L);
        funcionCuadratica0.setC((float) (byte) -1);
        java.lang.Object obj12 = funcionCuadratica0.raices();
        int int13 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str15 = funcionCuadratica14.toString();
        java.lang.Class<?> wildcardClass16 = funcionCuadratica14.getClass();
        java.lang.Class<?> wildcardClass17 = funcionCuadratica14.getClass();
        java.lang.Object obj18 = funcionCuadratica14.raices();
        int int19 = funcionCuadratica14.numRaices();
        boolean boolean20 = funcionCuadratica0.equals(funcionCuadratica14);
        float float22 = funcionCuadratica14.eval(10.0f);
        float float23 = funcionCuadratica14.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 200.0f + "'", float9 == 200.0f);
        org.junit.Assert.assertNotNull(obj12);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2 + "'", int13 == 2);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "1.0x^2" + "'", str15.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(obj18);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1 + "'", int19 == 1);
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
        org.junit.Assert.assertTrue("'" + float22 + "' != '" + 100.0f + "'", float22 == 100.0f);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 0.0f + "'", float23 == 0.0f);
    }

    @Test
    public void test499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test499");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        funcionCuadratica0.setC((float) 10);
        java.lang.Class<?> wildcardClass4 = funcionCuadratica0.getClass();
        funcionCuadratica0.setC(0.0f);
        funcionCuadratica0.setB(20100.0f);
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        java.lang.Object obj10 = funcionCuadratica0.raices();
        float float12 = funcionCuadratica0.eval(1.99685125E12f);
        float float13 = funcionCuadratica0.determinante();
        int int14 = funcionCuadratica0.numRaices();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(obj10);
        org.junit.Assert.assertTrue("'" + float12 + "' != '" + 3.987415E24f + "'", float12 == 3.987415E24f);
        org.junit.Assert.assertTrue("'" + float13 + "' != '" + 4.04009984E8f + "'", float13 == 4.04009984E8f);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14 == 2);
    }

    @Test
    public void test500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test500");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        java.lang.String str5 = funcionCuadratica0.toString();
        funcionCuadratica0.setA((float) (short) -1);
        java.lang.Object obj8 = funcionCuadratica0.raices();
        float float10 = funcionCuadratica0.eval((float) 0L);
        byThey.practico5.FuncionCuadratica funcionCuadratica11 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str12 = funcionCuadratica11.toString();
        java.lang.Class<?> wildcardClass13 = funcionCuadratica11.getClass();
        float float14 = funcionCuadratica11.getA();
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        java.lang.Object obj24 = funcionCuadratica15.raices();
        int int25 = funcionCuadratica15.numRaices();
        boolean boolean26 = funcionCuadratica11.equals(funcionCuadratica15);
        byThey.practico5.FuncionCuadratica funcionCuadratica27 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica27.setB((float) 10);
        float float31 = funcionCuadratica27.eval((float) 10);
        float float33 = funcionCuadratica27.eval((-1.0f));
        int int34 = funcionCuadratica27.numRaices();
        float float36 = funcionCuadratica27.eval((float) 10L);
        int int37 = funcionCuadratica27.numRaices();
        boolean boolean38 = funcionCuadratica15.equals(funcionCuadratica27);
        boolean boolean39 = funcionCuadratica0.equals(funcionCuadratica15);
        float float40 = funcionCuadratica15.getC();
        float float41 = funcionCuadratica15.getB();
        byThey.practico5.FuncionCuadratica funcionCuadratica42 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica42.setB((float) 10);
        float float46 = funcionCuadratica42.eval((float) 10);
        float float48 = funcionCuadratica42.eval((-1.0f));
        int int49 = funcionCuadratica42.numRaices();
        float float51 = funcionCuadratica42.eval((float) 10L);
        float float52 = funcionCuadratica42.getC();
        float float53 = funcionCuadratica42.getB();
        float float54 = funcionCuadratica42.getA();
        float float55 = funcionCuadratica42.getC();
        boolean boolean56 = funcionCuadratica15.equals(funcionCuadratica42);
        funcionCuadratica42.setB(2.00450253E10f);
        int int59 = funcionCuadratica42.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2+10.0x" + "'", str5.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(obj8);
        org.junit.Assert.assertTrue("'" + float10 + "' != '" + 0.0f + "'", float10 == 0.0f);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "1.0x^2" + "'", str12.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + float14 + "' != '" + 1.0f + "'", float14 == 1.0f);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertNotNull(obj24);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2 + "'", int25 == 2);
        org.junit.Assert.assertTrue("'" + boolean26 + "' != '" + false + "'", boolean26 == false);
        org.junit.Assert.assertTrue("'" + float31 + "' != '" + 200.0f + "'", float31 == 200.0f);
        org.junit.Assert.assertTrue("'" + float33 + "' != '" + (-9.0f) + "'", float33 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 2 + "'", int34 == 2);
        org.junit.Assert.assertTrue("'" + float36 + "' != '" + 200.0f + "'", float36 == 200.0f);
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 2 + "'", int37 == 2);
        org.junit.Assert.assertTrue("'" + boolean38 + "' != '" + true + "'", boolean38 == true);
        org.junit.Assert.assertTrue("'" + boolean39 + "' != '" + false + "'", boolean39 == false);
        org.junit.Assert.assertTrue("'" + float40 + "' != '" + 0.0f + "'", float40 == 0.0f);
        org.junit.Assert.assertTrue("'" + float41 + "' != '" + 10.0f + "'", float41 == 10.0f);
        org.junit.Assert.assertTrue("'" + float46 + "' != '" + 200.0f + "'", float46 == 200.0f);
        org.junit.Assert.assertTrue("'" + float48 + "' != '" + (-9.0f) + "'", float48 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int49 + "' != '" + 2 + "'", int49 == 2);
        org.junit.Assert.assertTrue("'" + float51 + "' != '" + 200.0f + "'", float51 == 200.0f);
        org.junit.Assert.assertTrue("'" + float52 + "' != '" + 0.0f + "'", float52 == 0.0f);
        org.junit.Assert.assertTrue("'" + float53 + "' != '" + 10.0f + "'", float53 == 10.0f);
        org.junit.Assert.assertTrue("'" + float54 + "' != '" + 1.0f + "'", float54 == 1.0f);
        org.junit.Assert.assertTrue("'" + float55 + "' != '" + 0.0f + "'", float55 == 0.0f);
        org.junit.Assert.assertTrue("'" + boolean56 + "' != '" + true + "'", boolean56 == true);
        org.junit.Assert.assertTrue("'" + int59 + "' != '" + 2 + "'", int59 == 2);
    }
}

