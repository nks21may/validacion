package test.practico5.FuncionCuadratica;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest2 {

    public static boolean debug = false;

    @Test
    public void test01() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test01");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        funcionCuadratica0.setC(11000.0f);
        java.lang.String str10 = funcionCuadratica0.toString();
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica14.setB((float) 1);
        funcionCuadratica14.setA((float) 2);
        int int19 = funcionCuadratica14.numRaices();
        boolean boolean20 = funcionCuadratica0.equals(funcionCuadratica14);
        float float21 = funcionCuadratica0.determinante();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2+10.0x+11000.0" + "'", str10.equals("1.0x^2+10.0x+11000.0"));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2 + "'", int19 == 2);
        org.junit.Assert.assertTrue("'" + boolean20 + "' != '" + false + "'", boolean20 == false);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-43900.0f) + "'", float21 == (-43900.0f));
    }

    @Test
    public void test02() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test02");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(100110.0f, (float) (byte) -1, 11.0f);
        float float5 = funcionCuadratica3.eval(0.0f);
        float float7 = funcionCuadratica3.eval((float) 2);
        float float8 = funcionCuadratica3.getC();
        byThey.practico5.FuncionCuadratica funcionCuadratica9 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str10 = funcionCuadratica9.toString();
        java.lang.Class<?> wildcardClass11 = funcionCuadratica9.getClass();
        funcionCuadratica9.setC((float) (-1L));
        byThey.practico5.FuncionCuadratica funcionCuadratica14 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica14.setB((float) 10);
        float float18 = funcionCuadratica14.eval((float) 10);
        float float20 = funcionCuadratica14.eval((-1.0f));
        int int21 = funcionCuadratica14.numRaices();
        float float23 = funcionCuadratica14.eval((float) 10L);
        int int24 = funcionCuadratica14.numRaices();
        java.lang.String str25 = funcionCuadratica14.toString();
        float float26 = funcionCuadratica14.getB();
        boolean boolean27 = funcionCuadratica9.equals(funcionCuadratica14);
        java.lang.String str28 = funcionCuadratica9.toString();
        boolean boolean29 = funcionCuadratica3.equals(funcionCuadratica9);
        int int30 = funcionCuadratica3.numRaices();
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 11.0f + "'", float5 == 11.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 400449.0f + "'", float7 == 400449.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 11.0f + "'", float8 == 11.0f);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "1.0x^2" + "'", str10.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + float18 + "' != '" + 200.0f + "'", float18 == 200.0f);
        org.junit.Assert.assertTrue("'" + float20 + "' != '" + (-9.0f) + "'", float20 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21 == 2);
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 200.0f + "'", float23 == 200.0f);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2 + "'", int24 == 2);
        org.junit.Assert.assertTrue("'" + str25 + "' != '" + "1.0x^2+10.0x" + "'", str25.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float26 + "' != '" + 10.0f + "'", float26 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean27 + "' != '" + false + "'", boolean27 == false);
        org.junit.Assert.assertTrue("'" + str28 + "' != '" + "1.0x^2+-1.0" + "'", str28.equals("1.0x^2+-1.0"));
        org.junit.Assert.assertTrue("'" + boolean29 + "' != '" + false + "'", boolean29 == false);
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 0 + "'", int30 == 0);
    }

    @Test
    public void test03() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test03");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(90.0f, (-384.0f), 89999.0f);
        float float4 = funcionCuadratica3.getC();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 89999.0f + "'", float4 == 89999.0f);
    }

    @Test
    public void test04() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test04");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str1 = funcionCuadratica0.toString();
        java.lang.Class<?> wildcardClass2 = funcionCuadratica0.getClass();
        int int3 = funcionCuadratica0.numRaices();
        byThey.practico5.FuncionCuadratica funcionCuadratica4 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str5 = funcionCuadratica4.toString();
        java.lang.Class<?> wildcardClass6 = funcionCuadratica4.getClass();
        java.lang.Class<?> wildcardClass7 = funcionCuadratica4.getClass();
        java.lang.String str8 = funcionCuadratica4.toString();
        java.lang.Object obj9 = funcionCuadratica4.raices();
        int int10 = funcionCuadratica4.numRaices();
        boolean boolean11 = funcionCuadratica0.equals(funcionCuadratica4);
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "1.0x^2" + "'", str1.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 1 + "'", int3 == 1);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "1.0x^2" + "'", str5.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "1.0x^2" + "'", str8.equals("1.0x^2"));
        org.junit.Assert.assertNotNull(obj9);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1 + "'", int10 == 1);
        org.junit.Assert.assertTrue("'" + boolean11 + "' != '" + true + "'", boolean11 == true);
    }

    @Test
    public void test05() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test05");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        int int7 = funcionCuadratica0.numRaices();
        float float8 = funcionCuadratica0.getB();
        java.lang.Class<?> wildcardClass9 = funcionCuadratica0.getClass();
        try {
            funcionCuadratica0.setA((float) (byte) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: a cant be 0");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7 == 2);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + 10.0f + "'", float8 == 10.0f);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test06() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test06");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        java.lang.String str4 = funcionCuadratica3.toString();
        float float5 = funcionCuadratica3.getC();
        funcionCuadratica3.setB(3.8338992E20f);
        float float8 = funcionCuadratica3.determinante();
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "10.0x^2+100.0x" + "'", str4.equals("10.0x^2+100.0x"));
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 0.0f + "'", float5 == 0.0f);
        org.junit.Assert.assertTrue("'" + float8 + "' != '" + Float.POSITIVE_INFINITY + "'", float8 == Float.POSITIVE_INFINITY);
    }

    @Test
    public void test07() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test07");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        float float9 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float9 + "' != '" + 10.0f + "'", float9 == 10.0f);
    }

    @Test
    public void test08() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test08");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) (byte) 10, (float) (short) 0, (float) (byte) 10);
        float float4 = funcionCuadratica3.getB();
        java.lang.String str5 = funcionCuadratica3.toString();
        float float6 = funcionCuadratica3.getA();
        float float7 = funcionCuadratica3.getA();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 0.0f + "'", float4 == 0.0f);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "10.0x^2+10.0" + "'", str5.equals("10.0x^2+10.0"));
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + 10.0f + "'", float6 == 10.0f);
        org.junit.Assert.assertTrue("'" + float7 + "' != '" + 10.0f + "'", float7 == 10.0f);
    }

    @Test
    public void test09() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test09");
        byThey.practico5.FuncionCuadratica funcionCuadratica0 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica0.setB((float) 10);
        float float4 = funcionCuadratica0.eval((float) 10);
        float float6 = funcionCuadratica0.eval((-1.0f));
        java.lang.String str7 = funcionCuadratica0.toString();
        funcionCuadratica0.setC(0.0f);
        byThey.practico5.FuncionCuadratica funcionCuadratica10 = new byThey.practico5.FuncionCuadratica();
        java.lang.String str11 = funcionCuadratica10.toString();
        funcionCuadratica10.setC((float) 10);
        boolean boolean14 = funcionCuadratica0.equals(funcionCuadratica10);
        byThey.practico5.FuncionCuadratica funcionCuadratica15 = new byThey.practico5.FuncionCuadratica();
        funcionCuadratica15.setB((float) 10);
        float float19 = funcionCuadratica15.eval((float) 10);
        float float21 = funcionCuadratica15.eval((-1.0f));
        java.lang.String str22 = funcionCuadratica15.toString();
        float float23 = funcionCuadratica15.getB();
        boolean boolean24 = funcionCuadratica0.equals(funcionCuadratica15);
        java.lang.String str25 = funcionCuadratica15.toString();
        java.lang.String str26 = funcionCuadratica15.toString();
        java.lang.Class<?> wildcardClass27 = funcionCuadratica15.getClass();
        funcionCuadratica15.setA(52.0f);
        java.lang.String str30 = funcionCuadratica15.toString();
        funcionCuadratica15.setB(1.00802796E33f);
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + 200.0f + "'", float4 == 200.0f);
        org.junit.Assert.assertTrue("'" + float6 + "' != '" + (-9.0f) + "'", float6 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "1.0x^2+10.0x" + "'", str7.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "1.0x^2" + "'", str11.equals("1.0x^2"));
        org.junit.Assert.assertTrue("'" + boolean14 + "' != '" + false + "'", boolean14 == false);
        org.junit.Assert.assertTrue("'" + float19 + "' != '" + 200.0f + "'", float19 == 200.0f);
        org.junit.Assert.assertTrue("'" + float21 + "' != '" + (-9.0f) + "'", float21 == (-9.0f));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "1.0x^2+10.0x" + "'", str22.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + float23 + "' != '" + 10.0f + "'", float23 == 10.0f);
        org.junit.Assert.assertTrue("'" + boolean24 + "' != '" + true + "'", boolean24 == true);
        org.junit.Assert.assertTrue("'" + str25 + "' != '" + "1.0x^2+10.0x" + "'", str25.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "1.0x^2+10.0x" + "'", str26.equals("1.0x^2+10.0x"));
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertTrue("'" + str30 + "' != '" + "52.0x^2+10.0x" + "'", str30.equals("52.0x^2+10.0x"));
    }

    @Test
    public void test10() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test10");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica(20100.0f, (float) (short) -1, (float) 100);
        float float4 = funcionCuadratica3.getB();
        float float5 = funcionCuadratica3.getC();
        int int6 = funcionCuadratica3.numRaices();
        org.junit.Assert.assertTrue("'" + float4 + "' != '" + (-1.0f) + "'", float4 == (-1.0f));
        org.junit.Assert.assertTrue("'" + float5 + "' != '" + 100.0f + "'", float5 == 100.0f);
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
    }

    @Test
    public void test11() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test11");
        byThey.practico5.FuncionCuadratica funcionCuadratica3 = new byThey.practico5.FuncionCuadratica((float) 10, (float) (short) 100, (float) 0L);
        funcionCuadratica3.setB((float) 1);
        funcionCuadratica3.setB((float) (byte) 1);
        int int8 = funcionCuadratica3.numRaices();
        funcionCuadratica3.setA((float) (short) 1);
        funcionCuadratica3.setB((float) 2);
        funcionCuadratica3.setC((float) (short) 100);
        float float15 = funcionCuadratica3.getB();
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2 + "'", int8 == 2);
        org.junit.Assert.assertTrue("'" + float15 + "' != '" + 2.0f + "'", float15 == 2.0f);
    }
}

