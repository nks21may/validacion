package test.practico5.NodeCachingLinkedList;


import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest2 {

    public static boolean debug = false;

    @Test
    public void test001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test001");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int4 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test002");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[0,1,0]" + "'", str14.equals("[0,1,0]"));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1 + "'", int16.equals(1));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[0,0]" + "'", str17.equals("[0,0]"));
    }

    @Test
    public void test003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test003");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[1]" + "'", str6.equals("[1]"));
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test004");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[0]" + "'", str12.equals("[0]"));
    }

    @Test
    public void test005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test005");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str13 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 100 + "'", int8.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 100 + "'", int10.equals(100));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "[100,100,10]" + "'", str13.equals("[100,100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test006");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test007");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test008");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[0]" + "'", str12.equals("[0]"));
    }

    @Test
    public void test009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test009");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[0]" + "'", str8.equals("[0]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNull(int12);
    }

    @Test
    public void test010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test010");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[10]" + "'", str7.equals("[10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[10]" + "'", str8.equals("[10]"));
    }

    @Test
    public void test011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test011");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (short) 0);
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 20 + "'", int12.equals(20));
    }

    @Test
    public void test012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test012");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test013");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int18 = nodeCachingLinkedList0.removeIndex(1);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 20 + "'", int16.equals(20));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 100 + "'", int18.equals(100));
    }

    @Test
    public void test014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test014");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) 'a');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[10]" + "'", str9.equals("[10]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[10]" + "'", str10.equals("[10]"));
    }

    @Test
    public void test015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test015");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass15 = nodeCachingLinkedList0.getClass();
        java.lang.String str16 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "[20,0,1]" + "'", str16.equals("[20,0,1]"));
    }

    @Test
    public void test016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test016");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[1,100,0]" + "'", str14.equals("[1,100,0]"));
    }

    @Test
    public void test017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test017");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[100,0,1]" + "'", str12.equals("[100,0,1]"));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 100 + "'", int14.equals(100));
    }

    @Test
    public void test018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test018");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Class<?> wildcardClass16 = nodeCachingLinkedList0.getClass();
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[100,1]" + "'", str17.equals("[100,1]"));
    }

    @Test
    public void test019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test019");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
    }

    @Test
    public void test020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test020");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[20,10]" + "'", str6.equals("[20,10]"));
    }

    @Test
    public void test021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test021");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
    }

    @Test
    public void test022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test022");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int17 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Class<?> wildcardClass18 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass19 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass20 = nodeCachingLinkedList0.getClass();
        java.lang.String str21 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertNull(int17);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "[100,0]" + "'", str21.equals("[100,0]"));
    }

    @Test
    public void test023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test023");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int9 = nodeCachingLinkedList0.get(0);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
    }

    @Test
    public void test024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test024");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test025");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[-1,20,100,10]" + "'", str12.equals("[-1,20,100,10]"));
    }

    @Test
    public void test026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test026");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.Integer int13 = nodeCachingLinkedList0.get(0);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[0,0]" + "'", str14.equals("[0,0]"));
    }

    @Test
    public void test027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test027");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) '4');
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test028");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 10 + "'", int8.equals(10));
    }

    @Test
    public void test029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test029");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[0]" + "'", str12.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
    }

    @Test
    public void test030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test030");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[-1,10]" + "'", str10.equals("[-1,10]"));
    }

    @Test
    public void test031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test031");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test032");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test033");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 10 + "'", int6.equals(10));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test034");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test035");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Class<?> wildcardClass16 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int18 = nodeCachingLinkedList0.removeIndex((int) '#');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNull(int18);
    }

    @Test
    public void test036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test036");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[-1,10]" + "'", str12.equals("[-1,10]"));
    }

    @Test
    public void test037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test037");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) '#');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test038");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
    }

    @Test
    public void test039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test039");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[1]" + "'", str7.equals("[1]"));
    }

    @Test
    public void test040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test040");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[-1,1]" + "'", str9.equals("[-1,1]"));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test041");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.get(1);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test042");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 100 + "'", int8.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 100 + "'", int10.equals(100));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[100,10]" + "'", str11.equals("[100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test043");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test044");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(20);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test045");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[-1]" + "'", str12.equals("[-1]"));
    }

    @Test
    public void test046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test046");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[20,10]" + "'", str6.equals("[20,10]"));
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNull(int10);
    }

    @Test
    public void test047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test047");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int16 = nodeCachingLinkedList0.get(1);
        java.lang.Integer int18 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.String str19 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 20 + "'", int16.equals(20));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1 + "'", int18.equals(1));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "[20,100]" + "'", str19.equals("[20,100]"));
    }

    @Test
    public void test048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test048");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test049");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[1]" + "'", str6.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[1]" + "'", str7.equals("[1]"));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test050");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[0]" + "'", str6.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[0,0]" + "'", str9.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[0,0]" + "'", str10.equals("[0,0]"));
    }

    @Test
    public void test051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test051");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[100]" + "'", str9.equals("[100]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[100]" + "'", str10.equals("[100]"));
    }

    @Test
    public void test052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test052");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
    }

    @Test
    public void test053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test053");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test054");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[0]" + "'", str12.equals("[0]"));
        org.junit.Assert.assertNull(int16);
    }

    @Test
    public void test055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test055");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test056");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[0]" + "'", str6.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[0,0]" + "'", str9.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[0]" + "'", str12.equals("[0]"));
    }

    @Test
    public void test057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test057");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str16 = nodeCachingLinkedList0.toString();
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        java.lang.Integer int19 = nodeCachingLinkedList0.removeIndex((int) 'a');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "[10,100]" + "'", str16.equals("[10,100]"));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[10,100]" + "'", str17.equals("[10,100]"));
        org.junit.Assert.assertNull(int19);
    }

    @Test
    public void test058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test058");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int7 = nodeCachingLinkedList0.get(1);
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 10 + "'", int7.equals(10));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[10,10]" + "'", str8.equals("[10,10]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[10,10]" + "'", str14.equals("[10,10]"));
    }

    @Test
    public void test059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test059");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test060");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[1]" + "'", str10.equals("[1]"));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test061");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int16 = nodeCachingLinkedList0.get(1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str19 = nodeCachingLinkedList0.toString();
        java.lang.Integer int21 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 20 + "'", int16.equals(20));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "[10,1,20,100]" + "'", str19.equals("[10,1,20,100]"));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 10 + "'", int21.equals(10));
    }

    @Test
    public void test062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test062");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(100);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test063");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[10,20]" + "'", str14.equals("[10,20]"));
    }

    @Test
    public void test064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test064");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 100 + "'", int10.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test065");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test066");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test067");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) '4');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[1,20]" + "'", str9.equals("[1,20]"));
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test068");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test069");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test070");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[-1,10]" + "'", str10.equals("[-1,10]"));
    }

    @Test
    public void test071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test071");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 20 + "'", int9.equals(20));
    }

    @Test
    public void test072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test072");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) 'a');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test073");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[1]" + "'", str4.equals("[1]"));
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test074");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int5 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
    }

    @Test
    public void test075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test075");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex(1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 20 + "'", int12.equals(20));
    }

    @Test
    public void test076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test076");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        java.lang.String str13 = nodeCachingLinkedList0.toString();
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int16 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[0]" + "'", str12.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "[0]" + "'", str13.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[0]" + "'", str14.equals("[0]"));
    }

    @Test
    public void test077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test077");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) 'a');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test078");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[20,100,0]" + "'", str17.equals("[20,100,0]"));
    }

    @Test
    public void test079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test079");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test080");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) '4');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test081");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex(20);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test082");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test083");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[0]" + "'", str10.equals("[0]"));
    }

    @Test
    public void test084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test084");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test085");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) '4');
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[1,1]" + "'", str10.equals("[1,1]"));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test086");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[1]" + "'", str7.equals("[1]"));
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test087");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test088");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass16 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 10 + "'", int11.equals(10));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test089");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNull(int12);
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[10]" + "'", str15.equals("[10]"));
    }

    @Test
    public void test090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test090");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test091");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test092");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 100 + "'", int8.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[100,10]" + "'", str11.equals("[100,10]"));
    }

    @Test
    public void test093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test093");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
    }

    @Test
    public void test094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test094");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test095");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        try {
            java.lang.Integer int18 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 20 + "'", int16.equals(20));
    }

    @Test
    public void test096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test096");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) 'a');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int12);
    }

    @Test
    public void test097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test097");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[100]" + "'", str11.equals("[100]"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[100]" + "'", str12.equals("[100]"));
    }

    @Test
    public void test098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test098");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get(1);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((-1));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[10,0]" + "'", str7.equals("[10,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test099");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
    }

    @Test
    public void test100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test100");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) '#');
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test101");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test102");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str13 = nodeCachingLinkedList0.toString();
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        java.lang.Class<?> wildcardClass16 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[100,0]" + "'", str10.equals("[100,0]"));
        org.junit.Assert.assertNull(int12);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "[100,0]" + "'", str13.equals("[100,0]"));
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test103");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        java.lang.Integer int19 = nodeCachingLinkedList0.removeIndex((int) '4');
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 100 + "'", int8.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 100 + "'", int10.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[0,100,10]" + "'", str14.equals("[0,100,10]"));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[10,0,100,10]" + "'", str17.equals("[10,0,100,10]"));
        org.junit.Assert.assertNull(int19);
    }

    @Test
    public void test104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test104");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test105");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.Integer int13 = nodeCachingLinkedList0.get(0);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[0,0]" + "'", str10.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[0,0]" + "'", str11.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test106");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[0,1]" + "'", str9.equals("[0,1]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test107");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
    }

    @Test
    public void test108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test108");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[20]" + "'", str10.equals("[20]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[20]" + "'", str11.equals("[20]"));
    }

    @Test
    public void test109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test109");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get(1);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int17 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Integer int19 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 10 + "'", int11.equals(10));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1 + "'", int13.equals(1));
        org.junit.Assert.assertNull(int17);
        org.junit.Assert.assertNull(int19);
    }

    @Test
    public void test110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test110");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get(0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test111");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get(0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test112");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex(20);
        java.lang.Class<?> wildcardClass17 = nodeCachingLinkedList0.getClass();
        java.lang.String str18 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNull(int16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "[10,100]" + "'", str18.equals("[10,100]"));
    }

    @Test
    public void test113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test113");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) '4');
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int11);
    }
}

