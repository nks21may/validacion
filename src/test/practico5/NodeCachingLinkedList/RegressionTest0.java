package test.practico5.NodeCachingLinkedList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest0 {

    public static boolean debug = false;

    @Test
    public void test001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test001");
        int int0 = byThey.practico5.NodeCachingLinkedList.DEFAULT_MAXIMUM_CACHE_SIZE;
        org.junit.Assert.assertTrue("'" + int0 + "' != '" + 20 + "'", int0 == 20);
    }

    @Test
    public void test002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test002");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        try {
            java.lang.Integer int4 = nodeCachingLinkedList0.get(0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test003");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        try {
            java.lang.Integer int4 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test004");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int4 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test005");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        try {
            java.lang.Integer int4 = nodeCachingLinkedList0.get(0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test006");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        try {
            java.lang.Integer int4 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test007");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test008");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int4 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test009");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get(1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[0]" + "'", str8.equals("[0]"));
    }

    @Test
    public void test010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test010");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 1 + "'", int4.equals(1));
    }

    @Test
    public void test011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test011");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test012");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test013");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        try {
            java.lang.Integer int2 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test014");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
    }

    @Test
    public void test015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test015");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test016");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test017");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test018");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
    }

    @Test
    public void test019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test019");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test020");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
    }

    @Test
    public void test021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test021");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test022");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        try {
            java.lang.Integer int4 = nodeCachingLinkedList0.get((int) (short) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test023");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int4 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test024");
        java.lang.Object obj0 = new java.lang.Object();
        java.lang.Class<?> wildcardClass1 = obj0.getClass();
        java.lang.Class<?> wildcardClass2 = obj0.getClass();
        java.lang.Class<?> wildcardClass3 = obj0.getClass();
        java.lang.Class<?> wildcardClass4 = obj0.getClass();
        java.lang.Class<?> wildcardClass5 = obj0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test025");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[0]" + "'", str8.equals("[0]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test026");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
    }

    @Test
    public void test027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test027");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test028");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
    }

    @Test
    public void test029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test029");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
    }

    @Test
    public void test030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test030");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
    }

    @Test
    public void test031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test031");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
    }

    @Test
    public void test032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test032");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get(0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 10 + "'", int11.equals(10));
    }

    @Test
    public void test033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test033");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
    }

    @Test
    public void test034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test034");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int5 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
    }

    @Test
    public void test035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test035");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test036");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test037");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int5 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass3);
    }

    @Test
    public void test038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test038");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        try {
            java.lang.Integer int5 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
    }

    @Test
    public void test039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test039");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test040");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int3 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
    }

    @Test
    public void test041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test041");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get(1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test042");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 10 + "'", int11.equals(10));
    }

    @Test
    public void test043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test043");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
    }

    @Test
    public void test044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test044");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
    }

    @Test
    public void test045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test045");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test046");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test047");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test048");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test049");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(0);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
    }

    @Test
    public void test050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test050");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test051");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((-1));
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test052");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
    }

    @Test
    public void test053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test053");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass15 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test054");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[10]" + "'", str7.equals("[10]"));
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test055");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int5 = nodeCachingLinkedList0.get((int) (short) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
    }

    @Test
    public void test056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test056");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) (short) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[10]" + "'", str4.equals("[10]"));
    }

    @Test
    public void test057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test057");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test058");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
    }

    @Test
    public void test059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test059");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-1) + "'", int8.equals((-1)));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[10]" + "'", str9.equals("[10]"));
    }

    @Test
    public void test060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test060");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[0]" + "'", str8.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[0]" + "'", str9.equals("[0]"));
    }

    @Test
    public void test061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test061");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) ' ');
        try {
            java.lang.Integer int17 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertNull(int15);
    }

    @Test
    public void test062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test062");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) 'a');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test063");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[1]" + "'", str9.equals("[1]"));
    }

    @Test
    public void test064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test064");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test065");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
    }

    @Test
    public void test066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test066");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test067");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test068");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test069");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[1]" + "'", str7.equals("[1]"));
    }

    @Test
    public void test070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test070");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
    }

    @Test
    public void test071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test071");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test072");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int5 = nodeCachingLinkedList0.get(0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
    }

    @Test
    public void test073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test073");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test074");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[-1,100,10]" + "'", str11.equals("[-1,100,10]"));
    }

    @Test
    public void test075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test075");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int3 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
    }

    @Test
    public void test076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test076");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) '4');
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test077");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test078");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test079");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test080");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.get(0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.get(0);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[1]" + "'", str12.equals("[1]"));
    }

    @Test
    public void test081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test081");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
    }

    @Test
    public void test082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test082");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        try {
            java.lang.Integer int2 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
    }

    @Test
    public void test083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test083");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test084");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.get(0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.get(0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test085");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) ' ');
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[20,100]" + "'", str11.equals("[20,100]"));
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test086");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test087");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test088");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
    }

    @Test
    public void test089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test089");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 100 + "'", int8.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test090");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) '4');
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test091");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) '4');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test092");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
    }

    @Test
    public void test093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test093");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (short) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
    }

    @Test
    public void test094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test094");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test095");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test096");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNull(int12);
    }

    @Test
    public void test097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test097");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[100,10]" + "'", str7.equals("[100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 100 + "'", int10.equals(100));
    }

    @Test
    public void test098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test098");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,1]" + "'", str9.equals("[20,1]"));
    }

    @Test
    public void test099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test099");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test100");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int16 = nodeCachingLinkedList0.get((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str19 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 100 + "'", int16.equals(100));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "[100,20,100]" + "'", str19.equals("[100,20,100]"));
    }

    @Test
    public void test101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test101");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[20,100,10]" + "'", str10.equals("[20,100,10]"));
    }

    @Test
    public void test102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test102");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
    }

    @Test
    public void test103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test103");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex(10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[0,20]" + "'", str8.equals("[0,20]"));
        org.junit.Assert.assertNull(int10);
    }

    @Test
    public void test104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test104");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
    }

    @Test
    public void test105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test105");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
    }

    @Test
    public void test106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test106");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test107");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test108");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
    }

    @Test
    public void test109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test109");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[20,0,1]" + "'", str14.equals("[20,0,1]"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[20,0,1]" + "'", str15.equals("[20,0,1]"));
    }

    @Test
    public void test110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test110");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get(1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test111");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
    }

    @Test
    public void test112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test112");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (byte) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
    }

    @Test
    public void test113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test113");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test114");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
    }

    @Test
    public void test115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test115");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test116");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test117");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
    }

    @Test
    public void test118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test118");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
    }

    @Test
    public void test119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test119");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
    }

    @Test
    public void test120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test120");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test121");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test122");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test123");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.String str16 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int18 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "[]" + "'", str16.equals("[]"));
    }

    @Test
    public void test124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test124");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 100 + "'", int8.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[100,10]" + "'", str10.equals("[100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 10 + "'", int13.equals(10));
    }

    @Test
    public void test125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test125");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((-1));
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test126");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) '#');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test127");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        java.lang.Integer int17 = nodeCachingLinkedList0.get((int) (short) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[10,100,20]" + "'", str15.equals("[10,100,20]"));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 100 + "'", int17.equals(100));
    }

    @Test
    public void test128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test128");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test129");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[1]" + "'", str7.equals("[1]"));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test130");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get(1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test131");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.Integer int12 = nodeCachingLinkedList0.get(1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 10 + "'", int12.equals(10));
    }

    @Test
    public void test132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test132");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test133");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(0);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (short) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 10 + "'", int6.equals(10));
    }

    @Test
    public void test134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test134");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get(0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test135");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(20);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test136");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test137");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex(100);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test138");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[100]" + "'", str15.equals("[100]"));
    }

    @Test
    public void test139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test139");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.get(0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test140");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test141");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[-1,-1,10]" + "'", str12.equals("[-1,-1,10]"));
    }

    @Test
    public void test142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test142");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
    }

    @Test
    public void test143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test143");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[0]" + "'", str8.equals("[0]"));
    }

    @Test
    public void test144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test144");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
    }

    @Test
    public void test145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test145");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (short) 0);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
    }

    @Test
    public void test146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test146");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int11 = nodeCachingLinkedList0.get(0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-1) + "'", int11.equals((-1)));
    }

    @Test
    public void test147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test147");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test148");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test149");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test150");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test151");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test152");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,10]" + "'", str8.equals("[-1,10]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[-1,10]" + "'", str11.equals("[-1,10]"));
    }

    @Test
    public void test153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test153");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test154");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test155");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex(20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test156");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test157");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[1]" + "'", str4.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test158");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 100 + "'", int8.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test159");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[1]" + "'", str4.equals("[1]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test160");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
        org.junit.Assert.assertNull(int15);
    }

    @Test
    public void test161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test161");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
    }

    @Test
    public void test162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test162");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test163");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (short) 1);
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,10]" + "'", str8.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1) + "'", int10.equals((-1)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 10 + "'", int12.equals(10));
    }

    @Test
    public void test164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test164");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test165");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test166");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
    }

    @Test
    public void test167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test167");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int16 = nodeCachingLinkedList0.get((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int20 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Class<?> wildcardClass21 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass22 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 100 + "'", int16.equals(100));
        org.junit.Assert.assertNull(int20);
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test168");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test169");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNull(int15);
    }

    @Test
    public void test170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test170");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test171");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (short) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test172");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test173");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test174");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[]" + "'", str10.equals("[]"));
    }

    @Test
    public void test175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test175");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test176");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
    }

    @Test
    public void test177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test177");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        java.lang.String str13 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[20,0,1]" + "'", str12.equals("[20,0,1]"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "[20,0,1]" + "'", str13.equals("[20,0,1]"));
    }

    @Test
    public void test178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test178");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
    }

    @Test
    public void test179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test179");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) 'a');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test180");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[1]" + "'", str4.equals("[1]"));
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test181");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) 'a');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test182");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test183");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int17 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertNull(int17);
    }

    @Test
    public void test184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test184");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
    }

    @Test
    public void test185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test185");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[100,10]" + "'", str11.equals("[100,10]"));
    }

    @Test
    public void test186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test186");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.get(0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 0 + "'", int5.equals(0));
    }

    @Test
    public void test187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test187");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int16 = nodeCachingLinkedList0.get(1);
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 20 + "'", int16.equals(20));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[1,20,100]" + "'", str17.equals("[1,20,100]"));
    }

    @Test
    public void test188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test188");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex(20);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test189");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (short) 1);
        java.lang.String str13 = nodeCachingLinkedList0.toString();
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex(20);
        try {
            java.lang.Integer int17 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,10]" + "'", str8.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1) + "'", int10.equals((-1)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 10 + "'", int12.equals(10));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "[-1,10]" + "'", str13.equals("[-1,10]"));
        org.junit.Assert.assertNull(int15);
    }

    @Test
    public void test190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test190");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[10]" + "'", str7.equals("[10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[10]" + "'", str8.equals("[10]"));
    }

    @Test
    public void test191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test191");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
    }

    @Test
    public void test192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test192");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
    }

    @Test
    public void test193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test193");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test194");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[]" + "'", str8.equals("[]"));
    }

    @Test
    public void test195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test195");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[10]" + "'", str4.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test196");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test197");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.get(0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
    }

    @Test
    public void test198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test198");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test199");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[1,0]" + "'", str12.equals("[1,0]"));
    }

    @Test
    public void test200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test200");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[20]" + "'", str7.equals("[20]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[20]" + "'", str8.equals("[20]"));
    }

    @Test
    public void test201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test201");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test202");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test203");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
    }

    @Test
    public void test204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test204");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int16 = nodeCachingLinkedList0.get((int) (byte) 1);
        try {
            java.lang.Integer int18 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 100 + "'", int16.equals(100));
    }

    @Test
    public void test205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test205");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[20,100,10]" + "'", str10.equals("[20,100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test206");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + int5 + "' != '" + 1 + "'", int5.equals(1));
    }

    @Test
    public void test207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test207");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test208");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) 'a');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[0,10]" + "'", str11.equals("[0,10]"));
    }

    @Test
    public void test209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test209");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) '#');
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test210");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[1,1]" + "'", str10.equals("[1,1]"));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1 + "'", int13.equals(1));
    }

    @Test
    public void test211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test211");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[1]" + "'", str4.equals("[1]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[1]" + "'", str9.equals("[1]"));
    }

    @Test
    public void test212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test212");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test213");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test214");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
    }

    @Test
    public void test215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test215");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test216");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
    }

    @Test
    public void test217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test217");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test218");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test219");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test220");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test221");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[0,1]" + "'", str9.equals("[0,1]"));
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test222");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[1]" + "'", str6.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[1]" + "'", str7.equals("[1]"));
    }

    @Test
    public void test223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test223");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex(100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNull(int15);
    }

    @Test
    public void test224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test224");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get(1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[100]" + "'", str9.equals("[100]"));
    }

    @Test
    public void test225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test225");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test226");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str13 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "[1,20,100]" + "'", str13.equals("[1,20,100]"));
    }

    @Test
    public void test227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test227");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[1]" + "'", str11.equals("[1]"));
    }

    @Test
    public void test228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test228");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (byte) 1);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[20,100,10]" + "'", str10.equals("[20,100,10]"));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100 + "'", int12.equals(100));
    }

    @Test
    public void test229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test229");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
    }

    @Test
    public void test230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test230");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test231");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[10]" + "'", str7.equals("[10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[10]" + "'", str8.equals("[10]"));
    }

    @Test
    public void test232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test232");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
    }

    @Test
    public void test233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test233");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int17 = nodeCachingLinkedList0.get(1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1) + "'", int15.equals((-1)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
    }

    @Test
    public void test234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test234");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test235");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[1]" + "'", str6.equals("[1]"));
    }

    @Test
    public void test236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test236");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[10,0]" + "'", str7.equals("[10,0]"));
    }

    @Test
    public void test237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test237");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.Integer int12 = nodeCachingLinkedList0.get(1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 10 + "'", int12.equals(10));
    }

    @Test
    public void test238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test238");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[0]" + "'", str8.equals("[0]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test239");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
    }

    @Test
    public void test240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test240");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int17 = nodeCachingLinkedList0.get(1);
        java.lang.Integer int19 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1) + "'", int15.equals((-1)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 100 + "'", int19.equals(100));
    }

    @Test
    public void test241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test241");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 100 + "'", int8.equals(100));
    }

    @Test
    public void test242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test242");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test243");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
    }

    @Test
    public void test244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test244");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int17 = nodeCachingLinkedList0.get((int) (short) 1);
        try {
            java.lang.Integer int19 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 100 + "'", int17.equals(100));
    }

    @Test
    public void test245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test245");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(1);
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test246");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass16 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[20,100]" + "'", str11.equals("[20,100]"));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test247");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int16 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test248");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test249");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,10]" + "'", str8.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1) + "'", int10.equals((-1)));
    }

    @Test
    public void test250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test250");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
    }

    @Test
    public void test251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test251");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test252");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1 + "'", int14.equals(1));
    }

    @Test
    public void test253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test253");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
    }

    @Test
    public void test254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test254");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[1,-1,10]" + "'", str10.equals("[1,-1,10]"));
    }

    @Test
    public void test255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test255");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) '#');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test256");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 100 + "'", int14.equals(100));
    }

    @Test
    public void test257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test257");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-1) + "'", int8.equals((-1)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
    }

    @Test
    public void test258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test258");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test259");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[0,1,0]" + "'", str15.equals("[0,1,0]"));
    }

    @Test
    public void test260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test260");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[10,0]" + "'", str7.equals("[10,0]"));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test261");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
    }

    @Test
    public void test262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test262");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
    }

    @Test
    public void test263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test263");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 10 + "'", int11.equals(10));
    }

    @Test
    public void test264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test264");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[1,10]" + "'", str11.equals("[1,10]"));
    }

    @Test
    public void test265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test265");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test266");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get(1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test267");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test268");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
    }

    @Test
    public void test269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test269");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertNull(int16);
    }

    @Test
    public void test270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test270");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[]" + "'", str8.equals("[]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
    }

    @Test
    public void test271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test271");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (short) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
    }

    @Test
    public void test272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test272");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass16 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test273");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1 + "'", int7.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test274");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
    }

    @Test
    public void test275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test275");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[100,10]" + "'", str10.equals("[100,10]"));
    }

    @Test
    public void test276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test276");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[0,1]" + "'", str9.equals("[0,1]"));
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test277");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
    }

    @Test
    public void test278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test278");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[0]" + "'", str9.equals("[0]"));
    }

    @Test
    public void test279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test279");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test280");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNull(int12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[1]" + "'", str14.equals("[1]"));
    }

    @Test
    public void test281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test281");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test282");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test283");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test284");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int16 = nodeCachingLinkedList0.get((int) (byte) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 20 + "'", int16.equals(20));
    }

    @Test
    public void test285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test285");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int12 = nodeCachingLinkedList0.get(0);
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1) + "'", int12.equals((-1)));
    }

    @Test
    public void test286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test286");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test287");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str16 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "[100,0,-1,-1,10]" + "'", str16.equals("[100,0,-1,-1,10]"));
    }

    @Test
    public void test288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test288");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.Integer int12 = nodeCachingLinkedList0.get(1);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 10 + "'", int12.equals(10));
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test289");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(20);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test290");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test291");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test292");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
    }

    @Test
    public void test293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test293");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[]" + "'", str8.equals("[]"));
    }

    @Test
    public void test294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test294");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test295");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(20);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test296");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[1]" + "'", str7.equals("[1]"));
    }

    @Test
    public void test297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test297");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test298");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 10 + "'", int8.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test299");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[10]" + "'", str7.equals("[10]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
    }

    @Test
    public void test300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test300");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[]" + "'", str12.equals("[]"));
    }

    @Test
    public void test301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test301");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[20]" + "'", str11.equals("[20]"));
    }

    @Test
    public void test302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test302");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int19 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[0,1,0]" + "'", str14.equals("[0,1,0]"));
        org.junit.Assert.assertNull(int16);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[0,1,0]" + "'", str17.equals("[0,1,0]"));
    }

    @Test
    public void test303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test303");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int6 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
    }

    @Test
    public void test304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test304");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[1]" + "'", str4.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1 + "'", int10.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test305");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[20]" + "'", str7.equals("[20]"));
    }

    @Test
    public void test306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test306");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test307");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '4');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test308");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[0]" + "'", str6.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test309");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 1);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        java.lang.String str13 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[1,0]" + "'", str12.equals("[1,0]"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "[1,0]" + "'", str13.equals("[1,0]"));
    }

    @Test
    public void test310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test310");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test311");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test312");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNull(int12);
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test313");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[1]" + "'", str10.equals("[1]"));
    }

    @Test
    public void test314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test314");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test315");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[20]" + "'", str12.equals("[20]"));
    }

    @Test
    public void test316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test316");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test317");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-1) + "'", int14.equals((-1)));
    }

    @Test
    public void test318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test318");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[1]" + "'", str4.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test319");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(1);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 10 + "'", int11.equals(10));
    }

    @Test
    public void test320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test320");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[0,20]" + "'", str8.equals("[0,20]"));
    }

    @Test
    public void test321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test321");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test322");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test323");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[100,100,1,10]" + "'", str14.equals("[100,100,1,10]"));
    }

    @Test
    public void test324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test324");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test325");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test326");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
    }

    @Test
    public void test327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test327");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test328");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,10]" + "'", str8.equals("[-1,10]"));
    }

    @Test
    public void test329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test329");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) '#');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test330");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test331");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
    }

    @Test
    public void test332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test332");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (byte) 1);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[1]" + "'", str8.equals("[1]"));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1 + "'", int12.equals(1));
    }

    @Test
    public void test333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test333");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
    }

    @Test
    public void test334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test334");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int17 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[20,100]" + "'", str11.equals("[20,100]"));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[20,100]" + "'", str14.equals("[20,100]"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[20,100]" + "'", str15.equals("[20,100]"));
    }

    @Test
    public void test335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test335");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 10 + "'", int11.equals(10));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test336");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[1]" + "'", str6.equals("[1]"));
    }

    @Test
    public void test337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test337");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int12 = nodeCachingLinkedList0.get(0);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1) + "'", int12.equals((-1)));
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[-1,100,10]" + "'", str15.equals("[-1,100,10]"));
    }

    @Test
    public void test338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test338");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
    }

    @Test
    public void test339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test339");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0,0]" + "'", str7.equals("[0,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[0]" + "'", str12.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test340");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test341");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test342");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) '4');
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[-1,20,100,10]" + "'", str12.equals("[-1,20,100,10]"));
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test343");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test344");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(20);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test345");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test346");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int16 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test347");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.Integer int12 = nodeCachingLinkedList0.get(1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int18 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        try {
            java.lang.Integer int20 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 10 + "'", int12.equals(10));
        org.junit.Assert.assertNull(int18);
    }

    @Test
    public void test348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test348");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test349");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[10]" + "'", str4.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 10 + "'", int8.equals(10));
    }

    @Test
    public void test350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test350");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.get(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
    }

    @Test
    public void test351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test351");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[0]" + "'", str8.equals("[0]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[0]" + "'", str9.equals("[0]"));
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test352");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) 'a');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
    }

    @Test
    public void test353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test353");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Class<?> wildcardClass16 = nodeCachingLinkedList0.getClass();
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[20,1]" + "'", str17.equals("[20,1]"));
    }

    @Test
    public void test354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test354");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[0,-1]" + "'", str11.equals("[0,-1]"));
    }

    @Test
    public void test355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test355");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test356");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test357");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
    }

    @Test
    public void test358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test358");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int9 = nodeCachingLinkedList0.get(1);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10,0]" + "'", str6.equals("[10,0]"));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
    }

    @Test
    public void test359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test359");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,10]" + "'", str8.equals("[-1,10]"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[-1,10]" + "'", str11.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[-1,10]" + "'", str12.equals("[-1,10]"));
    }

    @Test
    public void test360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test360");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[1]" + "'", str8.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[1]" + "'", str9.equals("[1]"));
    }

    @Test
    public void test361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test361");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.get(1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
    }

    @Test
    public void test362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test362");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int7 = nodeCachingLinkedList0.get(1);
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 10 + "'", int7.equals(10));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[10,10]" + "'", str8.equals("[10,10]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test363");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int16 = nodeCachingLinkedList0.get(1);
        java.lang.Integer int18 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.String str19 = nodeCachingLinkedList0.toString();
        java.lang.String str20 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 20 + "'", int16.equals(20));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1 + "'", int18.equals(1));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "[20,100]" + "'", str19.equals("[20,100]"));
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "[20,100]" + "'", str20.equals("[20,100]"));
    }

    @Test
    public void test364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test364");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test365");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[]" + "'", str8.equals("[]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
    }

    @Test
    public void test366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test366");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) ' ');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[20,100]" + "'", str11.equals("[20,100]"));
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test367");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str16 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "[-1,10,10,10]" + "'", str16.equals("[-1,10,10,10]"));
    }

    @Test
    public void test368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test368");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1) + "'", int10.equals((-1)));
    }

    @Test
    public void test369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test369");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass15 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int17 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test370");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex((-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int20 = nodeCachingLinkedList0.removeIndex((int) ' ');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNull(int16);
        org.junit.Assert.assertNull(int20);
    }

    @Test
    public void test371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test371");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test372");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[-1]" + "'", str10.equals("[-1]"));
    }

    @Test
    public void test373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test373");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[-1,0]" + "'", str10.equals("[-1,0]"));
    }

    @Test
    public void test374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test374");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Class<?> wildcardClass16 = nodeCachingLinkedList0.getClass();
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[]" + "'", str17.equals("[]"));
    }

    @Test
    public void test375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test375");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[1]" + "'", str7.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[20,1]" + "'", str10.equals("[20,1]"));
    }

    @Test
    public void test376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test376");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
    }

    @Test
    public void test377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test377");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test378");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1 + "'", int7.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[100]" + "'", str11.equals("[100]"));
    }

    @Test
    public void test379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test379");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test380");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int4);
    }

    @Test
    public void test381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test381");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.String str13 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 10 + "'", int8.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "[-1,10]" + "'", str13.equals("[-1,10]"));
    }

    @Test
    public void test382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test382");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((-1));
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 10 + "'", int8.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test383");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test384");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get(1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1 + "'", int7.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test385");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
    }

    @Test
    public void test386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test386");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test387");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[20,0,1]" + "'", str14.equals("[20,0,1]"));
        org.junit.Assert.assertNull(int16);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[20,0,1]" + "'", str17.equals("[20,0,1]"));
    }

    @Test
    public void test388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test388");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test389");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test390");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(20);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test391");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[20]" + "'", str10.equals("[20]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[20]" + "'", str11.equals("[20]"));
    }

    @Test
    public void test392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test392");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (short) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1 + "'", int9.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test393");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.String str16 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "[-1]" + "'", str16.equals("[-1]"));
    }

    @Test
    public void test394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test394");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[1,1]" + "'", str11.equals("[1,1]"));
    }

    @Test
    public void test395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test395");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[20,100,10]" + "'", str10.equals("[20,100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test396");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int17 = nodeCachingLinkedList0.get((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[10,100,20]" + "'", str15.equals("[10,100,20]"));
    }

    @Test
    public void test397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test397");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int17 = nodeCachingLinkedList0.get((int) (short) 1);
        java.lang.Class<?> wildcardClass18 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int20 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 100 + "'", int17.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertNull(int20);
    }

    @Test
    public void test398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test398");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test399");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test400");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test401");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[100,10]" + "'", str11.equals("[100,10]"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[100,10]" + "'", str12.equals("[100,10]"));
    }

    @Test
    public void test402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test402");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) '4');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[1]" + "'", str7.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[10,1]" + "'", str10.equals("[10,1]"));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test403");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex(1);
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1 + "'", int10.equals(1));
    }

    @Test
    public void test404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test404");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test405");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
    }

    @Test
    public void test406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test406");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex(20);
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNull(int10);
    }

    @Test
    public void test407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test407");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int17 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Class<?> wildcardClass18 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass19 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertNull(int17);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test408");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int14 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[-1]" + "'", str12.equals("[-1]"));
    }

    @Test
    public void test409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test409");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int17 = nodeCachingLinkedList0.get(0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1) + "'", int13.equals((-1)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 100 + "'", int17.equals(100));
    }

    @Test
    public void test410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test410");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) 'a');
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[1]" + "'", str8.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[1,1]" + "'", str11.equals("[1,1]"));
    }

    @Test
    public void test411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test411");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int10);
    }

    @Test
    public void test412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test412");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[20,-1,0,1]" + "'", str14.equals("[20,-1,0,1]"));
    }

    @Test
    public void test413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test413");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test414");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[10,0]" + "'", str7.equals("[10,0]"));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
    }

    @Test
    public void test415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test415");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str18 = nodeCachingLinkedList0.toString();
        java.lang.String str19 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "[0,100,0]" + "'", str18.equals("[0,100,0]"));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "[0,100,0]" + "'", str19.equals("[0,100,0]"));
    }

    @Test
    public void test416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test416");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int16 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[20,100]" + "'", str11.equals("[20,100]"));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[20,100]" + "'", str14.equals("[20,100]"));
    }

    @Test
    public void test417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test417");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        java.lang.String str16 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[10,100,20]" + "'", str15.equals("[10,100,20]"));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "[10,100,20]" + "'", str16.equals("[10,100,20]"));
    }

    @Test
    public void test418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test418");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str15 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int17 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "[0,100,100,1,10]" + "'", str15.equals("[0,100,100,1,10]"));
    }

    @Test
    public void test419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test419");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test420");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex((-1));
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int20 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNull(int16);
        org.junit.Assert.assertNull(int20);
    }

    @Test
    public void test421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test421");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex(10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int15);
    }

    @Test
    public void test422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test422");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) ' ');
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,10]" + "'", str8.equals("[-1,10]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNull(int12);
    }

    @Test
    public void test423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test423");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test424");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test425");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[100,1]" + "'", str10.equals("[100,1]"));
    }

    @Test
    public void test426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test426");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[0,0]" + "'", str14.equals("[0,0]"));
    }

    @Test
    public void test427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test427");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[10,10,0]" + "'", str10.equals("[10,10,0]"));
    }

    @Test
    public void test428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test428");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[1,0]" + "'", str10.equals("[1,0]"));
    }

    @Test
    public void test429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test429");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test430");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex(10);
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test431");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test432");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[100,10]" + "'", str8.equals("[100,10]"));
    }

    @Test
    public void test433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test433");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get((-1));
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[1]" + "'", str4.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test434");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
    }

    @Test
    public void test435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test435");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test436");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test437");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
    }

    @Test
    public void test438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test438");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
    }

    @Test
    public void test439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test439");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) 'a');
        try {
            java.lang.Integer int11 = nodeCachingLinkedList0.get(20);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNull(int9);
    }

    @Test
    public void test440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test440");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,10]" + "'", str8.equals("[-1,10]"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[-1,10]" + "'", str11.equals("[-1,10]"));
    }

    @Test
    public void test441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test441");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (short) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[20]" + "'", str10.equals("[20]"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[20]" + "'", str11.equals("[20]"));
    }

    @Test
    public void test442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test442");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int16 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 100 + "'", int16.equals(100));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[20,100]" + "'", str17.equals("[20,100]"));
    }

    @Test
    public void test443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test443");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int12 = nodeCachingLinkedList0.get((int) (short) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100 + "'", int12.equals(100));
    }

    @Test
    public void test444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test444");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[-1,1,20]" + "'", str10.equals("[-1,1,20]"));
    }

    @Test
    public void test445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test445");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.String str12 = nodeCachingLinkedList0.toString();
        java.lang.Integer int14 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "[1,0]" + "'", str12.equals("[1,0]"));
        org.junit.Assert.assertNull(int14);
    }

    @Test
    public void test446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test446");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 20 + "'", int8.equals(20));
    }

    @Test
    public void test447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test447");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test448");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
    }

    @Test
    public void test449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test449");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[]" + "'", str10.equals("[]"));
    }

    @Test
    public void test450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test450");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 100 + "'", int10.equals(100));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[]" + "'", str11.equals("[]"));
    }

    @Test
    public void test451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test451");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass3 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) '4');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass3);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test452");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test453");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,0]" + "'", str7.equals("[-1,0]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,0]" + "'", str8.equals("[-1,0]"));
    }

    @Test
    public void test454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test454");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) '#');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
        org.junit.Assert.assertNull(int8);
    }

    @Test
    public void test455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test455");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int8 = nodeCachingLinkedList0.get((int) ' ');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
    }

    @Test
    public void test456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test456");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-1) + "'", int8.equals((-1)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[10]" + "'", str11.equals("[10]"));
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test457");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test458");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.Integer int17 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Class<?> wildcardClass18 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        try {
            java.lang.Integer int22 = nodeCachingLinkedList0.get(100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertNull(int13);
        org.junit.Assert.assertNull(int15);
        org.junit.Assert.assertNull(int17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test459");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int7 = nodeCachingLinkedList0.get((int) '#');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
    }

    @Test
    public void test460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test460");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 100 + "'", int11.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test461");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 10 + "'", int11.equals(10));
    }

    @Test
    public void test462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test462");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 10 + "'", int8.equals(10));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[0]" + "'", str14.equals("[0]"));
    }

    @Test
    public void test463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test463");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
    }

    @Test
    public void test464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test464");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.String str1 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertTrue("'" + str1 + "' != '" + "[]" + "'", str1.equals("[]"));
    }

    @Test
    public void test465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test465");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        java.lang.String str14 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass15 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[20,100,10]" + "'", str9.equals("[20,100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "[-1,20,100,10]" + "'", str14.equals("[-1,20,100,10]"));
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test466");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        java.lang.Class<?> wildcardClass12 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test467");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex((int) ' ');
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[1]" + "'", str8.equals("[1]"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNull(int11);
    }

    @Test
    public void test468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test468");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex(100);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[100,0]" + "'", str10.equals("[100,0]"));
        org.junit.Assert.assertNull(int12);
    }

    @Test
    public void test469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test469");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.Integer int17 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.String str18 = nodeCachingLinkedList0.toString();
        java.lang.String str19 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "[20,1]" + "'", str18.equals("[20,1]"));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "[20,1]" + "'", str19.equals("[20,1]"));
    }

    @Test
    public void test470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test470");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test471");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test472");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) ' ');
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex((int) (byte) 0);
        java.lang.Integer int17 = nodeCachingLinkedList0.removeIndex((int) '#');
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 10 + "'", int15.equals(10));
        org.junit.Assert.assertNull(int17);
    }

    @Test
    public void test473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test473");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 20 + "'", int10.equals(20));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test474");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test475");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.removeIndex(0);
        try {
            java.lang.Integer int13 = nodeCachingLinkedList0.get((int) '4');
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
    }

    @Test
    public void test476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test476");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((-1));
        java.lang.Class<?> wildcardClass13 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[-1]" + "'", str10.equals("[-1]"));
        org.junit.Assert.assertNull(int12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test477");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex(20);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        try {
            java.lang.Integer int9 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test478");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) ' ');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[10]" + "'", str5.equals("[10]"));
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNull(int12);
    }

    @Test
    public void test479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test479");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.Integer int12 = nodeCachingLinkedList0.get(1);
        java.lang.String str13 = nodeCachingLinkedList0.toString();
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get(10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 10 + "'", int12.equals(10));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "[100,10]" + "'", str13.equals("[100,10]"));
    }

    @Test
    public void test480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test480");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        java.lang.String str10 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[10,0]" + "'", str9.equals("[10,0]"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "[10,0]" + "'", str10.equals("[10,0]"));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test481");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.removeIndex(20);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertNull(int13);
    }

    @Test
    public void test482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test482");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (byte) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int10);
    }

    @Test
    public void test483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test483");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int14 = nodeCachingLinkedList0.get((int) (short) 0);
        java.lang.Class<?> wildcardClass15 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[0]" + "'", str7.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1 + "'", int14.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test484");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[0]" + "'", str4.equals("[0]"));
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test485");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (short) 10);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 100 + "'", int8.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 100 + "'", int10.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test486");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (byte) 1);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) (short) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNull(int10);
    }

    @Test
    public void test487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test487");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        java.lang.Class<?> wildcardClass8 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int10 = nodeCachingLinkedList0.get(0);
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) '4');
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[1]" + "'", str3.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[1]" + "'", str4.equals("[1]"));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[1]" + "'", str5.equals("[1]"));
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1 + "'", int10.equals(1));
        org.junit.Assert.assertNull(int12);
    }

    @Test
    public void test488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test488");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass11 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        try {
            java.lang.Integer int15 = nodeCachingLinkedList0.get((int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 100 + "'", int13.equals(100));
    }

    @Test
    public void test489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test489");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int9 = nodeCachingLinkedList0.removeIndex((int) (byte) 100);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int13 = nodeCachingLinkedList0.get((int) (byte) 0);
        java.lang.Class<?> wildcardClass14 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int16 = nodeCachingLinkedList0.removeIndex((int) ' ');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int9);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 20 + "'", int13.equals(20));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertNull(int16);
    }

    @Test
    public void test490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test490");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        try {
            java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[10]" + "'", str6.equals("[10]"));
    }

    @Test
    public void test491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test491");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[]" + "'", str7.equals("[]"));
    }

    @Test
    public void test492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test492");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        java.lang.String str5 = nodeCachingLinkedList0.toString();
        java.lang.String str6 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 20);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex((int) '#');
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "[]" + "'", str5.equals("[]"));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "[]" + "'", str6.equals("[]"));
        org.junit.Assert.assertNull(int10);
        org.junit.Assert.assertNull(int12);
    }

    @Test
    public void test493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test493");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        java.lang.String str7 = nodeCachingLinkedList0.toString();
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass9 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass10 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int12 = nodeCachingLinkedList0.removeIndex(0);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "[-1,10]" + "'", str7.equals("[-1,10]"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[-1,10]" + "'", str8.equals("[-1,10]"));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1) + "'", int12.equals((-1)));
    }

    @Test
    public void test494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test494");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) '4');
        java.lang.Class<?> wildcardClass5 = nodeCachingLinkedList0.getClass();
        java.lang.Class<?> wildcardClass6 = nodeCachingLinkedList0.getClass();
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int10 = nodeCachingLinkedList0.removeIndex(0);
        try {
            java.lang.Integer int12 = nodeCachingLinkedList0.get(0);
            org.junit.Assert.fail("Expected exception of type java.lang.IllegalArgumentException; message: invalid index");
        } catch (java.lang.IllegalArgumentException e) {
        }
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertNull(int10);
    }

    @Test
    public void test495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test495");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.Integer int4 = nodeCachingLinkedList0.removeIndex((int) (byte) -1);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 100);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex(100);
        java.lang.Integer int10 = nodeCachingLinkedList0.get((int) (byte) 1);
        java.lang.String str11 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int15 = nodeCachingLinkedList0.get(0);
        java.lang.String str16 = nodeCachingLinkedList0.toString();
        java.lang.String str17 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int4);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 10 + "'", int10.equals(10));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "[100,10]" + "'", str11.equals("[100,10]"));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "[0,100,10]" + "'", str16.equals("[0,100,10]"));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "[0,100,10]" + "'", str17.equals("[0,100,10]"));
    }

    @Test
    public void test496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test496");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex((int) (short) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 100);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.String str8 = nodeCachingLinkedList0.toString();
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "[]" + "'", str8.equals("[]"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
    }

    @Test
    public void test497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test497");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.String str4 = nodeCachingLinkedList0.toString();
        java.lang.Integer int6 = nodeCachingLinkedList0.removeIndex((int) (short) 0);
        java.lang.Integer int8 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.String str9 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "[]" + "'", str4.equals("[]"));
        org.junit.Assert.assertNull(int6);
        org.junit.Assert.assertNull(int8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "[]" + "'", str9.equals("[]"));
    }

    @Test
    public void test498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test498");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 10);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex((int) '#');
        nodeCachingLinkedList0.addFirst((java.lang.Integer) (-1));
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[10]" + "'", str3.equals("[10]"));
        org.junit.Assert.assertNull(int7);
    }

    @Test
    public void test499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test499");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Class<?> wildcardClass1 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int5 = nodeCachingLinkedList0.removeIndex((int) (short) 1);
        java.lang.Integer int7 = nodeCachingLinkedList0.removeIndex(10);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int11 = nodeCachingLinkedList0.get((int) (byte) 0);
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 0);
        java.lang.Integer int15 = nodeCachingLinkedList0.removeIndex(100);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNull(int5);
        org.junit.Assert.assertNull(int7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertNull(int15);
    }

    @Test
    public void test500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test500");
        byThey.practico5.NodeCachingLinkedList nodeCachingLinkedList0 = new byThey.practico5.NodeCachingLinkedList();
        java.lang.Integer int2 = nodeCachingLinkedList0.removeIndex(1);
        java.lang.String str3 = nodeCachingLinkedList0.toString();
        java.lang.Class<?> wildcardClass4 = nodeCachingLinkedList0.getClass();
        nodeCachingLinkedList0.addFirst((java.lang.Integer) 1);
        java.lang.Integer int8 = nodeCachingLinkedList0.get((int) (byte) 0);
        org.junit.Assert.assertNull(int2);
        org.junit.Assert.assertTrue("'" + str3 + "' != '" + "[]" + "'", str3.equals("[]"));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
    }
}

