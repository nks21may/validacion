package test.practico4;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;
import byThey.practico4.*;

public class IPBlacklistTest {
	
	IPBlacklist ipblack;
	LoginService loginservice;
	
	@Before
	public void setUp() {
		ipblack = new IPBlacklist();
		loginservice = createMock(LoginService.class);
		ipblack.setService(loginservice);
	}

	@Test
	public void testBlacklisted() {
		String s = Utils.getPasswordHashMD5("asd");
		
		expect(loginservice.login("0.0.0.0", "asd", s)).andReturn(false).times(3);
		replay(loginservice);
		
		assertFalse(ipblack.blacklisted("0.0.0.0"));
		ipblack.login("0.0.0.0", "asd", "asd");
		assertFalse(ipblack.blacklisted("0.0.0.0"));
		ipblack.login("0.0.0.0", "asd", "asd");
		assertFalse(ipblack.blacklisted("0.0.0.0"));
		ipblack.login("0.0.0.0", "asd", "asd");
		assertTrue(ipblack.blacklisted("0.0.0.0"));
		assertFalse(ipblack.login("0.0.0.0", "asd", "asd"));

		
		verify(loginservice);
	}
	
	@Test
	public void testGetLastIp() {
		String s = Utils.getPasswordHashMD5("asd");
		expect(loginservice.login("0.0.0.0", "asd", s)).andReturn(false);
		expect(loginservice.login("0.0.0.1", "asd", s)).andReturn(true);
		replay(loginservice);
		
		ipblack.login("0.0.0.0", "asd", s);
		assertThat(ipblack.getLastip(), is("0.0.0.0"));
		ipblack.login("0.0.0.1", "asd", s);
		assertThat(ipblack.getLastip(), is("0.0.0.1"));
		
		verify(loginservice);
	}
}
