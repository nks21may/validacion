package test.practico6;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeNotNull;

import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import byThey.practico6.*;


@RunWith(Theories.class)
public class StrictlySortedSinglyLinkedListTest {
  
	@Theory
	public void testAdd(@SSSLinkedListGen  StrictlySortedSinglyLinkedList l) {
		assumeNotNull(l);
		StrictlySortedSinglyLinkedList laux = (StrictlySortedSinglyLinkedList) l.clone();
		
		Integer n = Integer.MAX_VALUE;
		if (!l.contains(n)){
			l.add(n);
			laux.add(n);
			assertTrue(l.repOK());
			assertTrue(laux.repOK());
		}
		
		l.add(n);
		assertTrue(laux.equals(l));
		assertTrue(l.repOK());
		assertTrue(laux.repOK());
	}
}
