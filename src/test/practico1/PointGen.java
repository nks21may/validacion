package test.practico1;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.junit.experimental.theories.ParametersSuppliedBy;

@Retention(RetentionPolicy.RUNTIME)
@ParametersSuppliedBy(PointGenSupplier.class)
public @interface PointGen {
	int max();

	int min();

	int n();
}
