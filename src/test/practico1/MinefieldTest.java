package test.practico1;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import byThey.practico1.Minefield;

public class MinefieldTest {

	Minefield m;
	
	@Before
	public void setUp() throws Exception {
		m = new Minefield();
		m.putMine(0, 0);
		m.putMine(0, 2);
		m.putMine(2, 0);
		m.putMine(2, 2);
		
		m.putMine(5, 5);
		m.putMine(7, 7);
	}

	@After
	public void tearDown() throws Exception {
		m = null;
	}

	@Test
	public void minedNeighboursTest1() {
		assertEquals(4, m.minedNeighbours(1, 1));
	}

}
