package test.practico1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.experimental.theories.ParameterSignature;
import org.junit.experimental.theories.ParameterSupplier;
import org.junit.experimental.theories.PotentialAssignment;

import byThey.practico1.ListaSobreArreglos;

public class ListGenSupply extends ParameterSupplier{

	@Override
	public List<PotentialAssignment> getValueSources(ParameterSignature arg0) throws Throwable {
		List<PotentialAssignment> list = new ArrayList<PotentialAssignment>();
		ListGen p = arg0.getAnnotation(ListGen.class);
		Random r = new Random();
		ListaSobreArreglos l;
		int m;
		for (int i = 0; i < p.n(); i++) {
			m = r.nextInt(100);
			l = new ListaSobreArreglos();
			for(int k = 0; k < m; k++){
				l.insertar(r.nextInt(l.longitud()+1),r.nextInt());
			}
			list.add(PotentialAssignment.forValue(l.toString(), l));
		}
		
		return list;
	}

}
