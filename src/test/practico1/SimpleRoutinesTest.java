package test.practico1;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.experimental.runners.Enclosed;

import byThey.practico1.SimpleRoutines;

@RunWith(Enclosed.class)
public class SimpleRoutinesTest {

	public static class NonParametrizer {

		//Este test solo esta para que aparezca 100% 
		@Test
		public void testSimpleRoutines() {
			new SimpleRoutines();
			assertTrue(true);
		}
		
		@Test
		public void testMax1() {
			assertEquals(SimpleRoutines.max(3, 2), 3);
		}

		@Test
		public void testMax2() {
			assertEquals(SimpleRoutines.max(-2147483648, 3), 3);
		}

		@Test
		public void testMax3() {
			assertEquals(SimpleRoutines.max(3, 2147483647), 2147483647);
		}

		@SuppressWarnings("rawtypes")
		@Test
		public void testMaxf() {
			assertEquals(SimpleRoutines.max((Comparable) 'a', (Comparable) 'b'), 'b');
		}

		@SuppressWarnings("rawtypes")
		@Test
		public void testGenericMax1() {
			assertEquals(SimpleRoutines.max((Comparable) 'b', (Comparable) 'a'), 'b');
		}

		@SuppressWarnings("rawtypes")
		@Test
		public void testGenericMax2() {
			assertEquals(SimpleRoutines.max((Comparable) 'a', (Comparable) 'a'), 'a');
		}

		@Test(expected = IllegalArgumentException.class)
		public void testGenericMax4() {
			SimpleRoutines.max(null, null);
		}

		@Test(expected = IllegalArgumentException.class)
		public void testGenericMax5() {
			SimpleRoutines.max(1, null);
		}

		@Test(expected = IllegalArgumentException.class)
		public void testGenericMax6() {
			SimpleRoutines.max(null, 2);
		}

		@Test
		public void testLargest() {
			Integer[] a = { 1, 2, 3, 4 };
			assertTrue(SimpleRoutines.largest(a).equals(4));
		}

		@Test
		public void testLargest2() {
			Integer[] b = { 5, 4, 3, 2 };
			assertTrue(SimpleRoutines.largest(b).equals(5));
		}

		@Test
		public void testBubbleSort1() {
			int[] a = { 2, 3, 3, 4, 5 };
			int[] b = { 5, 4, 3, 3, 2 };
			SimpleRoutines.bubbleSort(b);
			assertArrayEquals(b, a);
		}

		@Test
		public void testBubbleSort2() {
			int[] a = { 2, 3, 3, 4, 5 };
			int[] c = { 2, 3, 3, 4, 5 };
			SimpleRoutines.bubbleSort(a);
			assertArrayEquals(a, c);
		}

		@Test
		public void testBubbleSort3() {
			int[] f = {};
			int[] g = {};
			SimpleRoutines.bubbleSort(f);
			assertArrayEquals(f, g);
		}

		@Test
		public void testSelectionSort1() {
			int[] a1 = { 5, 4, 3, 3, 2 };
			int[] a2 = { 5, 4, 3, 3, 2 };
			SimpleRoutines.bubbleSort(a1);
			SimpleRoutines.selectionSort(a2);
			assertArrayEquals(a1, a2);
		}

		@Test
		public void testSelectionSort2() {
			int[] c1 = { 2, 3, 3, 4, 5 };
			int[] c2 = { 2, 3, 3, 4, 5 };
			SimpleRoutines.bubbleSort(c1);
			SimpleRoutines.selectionSort(c2);
			assertArrayEquals(c1, c2);
		}

		@Test
		public void testSelectionSort3() {
			int[] f = {};
			int[] g = {};
			SimpleRoutines.selectionSort(f);
			assertArrayEquals(f, g);
		}

		@Test
		public void testCapicua() {
			char[] a = {};
			assertTrue(SimpleRoutines.capicua(a));
		}

		@Test
		public void testCapicua2() {
			char[] a = { 'a' };
			assertTrue(SimpleRoutines.capicua(a));
		}

		@Test
		public void testCapicua3() {
			char[] a = { 'a', 'b', 'a' };
			assertTrue(SimpleRoutines.capicua(a));
		}

		@Test
		public void testCapicua4() {
			char[] a = { 'a', 'b', 'b', 'c', 'b', 'a' };
			assertFalse(SimpleRoutines.capicua(a));
		}

		@Test(timeout = 1000)
		public void myFibonaccitest() {
			long n = SimpleRoutines.myFibonacci(45);
			org.junit.Assert.assertEquals(1134903170, n);
		}

		@Test
		public void myFibonaccitest2() {
			long res1 = SimpleRoutines.fibonacci(3);
			long res2 = SimpleRoutines.myFibonacci(3);
			assertEquals(res2, res1);
		}

		@Test
		public void myFibonaccitest3() {
			long res1 = SimpleRoutines.myFibonacci(0);
			assertEquals(res1, 0);
		}
	}

	@RunWith(Parameterized.class)
	public static class Parametrizedias {

		@Parameters
		public static Collection<int[][]> data() {
			return Arrays.asList(new int[][][] { { { 5, 3, 3, 4, 2 }, { 2, 3, 3, 4, 5 } },
					{ { 1, 2, 3, 4, 5 }, { 1, 2, 3, 4, 5 } }, { { 5, 4, 3, 2, 1 }, { 1, 2, 3, 4, 5 } }, { {}, {} } });
		}

		private int[] fInput;

		private int[] fExpected;

		public Parametrizedias(int[] input, int[] expected) {
			this.fInput = input;
			this.fExpected = expected;
		}

		@Test
		public void leBubble() {
			SimpleRoutines.bubbleSort(fInput);
			assertArrayEquals(fInput, fExpected);
		}
	}
}
