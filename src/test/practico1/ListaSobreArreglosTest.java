package test.practico1;

import static org.junit.Assume.*;
import static org.junit.Assert.*;

import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import byThey.practico1.ListaSobreArreglos;

@RunWith(Theories.class)
public class ListaSobreArreglosTest {
	
	@Theory
	public void testInsertar(@ListGen(n = 10) ListaSobreArreglos l) {
		assumeNotNull(l);
		assertTrue(l.repOk());
	}

}
