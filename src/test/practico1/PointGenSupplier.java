package test.practico1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.experimental.theories.ParameterSignature;
import org.junit.experimental.theories.ParameterSupplier;
import org.junit.experimental.theories.PotentialAssignment;

import byThey.practico1.Point;

public class PointGenSupplier extends ParameterSupplier {

	@Override
	public List<PotentialAssignment> getValueSources(ParameterSignature arg0) throws Throwable {
		List<PotentialAssignment> l = new ArrayList<PotentialAssignment>();
		Random r = new Random();
		Point p;
		PointGen g = arg0.getAnnotation(PointGen.class);
		int min = g.min();
		int max = g.max();
		int n = g.n();
		for (int i = 0; i < n; i++) {
			p = new Point(r.nextInt(max) + min, r.nextInt(max) + min);
			l.add(PotentialAssignment.forValue(p.toString(), p));
		}
		return l;
	}

}
