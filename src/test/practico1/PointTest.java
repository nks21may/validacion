package test.practico1;

import static org.junit.Assert.*;
import static org.junit.Assume.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.*;
import org.junit.experimental.runners.Enclosed;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import byThey.practico1.Point;

@RunWith(Enclosed.class)
public class PointTest {

	public static class NonParametrizerPoint {

		@Test
		public void testPoint() {
			Point a = new Point();
			Point b = new Point(0, 0);
			assertEquals(a, b);
		}

		@Test
		public void testPointFloatFloat1() {
			Point b = new Point(1, 0);
			Point c = new Point(0, 1);
			assertNotEquals(c, b);
		}

		@Test
		public void testPointFloatFloat2() {
			Point b = new Point(1, 1);
			Point c = new Point(0, 1);
			assertNotEquals(b, c);
		}

		@Test
		public void testPointFloatFloat() {
			Point b = new Point(1, 1);
			Point c = new Point();
			assertNotEquals(c, b);
		}

		@Test
		public void testGetX1() {
			Point a = new Point();
			Point b = new Point(0, 1);
			assertTrue(a.getX() == b.getX());
		}

		@Test
		public void testGetX2() {
			Point a = new Point();
			Point c = new Point(1, 1);
			assertNotEquals(a.getX(), c.getX());
		}

		@Test
		public void testGetY() {
			Point a = new Point();
			Point b = new Point(0, 1);
			Point c = new Point(1, 0);
			assertTrue(a.getY() == c.getY());
			assertNotEquals(a.getY(), b.getY());
		}

		@Test
		public void testSetX() {
			Point a = new Point();
			a.setX(1);
			Point c = new Point(1, 0);
			assertEquals(a, c);
			a.setX(0);
			assertEquals(a, new Point());
		}

		@Test
		public void testSetY() {
			Point a = new Point();
			a.setY(1);
			Point c = new Point(0, 1);
			assertEquals(a, c);
			a.setY(0);
			assertEquals(a, new Point());
		}

		@Test
		public void testSwap() {
			Point a = new Point(1, 1);
			Point b = new Point(1, 0);
			Point c = new Point(0, 1);
			a.swap();
			assertEquals(a, new Point(1, 1));
			b.swap();
			assertEquals(b, c);
			c.swap();
			c.swap();
			assertEquals(b, c);
		}

		@Test
		public void testDistanceTo() {
			Point a = new Point();
			Point b = new Point(3, 5);
			double t = Math.sqrt(Math.pow(3, 2) + Math.pow(5, 2));
			assertTrue(Double.compare(b.distanceTo(a), t) == 0);
		}

		@Test
		public void testEquals() {
			Point a = new Point();
			Point b = new Point(3, 5);
			assertNotEquals(a, null);
			assertNotEquals(a, b);
			assertEquals(a, new Point(0, 0));
		}
	}

	@RunWith(Theories.class)
	public static class TheoriesPoint {

		@DataPoint
		public static Point a = new Point(2, 0);

		@DataPoints
		public static Point[] b = { new Point(2, 0), new Point(3, 0), new Point(1, 0), new Point(-2, 0), };
		
		@Theory
		public void ejer9(Point a, Point b) {
			assumeThat(a.getY(), is(b.getY()));
			assertTrue(a.distanceTo(b) == Math.abs(a.getX() - b.getX()));
		}
		
		@Theory
		public void ejer92(@PointGen(max = 10, min = 0, n = 5) Point a, @PointGen(max = 10, min = 0, n = 5) Point b){
			assumeNotNull(a,b);
			assumeThat(a.getY(), is(b.getY()));
			assertTrue(a.distanceTo(b) == Math.abs(a.getX() - b.getX()));
		}
		
		@Theory
		public void ejer11(@PointGen(max = 10, min = 0, n = 15) Point a, @PointGen(max = 10, min = 0, n = 15) Point b){
			assumeNotNull(a,b);
			assertTrue(a.hashCode() == b.hashCode() || !a.equals(b));
		}
	}
}
