package test.practico3;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import byThey.practico3.BoundedQueue;

import static org.hamcrest.CoreMatchers.*;

public class BoundedQueueTest {

	@Test(expected=IllegalArgumentException.class)
	public void test() {
		BoundedQueue q = new BoundedQueue();
		Object e = q.dequeue();
	}

	@Test
	public void test0() {
		Integer [] qe = {0,1,2};
		BoundedQueue q = new BoundedQueue(Arrays.asList(qe));
		
		Object e = q.dequeue();
		
		assertThat(e, is(equalTo(0)));
	}
	
	@Test
	public void test1() {
		Integer [] qe = {0,1,2};
		BoundedQueue q = new BoundedQueue(3);
		for (int i = 0; i < qe.length; i++)
			q.enqueue(qe[i]);
		
		Object e = q.dequeue();
		assertThat(e, is(equalTo(0)));
	}
	
	@Test
	public void test2() {
		Integer [] qe = {0,1,2};
		BoundedQueue q = new BoundedQueue(Arrays.asList(qe));
		q.dequeue();
		Object e = q.dequeue();
		
		assertThat(q.size(), is(equalTo(1)));
	}
	
	@Test
	public void test3() {
		Integer [] qe = new Integer[50];
		BoundedQueue q = new BoundedQueue(Arrays.asList(qe));

		assertTrue(q.isFull());
	}

	@Test
	public void test4() {
		Integer [] qe = {1,2,3};
		BoundedQueue q = new BoundedQueue(Arrays.asList(qe));
		q.dequeue();

		assertThat(q.dequeue(), is(2));
	}
	
	@Test
	public void test5() {
		Integer [] qe = {1,2,3};
		BoundedQueue q = new BoundedQueue(Arrays.asList(qe));
		q.dequeue();
		assertEquals(q.toString(), "{2, 3}");
	}
}
