package test.practico3;

import static org.junit.Assert.*;
import org.junit.Test;

import byThey.practico3.Sorting;

public class SortingTest {


	@Test
	public void test0() {
		assertNotNull(new Sorting());
	}
	
	@Test
	public void test1() {
		Integer [] a =  {1};
		Sorting.mergeSort(a, 0, 0); 
		assertThat(a, org.hamcrest.Matchers.arrayContaining(1));
	}
	  
	@Test
	public void test2() {
		Integer [] a =  {4,3,5,2,1};
		Sorting.mergeSort(a, 0, 4);
		assertThat(a, org.hamcrest.Matchers.arrayContaining(1,2,3,4,5));
	}
	
	@Test
	public void test3() {
		Integer [] a =  {3,3,3};
		Sorting.mergeSort(a, 0, 2);
		assertThat(a, org.hamcrest.Matchers.arrayContaining(3,3,3));
	}
	
	@Test
	public void test4() {
		Integer [] a =  {6,3,5,3,2,1};
		Sorting.mergeSort(a, 0, 5);
		assertThat(a, org.hamcrest.Matchers.arrayContaining(1,2,3,3,5,6));
	}
	
	@Test
	public void test5() {
		Integer [] a =  {1,2,3,2,3,4,5,6,7,8,9,10};
		Sorting.mergeSort(a, 0, 5);
		assertThat(a, org.hamcrest.Matchers.arrayContaining(1,2,2,3,3,4,5,6,7,8,9,10));
	} 
}

